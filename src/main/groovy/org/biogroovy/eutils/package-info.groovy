/**
 * This package contains classes used to search and fetch data from NCBI's EUtils RESTful services.
 */
package org.biogroovy.eutils;