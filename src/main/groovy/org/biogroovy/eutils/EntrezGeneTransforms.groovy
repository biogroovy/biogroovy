package org.biogroovy.eutils

import org.biogroovy.io.AbsTransformer

/**
 * This class contains a set of XSL transforms which can be used to transform
 * EntrezGene eUtils REST requests into different formats.
 */
class EntrezGeneTransforms extends AbsTransformer{

	/**
	 * Transforms an EntrezGene record into a brief HTML record.
	 */
	static final String BRIEF = '''
	<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
	
		<xsl:template match="/">
			<html>
			<head></head>
			<body>
			<xsl:apply-templates select="//Entrezgene_gene|//Gene-ref_db"/>
			</body>
			</html>
		</xsl:template>
		
		<xsl:template match="//Entrezgene_gene">
		<div>
			<div><label>Gene Symbol:</label></div><div><xsl:value-of select="Gene-ref/Gene-ref_locus" /></div>
		</div>
		<div>
			<div><label>Gene Name:</label></div><div><xsl:value-of select="Gene-ref/Gene-ref_desc" /></div>
		</div>
		</xsl:template>
		
		<xsl:template match="//Gene-ref_db">
			<xsl:apply-templates select="//Dbtag[Dbtag_db='HGNC']|//Dbtag[Dbtag_db='Ensembl']|//Dbtag[Dbtag_db='HPRD']"/>
		</xsl:template>
		
		<xsl:template match="//Dbtag[Dbtag_db='HGNC']">
		<div>
			<div><label>HGNC</label></div><div><xsl:value-of select="//Dbtag_tag/Object_id/Object-id_id" /></div>
		</div>
		</xsl:template>
		
		<xsl:template match="//Dbtag[Dbtag_db='Ensembl']">
		<div>
			<div><label>Ensembl</label></div><div><xsl:value-of select="Dbtag_tag/Object_id/Object-id_str" /></div>
		</div>
		</xsl:template>

		<xsl:template match="//Dbtag[Dbtag_db='HPRD']">
		<div>
			<div><label>HPRD</label></div><div><xsl:value-of select="Dbtag_tag/Object_id/Object-id_str" /></div>
		</div>
		</xsl:template>

		
		<xsl:template match="//Dbtag[Dbtag_db='MIM']">
		<div>
			<div><label>OMIM</label></div><div><xsl:value-of select="Dbtag_tag/Object_id/Object-id_id" /></div>
		</div>
		</xsl:template>

		
	</xsl:stylesheet>
	''';
	
	/**
	 * Transforms an EntrezGene record into a complete HTML record.
	 */
	static final String COMPLETE = '''
	<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
		<xsl:template match="/">
		
		</xsl:template>
	</xsl:stylesheet>
	''';
	
	
	
}
