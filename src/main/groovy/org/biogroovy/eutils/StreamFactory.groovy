package org.biogroovy.eutils;

import java.io.OutputStream;
import java.io.StringReader;
import java.net.URL;

import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

/**
 * The StreamFactory class is designed to transform data from REST services into other formats using XSLTs.
 * 
 * @author markfortner
 *
 */
public class StreamFactory {

	/**
	 * This method processes an input stream (specified by the url parameter), using 
	 * an XSLT specified in a string, and passes the output to an output stream.
	 * 
	 * @param url	The URL (usually for a restful service) which returns raw XML to be transformed.
	 * @param xslt	A string containing the XSLT used to process the XML.
	 * @param out	An output stream containing the processed XML.
	 */
	public static void processInputStream(String url, String xslt, OutputStream out){
		def factory = TransformerFactory.newInstance()
		def transformer = factory.newTransformer(new StreamSource(new StringReader(xslt)))
		URL urlObj = new URL(url);
		transformer.transform(new StreamSource(urlObj.openStream()), new StreamResult(out))
	}

	/**
	 * This method processes an input file, using an XSLT specified in a string, and passes
	 * the output to an output stream.
	 * 
	 * @param inputFile The input file 
	 * @param xslt 	The XSLT contents.
	 * @param out	The output stream.
	 */
	public static processInputStream(File inputFile, String xslt, OutputStream out){
		def factory = TransformerFactory.newInstance()
		def transformer = factory.newTransformer(new StreamSource(new StringReader(xslt)))
		transformer.transform(new StreamSource(inputFile), new StreamResult(out))
	}

	/**
	 * This method processes an input file using an XSLT file, and passes the output to an output stream.
	 * 
	 * @param inputFile The input file 
	 * @param xslt 	The XSLT contents.
	 * @param out	The output stream.
	 */
	public static processInputStream(File inputFile, File xslt, OutputStream out){
		def factory = TransformerFactory.newInstance()
		def transformer = factory.newTransformer(new StreamSource(xslt))
		transformer.transform(new StreamSource(inputFile), new StreamResult(out))
	}

	/**
	 * This convenience method processes XML.
	 * 
	 * @param url	The URL (usually for a restful service) which returns raw XML to be transformed.
	 * @param xslt	A string containing the XSLT used to process the XML.
	 * @param outputFile	An output file containing the processed XML.
	 */
	public static void processInputStream(String url, String xslt, File outputFile){
		processInputStream(url, xslt, new FileOutputStream(outputFile))
	}

	/**
	 * This convenience method processes XML.
	 * 
	 * @param url	The URL (usually for a restful service) which returns raw XML to be transformed.
	 * @param xslt	A File containing the XSLT used to process the XML.
	 * @param out	An output stream containing the processed XML.
	 */
	public static void processInputStream(String url, File xslt, OutputStream out){
		processInputStream(url, xslt.text, out);
	}

	/**
	 * This convenience method processes XML.
	 * @param url	The URL (usually for a restful service) which returns raw XML to be transformed.
	 * @param xslt	A File containing the XSLT used to process the XML.
	 * @param out	An output file containing the processed XML.
	 */
	public static void processInputStream(String url, File xslt, File outputFile){
		processInputStream(url, xslt.text, new FileOutputStream(outputFile));
	}
}
