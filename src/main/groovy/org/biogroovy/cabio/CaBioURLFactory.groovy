package org.biogroovy.cabio

import org.biogroovy.net.URLFactory
import org.w3c.dom.Element

/**
 * The following methods are used to fetch and parse data from the NCI's caBIO
 * RESTful interface.
 */
class CaBioURLFactory extends URLFactory{
	
	static final String PATHWAY_BY_ID = "pathwayById";
	static final String GENE = "gene";
	static final String GENES_IN_PATHWAY = "genesInPathway";
	static final String PROTEIN = "protein";
	

	/** A map of URLs used to fetch */
	private static final Map<String, String> URL_MAP =
		[
			pathwayById:'http://cabioapi.nci.nih.gov/cabio43/GetXML?query=gov.nih.nci.cabio.domain.Pathway&gov.nih.nci.cabio.domain.Pathway[@id=${pathwayId}]',
			gene:'http://cabioapi.nci.nih.gov/cabio41/GetXML?query=Gene&Gene[@symbol=${symbol}]',
			genesInPathway:'http://cabioapi.nci.nih.gov/cabio43/GetXML?query=Gene&Pathway[@id=${pathwayId}]&roleName=geneCollection',
			protein:'http://cabioapi.nci.nih.gov/cabio43/GetXML?query=gov.nih.nci.cabio.domain.Protein&gov.nih.nci.cabio.domain.Protein[@uniProtCode=${geneSymbol}_HUMAN]'
		];
	
	/**
	 * Gets the pathway XML
	 * @param pathwayId  The pathway ID
	 * @return  The root node of the XML document.
	 */
	public static Element getPathwayAsXML(String pathwayId){
		return getURLasXML( URL_MAP.pathwayById, ['pathwayId':pathwayId])
	}
	
	
	/**
	 * Gets the gene XML
	 * @param geneSymbol The gene symbol
	 * @return  The root node of the XML document
	 */
	public static Element getGeneAsXML(String geneSymbol){
		return getURLasXML( URL_MAP.gene, ['symbol':geneSymbol])
	}
	
	/**
	 * Gets a collection of genes found in a pathway.
	 * @param pathwayId  The ID of the pathway.
	 * @return  The root node of the XML document
	 */
	public static Element getGenesInPathway(String pathwayId){
		return getURLasXML( URL_MAP.genesInPathway, ['pathwayId':pathwayId])
	}
	
	/**
	 * Gets a protein
	 * @param geneSymbol  The gene symbol for the protein
	 * @return  The root node of the XML document
	 */
	public static Element getProtein(String geneSymbol){
		return getURLasXML( URL_MAP.protein, ['geneSymbol':geneSymbol.toUpperCase()])
	}
	
}
