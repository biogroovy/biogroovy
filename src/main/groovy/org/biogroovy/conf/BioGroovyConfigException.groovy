package org.biogroovy.conf

class BioGroovyConfigException extends Exception {

	public BioGroovyConfigException() {
		super();
	}

	public BioGroovyConfigException(String message) {
		super(message);
	}

	public BioGroovyConfigException(Throwable cause) {
		super(cause);
	}

	public BioGroovyConfigException(String message, Throwable cause) {
		super(message, cause);
	}

	public BioGroovyConfigException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

}
