/**
 * This package contains classes used to configure biogroovy.
 */
package org.biogroovy.conf;