package org.biogroovy.search;

import groovy.util.logging.Slf4j

import org.biogroovy.io.Serializer
import org.biogroovy.io.local.SearchReader;
import org.biogroovy.io.local.SearchWriter;

/**
 * This class is responsible for serializing a search.
 *
 */
@Slf4j
public class SearchSerializer extends Serializer<Search, Node>{
	
	

	/**
	 * Constructor.
	 */
	public SearchSerializer() {
		super("search", new SearchReader(), new SearchWriter());
	}
	
	public Search read(BufferedInputStream stream){
		reader.read(stream);
	}
	
	public void write(Search src, OutputStream out){
		writer.write(src, out);
	}

}
