package org.biogroovy.search;

import java.util.List;

import com.google.common.base.Predicate;

/**
 * Implementations of this interface are responsible for determining if search
 * parameters are valid. The Predicate is responsible for validating the
 * SearchParam or SearchParamSet. If they are invalid, then a series of
 * validation messages can be retrieved using the
 * {@link #getValidationMessages()} method.
 * 
 *
 * @param <S>
 */
public interface ISearchParamValidator<S> extends Predicate<S> {

	/**
	 * Gets a list of validation messages.  These are messages that indicate
	 * how the search parameter's values fails the validator's criteria.
	 * @return a list of validation messages.
	 */
	List<String> getValidationMessages();

}
