package org.biogroovy.search;

/**
 * Implementations of this interface are responsible for converting collections of POJOs
 * returned by the IReader into SearchResult objects.  The result builder makes it possible
 * to parse partial objects and return those as search results.
 * 
 * @param <P> the POJO type
 */
public interface IResultBuilder<P> {
	
	/** 
	 * This method is responsible for converting the POJO into a SearchResult object. 
	 * @param pojo the POJO
	 * @return the search result object
	 */
	SearchResult<P> convert(P pojo);

}
