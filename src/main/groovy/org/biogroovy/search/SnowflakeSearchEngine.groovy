package org.biogroovy.search

/**
 * Implementations of this interface take lists of search results from previous searches,
 * analyze the search results, and perform subsequent searches.  For example, you might have a 
 * SnowflakeSearchEngine that finds the most common MeSH terms in a series of search results,
 * and uses those terms to construct a new search.  The term "Snowflake" arises because it uses 
 * the agglomerated search results similar to the way in which snowflakes are agglomerated
 * into snowballs.
 *
 */
interface SnowflakeSearchEngine extends ISearchEngine{
	
	
	/**
	 * Implementations of this method process the initialResults and use it to construct
	 * and execute searches.
	 * 
	 * @param initialResults a list of initial search results obtained during a previous search.
	 * @return a list of new search results
	 */
	public List<SearchResult> research(List<SearchResult> initialResults);

}
