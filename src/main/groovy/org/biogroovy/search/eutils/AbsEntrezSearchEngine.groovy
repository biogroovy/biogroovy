package org.biogroovy.search.eutils;

import org.biogroovy.search.AbstractSearchEngine;
import org.biogroovy.search.SearchResult;

/**
 * 
 *
 */
public abstract class AbsEntrezSearchEngine extends AbstractSearchEngine<SearchResult>{
	
	/**
	 * Constructor.
	 */
	public AbsEntrezSearchEngine(){
		super(new EntrezParamMapBuilder());
	}

}
