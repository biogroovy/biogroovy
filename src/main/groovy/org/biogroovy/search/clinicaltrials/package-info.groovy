/**
 * This package contains clinical trials-related classes.
 *
 */
package org.biogroovy.search.clinicaltrials;