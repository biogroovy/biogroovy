package org.biogroovy.search;

/**
 * Implementations of this interface are responsible for converting a
 * SearchParamSet into a map of values.  Because different search engines
 * handle parameters differently, this interface makes it possible to inject
 * the manner in which parameters are converted into URL parameters. For example,
 * The Entrez search engines can use MeSH parameters as part of the main search term.
 *
 */
public interface ISearchParamMapBuilder {
	
	/**
	 * This method is responsible for converting the parameter set into
	 * a form suitable for binding to a URL.
	 * @param paramSet the parameter set.
	 * @return a map of key-value pairs suitable for posting to a URL.
	 */
	Map<String, String> createParamMap(SearchParamSet paramSet);
	
	/**
	 * This method is responsible for converting the parameter set into
	 * a form suitable for binding to a URL.  Only those parameters which
	 * have values will be included in the resulting map.
	 * @param paramSet the parameter set.
	 * @return a map of key-value pairs suitable for posting to a URL.
	 */
	Map<String, String> createPopulatedParamMap(SearchParamSet paramSet);

}
