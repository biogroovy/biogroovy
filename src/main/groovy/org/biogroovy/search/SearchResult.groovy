package org.biogroovy.search;

import org.biogroovy.models.Author

/**
 * This class represents a single search result.
 * @param <P> the original POJO type.
 */
public class SearchResult<P> {
	
	/** The title or name of the search result */
	String title;
	
	/** The url for the search result */
	String url;
	
	/** A description of the search result */
	String description;
	
	/** A unique, database-specific identifier */
	String accession;
	
	/** The source of the result.  This may be a Journal, or a web site. */
	String source;
	
	/** A list of tags associated with the search result. */
	private List<String> tags;
	
	private List<Author> authors;
	
	private P originalResult;

	
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List<String> getTags() {
		return tags;
	}

	public void setTags(List<String> tags) {
		this.tags = tags;
	}
	
	public void setResult(P result){
		this.originalResult = result;
	}
	
	public P getResult(){
		this.originalResult;
	}
	

	@Override
	public String toString() {
		return "SearchResult [title=" + title + ", url=" + url
				+ ", description=" + description + ", accession=" + accession
				+ ", tags=" + tags + "]";
	}
	
	
	

}
