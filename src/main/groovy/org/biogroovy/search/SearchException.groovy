package org.biogroovy.search;

/**
 * This class represents an exception that occurs while performing a search.
 *
 */
public class SearchException extends Exception {

	/** */
	private static final long serialVersionUID = 8592449723429814183L;

	/**
	 * Constructor.
	 */
	public SearchException() {
		super();
	}

	/**
	 * Constructor.
	 * 
	 * @param message
	 *            the exception message.
	 * @param cause
	 *            the underlying cause
	 * @param enableSuppression
	 *            enable the suppression of exceptions
	 * @param writableStackTrace
	 *            indicates if the stack trace is writeable
	 */
	public SearchException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public SearchException(String message, Throwable cause) {
		super(message, cause);
	}

	public SearchException(String message) {
		super(message);
	}

	public SearchException(Throwable cause) {
		super(cause);
	}

}
