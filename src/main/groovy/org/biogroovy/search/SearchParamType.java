package org.biogroovy.search;

/**
 * Indicates the type of data used
 *
 */
public enum SearchParamType {
	
	/** A string type. */
	STRING,
	
	/** A date type. */
	DATE,
	
	/** A number type. */
	NUMBER,
	
	/** A boolean type. */
	BOOLEAN;
		

}
