package org.biogroovy.search.validators

import org.biogroovy.search.SearchParam


/**
 * Verifies that the search parameter value is not null.
 */
class NotNullValidator extends AbsSearchParamValidator<SearchParam> {

	@Override
	public boolean apply(SearchParam input) {
		boolean isValid = input.value != null;
		if(!isValid){
			messages.add("The value of the field ${input.name} was null");
		}
		return isValid;
	}

}
