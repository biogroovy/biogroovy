package org.biogroovy.search.validators;

import java.util.ArrayList;
import java.util.List;

import org.biogroovy.search.ISearchParamValidator;

/**
 * This class is a base implementation of the search param validator.
 * 
 * @param <S> the SearchParam or SearchParamSet type to be validated.
 */
abstract class AbsSearchParamValidator<S> implements ISearchParamValidator<S> {
	
	protected List<String> messages = new ArrayList<>();
	
	@Override
	public List<String> getValidationMessages() {
		return messages;
	}
	
}
