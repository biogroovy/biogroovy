/**
 * This package contains search-related classes.
 * To create your own search engine:
 * 
 * <ol>
 *  <li>extend the {@link AbstractSearchEngine}</li>
 *  <li>implement the {@link AbstractSearchEngine#initSearchParams()} method</li>
 *  <li>implement an IResultBuilder inner class to handle the conversion of POJOs to search results</li>
 * </ol>
 */
package org.biogroovy.search;