package org.biogroovy.search.ebi

import org.biogroovy.models.Article
import org.biogroovy.models.MeshHeading
import org.biogroovy.search.AbstractSearchEngine
import org.biogroovy.search.IResultBuilder
import org.biogroovy.search.SearchException
import org.biogroovy.search.SearchParam;
import org.biogroovy.search.SearchResult


/**
 * This SearchEngine implementation searches the EuropePMC database.
 *
 */
public class EuropePMCSearchEngine extends AbstractSearchEngine<SearchResult> {

    /** The name of the search engine */
    private static final String NAME = "EuropePMC";

    /** The URL of the pubmed server */
    private static final String URL = "http://www.ebi.ac.uk/europepmc/webservices/rest/search";


    public EuropePMCSearchEngine(){
        super();
        resultBuilder = new EuropePMCSearchResultBuilder();
    }

    @Override
    String getName() {
        return NAME
    }

    @Override
    String getURLTemplate() {
        return URL;

    }

    @Override
    List<SearchResult> doSearch(Map<String, String> parameters) throws SearchException {
        return null
    }

    @Override
    protected void initSearchParams() {
        SearchParam<String> term = new SearchParam(name:'Query', dataName:'query', isTerm:true);
        term.setHint("The search query");

        SearchParam<String> title = new SearchParam(name:'Title', isSubTerm:true);
        title.setHint("The title of the paper");

        SearchParam<String> pmcId = new SearchParam(name:'PMC ID',dataName:'pmcId', isSubTerm:true);
        pmcId.setHint("The PMC ID");

        SearchParam<String> extId = new SearchParam(name:'External ID', dataName:'ext_id',isSubTerm:true);
        extId.setHint("The external ID");

        SearchParam<String> abs = new SearchParam(name:'Abstract', dataName:'abstract', isSubTerm:true);
        abs.setHint("The abstract");

        SearchParam<String> pubYear = new SearchParam(name:'Publication Year', dataName:'pub_year', isSubTerm:true);
        pubYear.setHint("The publication year");

        SearchParam<String> auth = new SearchParam(name:'Author', dataName:'auth', isSubTerm:true);
        auth.setHint("The author");


        getSearchParameters().addAll(term, title, pmcId, extId, abs, pubYear);
    }

   	/**
	 * This class converts articles into search results.
	 */
	class EuropePMCSearchResultBuilder implements IResultBuilder<Article> {
		
		/**
		 * Constructor.
		 */
		public EuropePMCSearchResultBuilder(){
			
		}
		
		@Override
		SearchResult<Article> convert(Article article){
			SearchResult<Article> result = new SearchResult([title:article.title, description:article.abs]);
			result.url = URL + article.pubmedId
			result.accession = article.pubmedId;
			result.authors = article.authors;
			result.source = article?.journal.toIdString();
			result.tags = new ArrayList();
			article.meshHeadings.each { MeshHeading heading ->
				result.tags.add(heading.descriptorName);
			}
			article.keywords.each{String keyword ->
				result.tags.add(keyword);
			}
			result.setResult(article);
			return result;
		}
	}
}
