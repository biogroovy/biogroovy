package org.biogroovy.search;

import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * This interface represents a SearchEngine.
 * @param <R> the type of the search result
 */
public interface ISearchEngine<R> {

	/**
	 * This method gets the name of the search engine. This is used to present
	 * the user with a list of installed search engines, and for the factory to
	 * create new SearchEngines
	 * 
	 * @return The name of the search engine.
	 */
	String getName();

	/**
	 * This method gets the URL template used in searching. This method should add
	 * all static request parameters (such as database name) to the URL.
	 * 
	 * @return a URL template.
	 */
	String getURLTemplate();

	/**
	 * This method performs the search by binding the parameters to the URL, and
	 * then parsing the results and returning them as a list of SearchResult
	 * objects.
	 * 
	 * @param parameters  a map of parameter names and values.
	 * @return  a list of SearchResult objects.
	 * @throws IOException
	 *             If there is a problem either creating the URL connection or
	 *             reading the data.
	 * @deprecated use doSearch(Search) instead.
	 */
	@Deprecated
	List<R> doSearch(Map<String, String> parameters)
	throws SearchException;

	/**
	 * This method performs a search using the parameters in the Search object, 
	 * and returns a list of SearchResult objects in the Search object.
	 * 
	 * @param search the search to be performed.
	 */
	void doSearch(Search search) throws SearchException;


	/**
	 * This method gets a list of allowable parameters for the search engine.
	 * 
	 * @return a collection of parameters.
	 */
	SearchParamSet getSearchParameters();
	
	/**
	 * This method creates a search object suitable for this search engine.
	 * It clones the search engines parameter set, and initializes the search.
	 * 
	 * @return a fully populated search object.
	 */
	Search createSearch();
	
	/**
	 * This method gets the result builder used to convert results from POJOs
	 * into SearchResult objects.
	 * 
	 * @return the result builder
	 */
	IResultBuilder getResultBuilder();
	
	/**
	 * This method gets the search param map builder.  This builder converts the
	 * search parameters into valid URL parameters.
	 * 
	 * @return a search param map builder
	 */
	ISearchParamMapBuilder getSearchParamMapBuilder();
	
	
}
