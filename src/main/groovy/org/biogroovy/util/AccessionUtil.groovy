package org.biogroovy.util
import java.util.regex.Matcher
import java.util.regex.Pattern

/**
 * This class contains a series of utility methods used to identify an accession.
 * 
 * @author markfortner
 */
class AccessionUtil {

    static Map<String, Pattern> accessionPatternMap = new LinkedHashMap<String, Pattern>();

    static Map<String, String> accessionMap = new LinkedHashMap<String, String>();
    static {
		
		accessionMap.put AccessionType.ALT_COMPLEX_GENOMIC , AccessionType.REGEX_ALT_COMPLEX_GENOMIC;
		accessionMap.put AccessionType.ALT_PROTEIN_RECORD, AccessionType.REGEX_ALT_PROTEIN_RECORD;
		accessionMap.put AccessionType.COMPLETE_GENOMIC, AccessionType.REGEX_COMPLETE_GENOMIC;
		accessionMap.put AccessionType.GENOMIC_REC, AccessionType.REGEX_GENOMIC_REC;
		accessionMap.put AccessionType.INCOMPLETE_GENOMIC, AccessionType.REGEX_INCOMPLETE_GENOMIC;
		accessionMap.put AccessionType.INT_GENOMIC_ASSEMBLY, AccessionType.REGEX_INT_GENOMIC_ASSEMBLY;
		accessionMap.put AccessionType.INT_GENOMIC_ASSEMBLY2, AccessionType.REGEX_INT_GENOMIC_ASSEMBLY2;
		accessionMap.put AccessionType.NON_CODE_TRANS, AccessionType.REGEX_NON_CODE_TRANS;
		accessionMap.put AccessionType.PROTEIN_PROD2, AccessionType.REGEX_PROTEIN_PROD2;
		accessionMap.put AccessionType.PROTEIN_PROD3, AccessionType.REGEX_PROTEIN_PROD3;
		accessionMap.put AccessionType.PROTEIN_PROD4, AccessionType.REGEX_PROTEIN_PROD4;
		accessionMap.put AccessionType.PROTEIN_PROD5, AccessionType.REGEX_PROTEIN_PROD5;
		
		
		
        accessionMap.each(){key, item ->
            accessionPatternMap.put(key, Pattern.compile(item));
        }
    }



    /**
     * This method identifies the type of accession.
     */
    public static String getAccessionType(String accession){
		for(Map.Entry entry : accessionPatternMap.entrySet()){
			
            Matcher m = entry.value.matcher(accession);
            if (m.matches()){
                return entry.key;
            }
        }
    }

    /**
     * This method determines if the accession is of a specific accession type.
     */
    public static boolean isAccessionOfType(String accessionType, String accession){
        Pattern p = accessionPatternMap.get(accessionType);
        
        if (p == null){
            return false;
        }
        
        Matcher m = p.matcher(accession);
        return m.matches();
    }



    /**
     * This method gets the regular expression for the given accession type.
     * @param accessionType  The AccessionType constant.
     */
    public static String getRegex(String accessionType){
        return accessionMap.get(accessionType);
    }

    /**
     * This method gets the regular expression pattern for the given accession type.
     * @param accessionType  The AccessionType constant.
     */
    public static Pattern getRegexp(String accessionType){
        return accessionPatternMap.get(accessionType);
    }
	
}

