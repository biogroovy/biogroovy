package org.biogroovy.io;

import groovy.util.logging.Slf4j

import java.text.SimpleDateFormat

import javax.xml.namespace.QName
import javax.xml.xpath.XPathConstants
import javax.xml.xpath.XPathFactory


/**
 * This is the base class for a number of types of readers.
 * 
 * @param <P> the return POJO (model) datatype.
 */
@Slf4j
public abstract class AbsReader<P, N> extends AbsFetcher<P> implements IReader<P, N>{
	
	/** The formatter used to convert the date from a string into a date. */
	protected SimpleDateFormat dateFormatter = new SimpleDateFormat("YYYY-MM-dd");


    /**
     * This method reads an input stream containing the XML for a protein or transcript.
     * The XML is assumed to be the same format as that returned from the EUtils
     * RESTful web service.
     * @param inputStream  The inputstream containing the XML record.
     */
    public abstract P read(InputStream inputStream) throws IOException;


    /**
     * This method reads a file containing the XML for an object.
     * @param file  The file containing the XML record.
     */
    public P readFile(File file) throws IOException{
		return read(new FileInputStream(file));
	}


    /**
     * This method reads an input stream containing the XML for a list of objects.
     */
    public abstract List<P> readList(InputStream inputStream) throws IOException;




    /**
     * This method reads a file containing the XML for a list of objects.
     * @param file The file containing the list o XML records.
     */
    public List<P> readList(File file) throws IOException{
        return readList(new FileInputStream(file));
    }






}

