package org.biogroovy.io;

/**
 * This enumeration describes the types of XML nodes.
 * 
 *
 */
public enum NodeType {
	
	/** An integer node. */
	INTEGER,
	
	/** A long node. */
	LONG,
	
	/** A double node. */
	DOUBLE,
	
	/** A string node. */
	STRING,
	
	/** A list of nodes. */
	LIST
}
