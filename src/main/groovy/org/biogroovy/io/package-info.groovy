/**
 * This package contains IO-related classes. The classes in this package are designed
 * specifically to read and write (serialize) model-related classes (i.e. the classes
 * found in the <b>org.biogroovy.model</b> package.  The main entry-point for the package
 * is the ISerializer interface, and the SerializerFactory.  The SerializerFactory is designed
 * to allow users to register Serializers with it.  
 * 
 * The {@link IFetcher} is the main interface for fetching data from remote databases using RESTful services.
 * The {@link AbsReader} is the base-class for a number of existing XML-based readers. <p/>
 * The {@link AbsTransformer} is the base-class for all XSLT-based transformers.<p/>
 * The {@link AbsJsonReader} is the base-class for fetching JSON data from RESTful services. <p/>
 * The {@link AbsXmlReader} contains methods for parsing XML using the Groovy XML parser. <p/>
 * The {@link AbsSlurper} is the base-class for parsing XML using the XML Slurper. <p/>
 * 
 * 
 */
package org.biogroovy.io;