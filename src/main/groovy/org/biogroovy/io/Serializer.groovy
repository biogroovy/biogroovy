package org.biogroovy.io;

import org.biogroovy.models.IModel;

/**
 * This class serves as a "holder" for a reader and writer.
 * 
 * @param <M>
 *            the model to be serialized
 */
public class Serializer<M extends IModel, N> implements ISerializer<M, N> {

	/** The name of the type being serialized. */
	private String name;
	
	/** The reader. */
	private IReader<M, N> reader;
	
	/** The writer. */
	private IWriter<M> writer;

	/**
	 * Constructor.
	 */
	public Serializer() {
		
	}

	/**
	 * Constructor.
	 * 
	 * @param name
	 *            the name of the serializer.
	 * @param reader
	 *            the reader
	 * @param writer
	 *            the writer.
	 */
	public Serializer(String name, IReader<M, N> reader, IWriter<M> writer) {
		this.name = name;
		this.reader = reader;
		this.writer = writer;
	}

	@Override
	public String getModelName() {
		return name;
	}

	public void setModelName(String name) {
		this.name = name;
	}

	@Override
	public IReader<M, N> getReader() {
		return reader;
	}

	@Override
	public IWriter<M> getWriter() {
		return writer;
	}

}
