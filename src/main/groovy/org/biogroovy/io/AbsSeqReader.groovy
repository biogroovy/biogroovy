package org.biogroovy.io;

/**
 * This class serves as the template for XML-based sequence readers.
 *
 */
public abstract class AbsSeqReader<P> extends AbsXmlReader<P> {
    
    /**
     * This method parses all references to external databases.
     * @param root the root of the record.
     * @param sequence the sequence object being annotated.
     */
    protected abstract void parseDbReferences(def root, P sequence);

    /**
     * This method parses the articles.
     * @param root the root of the record.
     * @param sequence the sequence object being annotated.
     */
    protected abstract void parseArticles(def root, P sequence);

    /**
     * This method parses the GO elements associated with the sequence.
     * @param root the root of the record.
     * @param sequence the sequence object being annotated.
     */
    protected abstract void parseGo(def root, P sequence);


}
