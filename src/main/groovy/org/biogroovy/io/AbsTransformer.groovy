package org.biogroovy.io

import javax.xml.transform.TransformerFactory
import javax.xml.transform.stream.StreamResult
import javax.xml.transform.stream.StreamSource


/**
 * This class provides methods used to transform input streams using an XSLT.
 *
 */
class AbsTransformer {
	
	
	/**
	 * This method transforms a source document using an XSLT and puts the results into the outputstream.
	 * @param inputStream	The source document
	 * @param xslt			The XSLT used to transform the document
	 * @param outputStream	The outputstream containing the results
	 */
	public void transform(InputStream inputStream, String xslt, OutputStream outputStream){
		def factory = TransformerFactory.newInstance()
		def transformer = factory.newTransformer(new StreamSource(new StringReader(xslt)))
		transformer.transform(new StreamSource(new InputStreamReader(inputStream)), new StreamResult(outputStream))
	}
	
	/**
	 * This method transformsa s
	 * This method transforms a source document using an XSLT and puts the results into the outputstream.
	 * @param inputStream	The source document
	 * @param xslt			The input stream for the XSLT used to transform the document
	 * @param outputStream	The outputstream containing the results
	 */
	public void transform(InputStream inputStream, InputStream xslt, OutputStream outputStream){
		def factory = TransformerFactory.newInstance()
		def transformer = factory.newTransformer(new StreamSource(new InputStreamReader(xslt)))
		transformer.transform(new StreamSource(new InputStreamReader(inputStream)), new StreamResult(outputStream))
	}
	
	

}
