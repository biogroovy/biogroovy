package org.biogroovy.io;

import org.biogroovy.models.IModel;

/**
 * Implementations of this interface are responsible for readering and writing
 * files containing models.
 * 
 * @param <M>
 *            The model type (i.e. Gene, Protein, Article, etc).
 * @param <N> The node type
 */
public interface ISerializer<M extends IModel, N> {

	/**
	 * This method gets the name of the model that the serializer works on. This
	 * is used to register new implementations of the serializer with the
	 * SerializerFatory.
	 * 
	 * @return
	 */
	String getModelName();
	
	/**
	 * Gets the registered reader.
	 * @return the reader implementation
	 */
	IReader<M, N> getReader();
	
	/**
	 * Gets the registered writer.
	 * @return the writer implementation.
	 */
	IWriter<M> getWriter();

}
