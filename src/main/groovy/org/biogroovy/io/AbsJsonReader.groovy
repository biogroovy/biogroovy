package org.biogroovy.io;

import java.util.Map;

/**
 * This class is responsible for defining the methods required to convert JSON-based
 * content into POJO-based models.
 * 
 *
 * @param <P> the POJO.
 */
public abstract class AbsJsonReader<P> extends AbsReader<P, Map>{


	
	/**
	 * This method is responsible for parsing a map of JSON property values
	 * into a POJO object.
	 * @param seq the sequence object
	 * @param jsonPropMap the map of JSON properties
	 */
	public abstract void parse(P seq, Map jsonPropMap);


}
