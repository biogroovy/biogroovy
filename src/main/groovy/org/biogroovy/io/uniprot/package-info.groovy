/**
 * This package contains classes for handling UniProt data.
 */
package org.biogroovy.io.uniprot;
