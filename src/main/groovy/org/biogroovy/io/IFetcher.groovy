package org.biogroovy.io;


/**
 * Implementations of this interface are responsible for fetching a single instance
 * of a model object from a RESTful service.
 *
 * @param < P >  the model object type (i.e. Gene, Protein, Article, etc)
 */
public interface IFetcher<P> {

    /**
     * This method reads the sequence object (a Protein or Transcript) object
     * from using NCBI's EUtils RESTful web service and returns the appropriate object.
     * @param id The ID of the object being downloaded.
     * @return the model object
     * @throws IOException if there is a problem retrieving the object
     */
    P fetch(String id) throws IOException;
	

    /**
     * Binds the ID to the URL template and returns a URL.
     * @param id the ID of the object to be fetched.
     * @param paramMap an optional parameter containing the userId or key used
     *    to access a RESTful service.
     * @return a fully populated URL.
     */
    URL getUrl(String id, Map<String, String> paramMap);

    /**
     * This method gets the name of the database used by the Fetcher.
     * @return a String containing the name of the database
     */
    String getDatabaseName();

    /**
     * This method fetches the XML record for a given id, and writes the contents out as a file.
     * @param id The id of the record to be fetched.
     * @param outfile the output file. If not specified, the file will be written out to
     *                  ${user.home}/${database_name}_${id}.xml
     */
    void fetchAsFile(String id, File outfile) throws IOException;

    /**
     * This method retrieves a list of model objects.
     * @param ids a comma-delimited list of model objects to be retrieved.
     * @return a list of model objects.
     * @throws IOException if there is a problem retrieving the objects.
     */
    List<P> fetchAll(String ids) throws IOException;

    /**
     * This method fetches a list of records, and outputs the resulting XML files
     * to the specified directory.  Files use the
     * @param outDir if null, the output directory is assumed to be the ${user.home}
     * @param idList a comma-delimited list of ids
     */
    void fetchAsFiles(File outDir, String idList) throws IOException;

	IFetcher<P> getNewInstance();

}
