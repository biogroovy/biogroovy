package org.biogroovy.io.local

import groovy.xml.MarkupBuilder;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Collection;

import org.biogroovy.io.AbsWriter;
import org.biogroovy.models.Journal

/**
 * This class is responsible for writing Journal metadata out to the file system. 
 *
 */
class JournalWriter extends AbsWriter<Journal> {
	
	/**
	 * Constructor.
	 */
	public JournalWriter(){
		super(".journal.xml");
	}


	@Override
	public void write(Journal journal, OutputStream out) throws IOException {
		MarkupBuilder builder = new MarkupBuilder(new PrintWriter(out));
		builder.mkp.xmlDeclaration(version:"1.0", encoding:"utf-8");
		
		createXML(builder, journal);
		
	}

	@Override
	public void writeCollection(Collection<Journal> journalList, OutputStream out)
			throws IOException {
				
			
		MarkupBuilder builder = new MarkupBuilder(new PrintWriter(out));
		builder.mkp.xmlDeclaration(version:"1.0", encoding:"utf-8");
		builder.journals{
			journalList.each{Journal journal ->
				createXML(builder, journal)
			}
		}
		
	}

	@Override
	protected void createXML(MarkupBuilder builder, Journal pojo) {
		builder.journal(title: pojo.title, issn:pojo.issn, 
			'pub-date':dateFormatter.format(pojo.publicationDate), 
			'start-page':pojo.startPage,
			'end-page':pojo.endPage,
			volume:pojo.volume,
			issue:pojo.issue,
			'image-url': pojo.imageUrl,
			'site-url': pojo.siteUrl,
			'rss-url':pojo.rssUrl
			
			)
		
	}

}
