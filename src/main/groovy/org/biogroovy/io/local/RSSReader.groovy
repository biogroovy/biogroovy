package org.biogroovy.io.local

import groovy.util.slurpersupport.GPathResult
import org.biogroovy.io.AbsReader
import org.biogroovy.io.IFetcher
import org.biogroovy.models.RSSFeed
import org.w3c.dom.Element
import org.w3c.dom.Node

import javax.xml.parsers.DocumentBuilder
import javax.xml.parsers.DocumentBuilderFactory

/**
 * 
 *
 */
class RSSReader extends AbsReader<RSSFeed, GPathResult> {


	@Override
	public RSSFeed read(InputStream inputStream) throws IOException {
		XmlSlurper slurper = new XmlSlurper();
        GPathResult result = slurper.parse(inputStream)
		RSSFeed feed = new RSSFeed();
        parse(feed, result);
        return feed;
	}

	@Override
	public void parse(RSSFeed feed, GPathResult node) {

		feed.iconUrl = node.@iconUrl
		feed.url = node.@url
		feed.name = node.@name
		feed.description = node.description[0].text();
		node.tag.each { tagNode ->
			feed.tagSet.add(tagNode.@name);
		}
	}

	@Override
	public List<RSSFeed> readList(InputStream inputStream) throws IOException {
		GPathResult xml = new XmlSlurper().parse(inputStream);
		List<RSSFeed> feedList = new ArrayList<>();
		
		xml.feeds.rss.each{GPathResult rssNode ->
			RSSFeed feed = new RSSFeed()
			parse(feed, rssNode);
			feedList.add(feed)
		}
		
		return feedList;
	}


	@Override
	public RSSFeed fetch(String id) throws IOException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public URL getUrl(String id, Map<String, String> paramMap) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getDatabaseName() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<RSSFeed> fetchAll(String id) throws IOException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public IFetcher<RSSFeed> getNewInstance() {
		return new RSSReader();
	}

}
