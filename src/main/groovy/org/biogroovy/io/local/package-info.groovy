/**
 * This package contains classes used to read and write {@link org.biogroovy.models.Article}, Journal, Sequence, and Search
 * metadata to/from local files.
 */ 
package org.biogroovy.io.local;
