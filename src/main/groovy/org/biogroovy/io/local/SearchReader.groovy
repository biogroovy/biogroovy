package org.biogroovy.io.local

import java.io.IOException;
import java.net.URL;
import java.util.List;

import org.biogroovy.io.AbsReader;
import org.biogroovy.io.IFetcher;
import org.biogroovy.search.Search
import org.biogroovy.search.SearchParam
import org.biogroovy.search.SearchParamType
import org.w3c.dom.Node

/**
 * 
 *
 */
class SearchReader extends AbsReader<Search, Node> {

	@Override
	public Search read(InputStream inputStream) throws IOException {
		def xml = new XmlParser().parse(stream);
		Search search = new Search();
		parse(search, xml);
		return search;
	}

	@Override
	public void parse(Search search, Node node) {
			
		search.name = xml.@name;
		
		search.setLastExecutedString( xml.@lastExecuted);
		search.searchEngineName = xml.@searchEngineName;
		
		xml.searchParams.searchParam.each { param ->
			SearchParam searchParam = new SearchParam(name:param.@name, dataName:param.@dataName, isEditable:param.@isEditable, isRequired:param.@isRequired, isSubTerm:param.@isSubTerm, isTerm:param.@isTerm, hint:param.@hint, value:param.@value);
			searchParam.paramType = SearchParamType.valueOf(param.@paramType);
			
			search.searchParams.addAll(searchParam);
		}
		
	}

	@Override
	public List<Search> readList(InputStream inputStream) throws IOException {
		List<Search> searchList = new ArrayList<>();
		Search search = read(inputStream);
		searchList.add(search);
	}


	@Override
	public Search fetch(String id) throws IOException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public URL getUrl(String id, Map<String, String> paramMap) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getDatabaseName() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Search> fetchAll(String id) throws IOException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public IFetcher<Search> getNewInstance() {
		return new SearchReader();
	}

}
