package org.biogroovy.io.local

import groovy.xml.MarkupBuilder;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Collection;

import org.biogroovy.io.AbsWriter;
import org.biogroovy.models.RSSFeed

/**
 * This class is responsible for writing an RSS bookmark out to a file.
 *
 */
class RSSWriter extends AbsWriter<RSSFeed> {
	
	public RSSWriter(){
		super(".rss.xml");
	}

	@Override
	public void write(RSSFeed seq, OutputStream out) throws IOException {
		MarkupBuilder builder = new MarkupBuilder(new PrintWriter(out));
		builder.mkp.xmlDeclaration(version:"1.0", encoding:"utf-8");
		
		createXML(builder, seq);

	}

	@Override
	public void writeCollection(Collection<RSSFeed> seqs, OutputStream out)
	throws IOException {

		MarkupBuilder builder = new MarkupBuilder(new PrintWriter(out));
		builder.mkp.xmlDeclaration(version:"1.0", encoding:"utf-8");
		builder.feeds{
			seqs.each{ seq ->
				createXML(builder, seq)
			}
		}
	}

	@Override
	protected void createXML(MarkupBuilder builder, RSSFeed pojo) {
		builder.rss(name:pojo.name, url:pojo.url, iconUrl:pojo.iconUrl, lastRead:pojo.getDateAsString()){
			description(pojo.description)
			pojo.tagSet.each{tagStr ->
				tag(name:tagStr)
			}
		}
	}
}
