package org.biogroovy.io.local
import groovy.xml.MarkupBuilder

import org.biogroovy.models.Article
import org.biogroovy.models.Gene
import org.biogroovy.models.GeneOntology
import org.biogroovy.models.Protein
import org.biogroovy.models.Transcript
import org.biogroovy.io.AbsSeqWriter

/**
 * This class is responsible for writing Articles out as XML.
 *
 */
public class GeneWriter extends AbsSeqWriter<Gene>{

	/**
	 * Constructor.
	 */
	public GeneWriter(){
		super("gene.xml");
	}


	@Override
	public void write(Gene gene, OutputStream out) throws IOException {
		PrintWriter writer = new PrintWriter(out);
		MarkupBuilder xml = new MarkupBuilder(writer);
		createXML(xml, gene);
	}


	@Override
	public void writeCollection(Collection<Gene> geneList, OutputStream out)
	throws IOException {

		PrintWriter writer = new PrintWriter(out);
		MarkupBuilder xml = new MarkupBuilder(writer);
		xml.geneList{
			geneList.each {Gene gene ->
				createXML(xml, gene);
			}
		}

	}

	@Override
	protected void createXML(MarkupBuilder xml, Gene gene){
		xml.gene(entrezGeneId:gene.entrezGeneId, name:gene.name, symbol:gene.symbol, description:gene.description){
			gene.synonyms.each{
				synonym(name:it)
			}
			
			sequence(){gene.sequence}
			
			gene.transcripts.each{Transcript trans ->
				transcript(modelId:trans.modelId)
			}
			
			gene.proteins.each{Protein prot ->
				protein(modelId:prot.modelId)
			}
			
			gene.goComponentList.each {GeneOntology goTerm ->
				go(goId:goTerm.goId, name:goTerm.name, type:goTerm.type)
			}
			gene.goFunctionList.each {GeneOntology goTerm ->
				go(goId:goTerm.goId, name:goTerm.name, type:goTerm.type)
			}
			gene.goProcessList.each {GeneOntology goTerm ->
				go(goId:goTerm.goId, name:goTerm.name, type:goTerm.type)
			}
			
			gene.articles.each { Article art ->
				article(modelId:art.modelId)
			}
		}
	}
}
