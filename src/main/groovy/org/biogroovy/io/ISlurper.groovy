package org.biogroovy.io;

import groovy.util.slurpersupport.NodeChild;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

/**
 * This interface describes the methods used by XmlSlurper to parse data.
 * @param <P> the POGO
 * @param <N> the node type (could be a NodeChild if using an XmlSlurper, or a Map if using a JsonSlurper)
 */
public interface ISlurper<P,N> {

	/**
	 * This method reads an input stream containing the XML or JSON for a protein or transcript.
	 * @param inputStream  The input stream containing the XML record.
	 * @throws IOException if there is a problem reading the POGO.
	 */
	 P read(InputStream inputStream) throws IOException;

	 /**
	  * This method is responsible for parsing a single node and returning a model object.
	  * @param node the node to be parsed.
	  * @param model the model object.
	  */
	 void parse(P model, N node);

	 /**
	  * This method reads an input stream containing the XML for a list of objects.
	  * @param inputStream the input stream containing the REST response.
	  * @return a list of POGOs containing the results.
	  */
	  List<P> readList(InputStream inputStream) throws IOException;

}
