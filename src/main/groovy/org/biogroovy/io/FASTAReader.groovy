
package org.biogroovy.io

import groovy.util.logging.Slf4j

import java.util.Map.Entry

import org.biojava.bio.*
import org.biojava.bio.seq.*
import org.biojava.bio.seq.io.*
import org.biojava3.core.sequence.*
import org.biojava3.core.sequence.io.*;
import org.biojava3.core.sequence.template.*

/**
 * This class reads a FASTA file.
 * 
 */
@Slf4j
public class FASTAReader{

	public void read(String filename){
		try {
			//prepare a BufferedReader for file io
			BufferedReader br = new BufferedReader(new FileReader(filename));

			String format = args[1];
			String alphabet = args[2];

			LinkedHashMap<String, ProteinSequence> a = FastaReaderHelper.readFastaProteinSequence(new File(filename));
			//FastaReaderHelper.readFastaDNASequence for DNA sequences

			for ( Entry<String, ProteinSequence> entry : a.entrySet() ) {
				System.out.println( entry.getValue().getOriginalHeader() + "=" + entry.getValue().getSequenceAsString() );
			}
		}
		catch (FileNotFoundException ex) {
			log.error(ex.getMessage(), ex);
		}

	}

}
