/**
 * This package contains classes used to read clinical trial data.
 */
package org.biogroovy.io.clinicaltrials;
