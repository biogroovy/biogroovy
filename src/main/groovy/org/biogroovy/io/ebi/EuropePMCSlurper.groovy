package org.biogroovy.io.ebi

import groovy.json.JsonSlurper
import org.biogroovy.io.ISlurper
import org.biogroovy.models.Article
import org.biogroovy.models.Author
import org.biogroovy.models.Journal

/**
 * This class is responsible for parsing an article from the EuropePMC JSON response.
 */
class EuropePMCSlurper implements ISlurper<Article, Map> {


    @Override
    Article read(InputStream inputStream) throws IOException {

        if(inputStream == null){
            throw new IOException("Null inputstream detected");
        }

        JsonSlurper slurper = new JsonSlurper();
        Map root = (Map)slurper.parse(inputStream);
        Article article = new Article();
        parse(article, root.resultList.result[0]);


        return article
    }

    @Override
    void parse(Article article, Map result) {
        article.pubmedId = result.pmid
        article.pmcId = result.pmcid
        article.title = result.title
        article.abs = result.abstractText

        parseAuthors(article, result.authorList)
        parseJournal(article, result.journalInfo)

        if (result?.pubType?.contains('review')){
            article.isReview = true
        }
    }

    @Override
    List<Article> readList(InputStream inputStream) throws IOException {
        List<Article> articleList = new ArrayList<>();
        JsonSlurper slurper = new JsonSlurper();
        Map root = (Map)slurper.parse(inputStream);

        root.resultList.result.each{it ->
            Article article = new Article()
            parse(article, (Map)it)

            articleList.add(article)
        }

        return articleList;
    }

    /**
     * This method is responsible for parsing a Journal entry out of the article JSON object.
     * @param article the article to be populated
     * @param journalInfo the json object containing the journal info
     */
    private void parseJournal(Article article, def journalInfo){
        article.journal = new Journal();
        article.journal.title = journalInfo.journal.title
        article.journal.issn = journalInfo.journal.essn

    }

    /**
     * This method is responsible for parsing the authors from a JSON object
     * @param article the article to be populated
     * @param authorList the list of authors to be parsed.
     */
    private void parseAuthors(Article article, def authorList){
        authorList.author.each{auth ->
            Author author = new Author()
            author.firstname = auth.firstName
            author.lastname = auth.lastName
            author.initials = auth.initials


            article.authors.add(author)
        }
    }


}
