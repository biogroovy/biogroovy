/**
 * This package contains classes used to interface with RESTful web services from 
 * the European Bioinformatics Institute (EBI).
 */
package org.biogroovy.io.ebi;