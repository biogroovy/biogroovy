package org.biogroovy.io;

import org.biogroovy.models.IModel


/**
 * This factory is responsible for creating new instances of serializers.
 *
 */
public class SerializerFactory<M extends IModel, N> {

	private Map<String, ISerializer<?,?>> serializerMap = new HashMap<>();

	/**
	 * Constructor.
	 */
	private SerializerFactory() {
	}

	/**
	 * Adds a serializer to the factory.
	 * 
	 * @param serializer
	 *            the serializer to be added.
	 */
	public void addSerializer(ISerializer<M, N> serializer) {
		serializerMap.put(serializer.getModelName(), serializer);
	}

	/**
	 * Gets an instance of the serializer.
	 * 
	 * @param pojoType
	 *            the name of the pojo(model) type.
	 * @return the serializer or null if it has not been registered.
	 */
	public ISerializer<M, N> getInstance(String pojoType) {
		return serializerMap.get(pojoType);
	}

	/**
	 * Sets the serializer map.
	 * 
	 * @param serializerMap
	 */
	public void setSerializers(Map<String, ISerializer<?, ?>> serializerMap) {
		this.serializerMap = serializerMap;
	}

	/**
	 * Determines if there are any serializers registered.
	 * 
	 * @return true if there are any serializers registered; false, otherwise.
	 */
	public boolean isEmpty() {
		return serializerMap.isEmpty();
	}

}
