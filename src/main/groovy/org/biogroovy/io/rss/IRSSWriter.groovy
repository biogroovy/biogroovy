package org.biogroovy.io.rss;

import java.io.IOException;
import java.io.Writer;

import org.biogroovy.models.RSSFeed;

/**
 * Implementations of this interface are responsible for writing RSSFeeds out to a given destination.
 *
 * @param <F> the RSS Feed type
 * @param <O> the output file/stream type
 */
public interface IRSSWriter<F, O> {

	/**
	 * This method writes a single feed out to a destination
	 * @param feed the RSS feed
	 * @param output the output file.
	 * @throws IOException thrown if there was a problem writing the feed out
	 */
	void write(F feed, O output) throws IOException

	/**
	 * This method writes multiple feeds out to the 
	 * @param output the output file/stream
	 * @param feeds the feeds to be written out
	 * @throws IOException if there is a problem writing the feeds out
	 */
	void writeFeeds(O output, F... feeds) throws IOException;

	/**
	 * This method writes multiple feeds out to the
	 * @param output the output file/stream
	 * @param feeds the feeds to be written out
	 * @throws IOException if there is a problem writing the feeds out
	 */
	void writeFeeds(O output, List<F> feeds) throws IOException;
}
