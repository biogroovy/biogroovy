/**
 * This package contains classes used to read journals and articles from RSS feeds.
 */
package org.biogroovy.io.rss;