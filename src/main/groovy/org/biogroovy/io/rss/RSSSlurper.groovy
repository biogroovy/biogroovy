package org.biogroovy.io.rss

import groovy.util.logging.Slf4j

import org.biogroovy.models.Article
import org.biogroovy.models.Author
import org.biogroovy.models.MediaMetadata
import org.biogroovy.models.RSSFeed

import com.sun.syndication.feed.module.mediarss.MediaEntryModule
import com.sun.syndication.feed.module.mediarss.types.MediaContent
import com.sun.syndication.feed.module.mediarss.types.Metadata
import com.sun.syndication.feed.module.mediarss.types.UrlReference
import com.sun.syndication.feed.synd.SyndEntry
import com.sun.syndication.feed.synd.SyndFeed
import com.sun.syndication.feed.synd.SyndPerson
import com.sun.syndication.io.SyndFeedInput

@Slf4j
class RSSSlurper implements IRSSSlurper<RSSFeed>{
	
	@Override
	public RSSFeed readSource(InputStream inputStream) throws IOException {
		
		SyndFeedInput input = new SyndFeedInput();
		SyndFeed feed = input.build(new InputStreamReader(inputStream));
		RSSFeed rssFeed = new RSSFeed(
			name: feed.getTitle(), 
			description: feed.getDescription(),
			url: feed.getLink(),
			iconUrl: feed.getImage()?.url
		);

		
		Article article = null;
		
		for(SyndEntry entry : feed.getEntries()){
		
			article = new Article();
			article.title = entry.title;
			article.url = entry.uri;
			article.abs = entry.description.value;
			article.dateCreated = entry.getPublishedDate();
			
			// parse the categories
			entry.getCategories().each{it ->
				article.keywords.add(it.name);
			}
		
			// parse the media module
			parseMedia(article, entry)
			
			// parse the authors
			parseAuthors(article, entry)
			
			rssFeed.articleList.add(article);
			
			log.debug(article.toString());
		
		}
		
		return rssFeed;
	}
	
	
	@Override
	void parseMedia(Article article, SyndEntry entry){
		MediaEntryModule module = entry.getModule("http://search.yahoo.com/mrss/");
		
		if(module != null){
			MediaMetadata media = null;
			module.mediaContents.each{MediaContent content ->
				UrlReference ref = content.reference;
				Metadata metadata = content.getMetadata();
				
				String contentType = content.type;
				String url = ref?.getUrl().toString();
				if(contentType == null && url != null){
					if(url.endsWith("jpg") || url.endsWith("jpeg")){
						contentType = "image/jpeg"	
					}else if (url.endsWith("png")){
						contentType = "image/png"
					}else if(url.endsWith("gif")){
						contentType = "image/gif"
					}
				}
				
				
				media = new MediaMetadata(
					url: url,
					description: metadata.description,
					height:content.height,
					width: content.height,
					mediaType: contentType,
					title: content.metadata.title
				)
				article.mediaList.add(media);
			}
		}
	}

	@Override
	void parseAuthors(Article article, SyndEntry entry){
		
		entry.getAuthors().each{ SyndPerson author ->
			Author auth = new Author();
			auth.lastname = author.getName();
			article.authors.add(auth)
		}

	}


}
