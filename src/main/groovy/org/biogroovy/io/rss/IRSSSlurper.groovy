package org.biogroovy.io.rss;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import org.biogroovy.models.Article;
import org.biogroovy.models.Author;

import com.sun.syndication.feed.synd.SyndEntry;
import com.sun.syndication.feed.synd.SyndPerson;

/**
 * Implementations of this interface read an RSS data source and provide article and source information. 
 * When calling implementations of this source, call the readSource method first to initialize the source, 
 * and articles, then call the getSource, and getArticles methods.
 * 
 * @param <F> The RSS feed type
 */
public interface IRSSSlurper<F> {
	
	/**
	 * This method reads an input stream and returns a news feed.
	 * @param inputStream
	 * @return
	 * @throws IOException
	 */
	F readSource(InputStream inputStream) throws IOException


	/**
	 * This method parses the media metadata from the article.
	 * @param article the article to be populated
	 * @param entry the SyndEntry containing the article metadata.
	 */
	void parseMedia(Article article, SyndEntry entry)

	/**
	 * This method parses the authors from the article
	 * @param article the article to be populated
	 * @param entry  the SyndEntry containing the article metadata
	 */
	void parseAuthors(Article article, SyndEntry entry)
	


}
