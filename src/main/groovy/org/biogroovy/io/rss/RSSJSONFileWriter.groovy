package org.biogroovy.io.rss

import groovy.json.JsonBuilder
import groovy.util.logging.Slf4j

import org.biogroovy.models.Article
import org.biogroovy.models.MediaMetadata
import org.biogroovy.models.RSSFeed

@Slf4j
class RSSJSONFileWriter implements IRSSWriter<RSSFeed, Writer> {

	@Override
	public void writeFeeds(Writer writer, RSSFeed... rssFeeds) throws IOException{
		
		JsonBuilder builder = new JsonBuilder();
		builder{
			"feeds" rssFeeds.collect {RSSFeed rssFeed ->
				[
					"name": rssFeed.name,
					"url": rssFeed.url,
					"description": rssFeed.description,
					"imageUrl": rssFeed.iconUrl,
					"articles": rssFeed.articleList.collect {Article article ->
						[
							"title": article.title,
							"abstract": article.abs,
							"createDate": article.dateCreated,
							"keywords": article.getKeywords(),
							"media": article.mediaList.collect {MediaMetadata meta ->
								[
									title: meta.title,
									description:  meta.description,
									url: meta.url,
									mediaType: meta.mediaType,
									height: meta.height,
									width: meta.width,
								]
							}
						]
					}

				]
			}
		}
		
		builder.writeTo(writer)
		writer.flush();
		writer.close();
	}

	@Override
	public void writeFeeds(Writer writer, List<RSSFeed> rssFeeds)
	throws IOException {

		JsonBuilder builder = new JsonBuilder();
		builder{
			feeds rssFeeds.collect {RSSFeed rssFeed ->
				[
					"name": rssFeed.name,
					"url": rssFeed.url,
					"description": rssFeed.description,
					"imageUrl": rssFeed.iconUrl,
					"articles": rssFeed.articleList.collect {Article article ->
						[
							"title": article.title,
							"abstract": article.abs,
							"createDate": article.dateCreated,
							"keywords": article.getKeywords(),
							"media": article.mediaList.collect {MediaMetadata meta ->
								[
									title: meta.title,
									description:  meta.description,
									url: meta.url,
									mediaType: meta.mediaType,
									height: meta.height,
									width: meta.width,
								]
							}
						]
					}

				]
			}
		}
		
		builder.writeTo(writer)
		writer.flush();
		writer.close();
	}



	@Override
	public void write(RSSFeed rssFeed, Writer writer) throws IOException {
		writeFeeds(writer, rssFeed)
	}
	

}
