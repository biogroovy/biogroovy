package org.biogroovy.io

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

/**
 * Implementations of this interface are responsible for reading files containing models.
 * @param <P> the return POJO (model) datatype.
 * @param <N> the type of node that is parsed to return a POJO.
 */
interface IReader<P, N> {
	

	/**
	 * This method reads an input stream containing the XML for a protein or transcript.
	 * The XML is assumed to be the same format as that returned from the EUtils
	 * RESTful web service.
	 * @param inputStream  The inputstream containing the XML record.
	 */
	 P read(InputStream inputStream) throws IOException;

	 /**
	  * This method is responsible for parsing a single node and returning a model object.
	  * @param node the node to be parsed.
	  * @return the model object.
	  */
	  void parse(P model, N node);

	 /**
	  * This method reads an input stream containing the XML for a list of objects.
	  * @param inputStream the inputstream containing the REST response.
	  * @return a list of POJOs containing the results.
	  */
	  List<P> readList(InputStream inputStream) throws IOException;
 
}
