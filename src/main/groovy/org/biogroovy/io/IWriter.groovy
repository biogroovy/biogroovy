package org.biogroovy.io;


import java.io.OutputStream;
import java.nio.file.Path;
import java.util.Collection;

import org.biogroovy.models.AbsSequence;
import org.biogroovy.models.IModel;

/**
 * This interface describes the methods required to output a file or collection
 * of files that are Sequence-related.
 * 
 *
 * @param <P>
 *            the data type for the model (anything in the org.biogroovy.model package).
 */
public interface IWriter<P extends IModel> {


	/**
	 * This method is responsible for writing the contents of
	 * 
	 * @param seq
	 *            the sequence object.
	 * @param out
	 *            the output stream
	 * @throws IOException
	 *             if there is a problem writing the file out.
	 */
	void write(P seq, OutputStream out) throws IOException;

	/**
	 * This method is responsible for writing a collection of sequences out into
	 * a single file.
	 * 
	 * @param seqs
	 *            The sequences to be written out.
	 * @param out
	 *            the output stream.
	 * @throws IOException
	 *             if there is a problem writing the file.
	 */
	void writeCollection(Collection<P> seqs, OutputStream out)
	throws IOException;


	/**
	 * This method is responsible for writing out each sequence in the
	 * collection into its own file.
	 *
	 * @param seqs
	 *            the sequences to be written out
	 * @param outputDir
	 *            the directory where the files will appear.
	 * @param autoCreateDir
	 *            a flag indicating whether or not the directory should be
	 *            created if it doesn't exist. If set to false, and the
	 *            directory doesn't exist, an IOException must be thrown.
	 * @throws IOException
	 *             if there is a problem exporting the files.
	 */
	void writeCollectionSeparately(Collection<P> seqs, Path outputDir,
	boolean autoCreateDir) throws IOException;
}
