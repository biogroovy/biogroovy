/**
 * This package contains classes used to fetch Journal and Article metadata
 * from the JournalTOCs RESTful service.  This service provides journal table-of-contents
 * for scientific journals.
 */
package org.biogroovy.io.journaltocs;
