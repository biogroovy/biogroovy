/**
 * This package contains classes designed to fetch data from the MyGene.info web service
 * created by the <a href="http://www.sulab.org">Su Lab</a> at Scripps.
 */
package org.biogroovy.io.sulab;
