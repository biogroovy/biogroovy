package org.biogroovy.io;

import java.io.IOException;
import java.nio.file.Path;
import java.util.Collection;
import org.biogroovy.models.ISequence;

/**
 * This interface describes the methods used to write sequence-related data 
 * into a file.
 * 
 * @param <P> the sequence type (i.e. Gene, Protein, Transcript)
 */
public interface ISeqWriter<P extends ISequence> extends IWriter<P> {
	
	/**
	 * An enumeration describing the file naming strategies that can be used
	 * when create files.
	 */
	public enum NamingStrategy {

		/** Use the symbol for the sequence as the file name. */
		USE_SYMBOL,

		/** Use the accession for the sequence as the file name. */
		USE_ACCESSION
	}

	/**
	 * This method is responsible for writing out each sequence in the
	 * collection into its own file.
	 * 
	 * @param seqs
	 *            the sequences to be written out
	 * @param outputDir
	 *            the directory where the files will appear.
	 * @param autoCreateDir
	 *            a flag indicating whether or not the directory should be
	 *            created if it doesn't exist. If set to false, and the
	 *            directory doesn't exist, an IOException must be thrown.
	 * @param strategy
	 *            a file naming strategy used to determine how the files will be
	 *            named.
	 * @throws IOException
	 *             if there is a problem exporting the files.
	 */
	void writeCollectionSeparately(Collection<P> seqs, Path outputDir,
			boolean autoCreateDir, NamingStrategy strategy) throws IOException;

}
