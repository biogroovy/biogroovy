/**
 * This package contains the UniProtKB search engine.
 */
package org.biogroovy.uniprot;