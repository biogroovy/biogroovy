package org.biogroovy.models

/**
 * This class describes a sponsor of a clinical trial. 
 */
class ClinicalTrialSponsor {
	
	enum SponsorType{
		
		LEAD_SPONSOR,
		COLLABORATOR;
	}
	
	/** The name of the sponsoring agency. */
	String agency;
	
	/** Indicates the class of agency. */
	String agencyClass;
	
	/** Indicates the type of sponsor. */
	SponsorType sponsorType;
	
	@Override
	public String toString() {
		return "ClinicalTrialSponsor [agency=" + agency + ", agencyClass="
				+ agencyClass + "]";
	}

	

}
