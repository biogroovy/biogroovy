package org.biogroovy.models

/**
 * This class describes the type of intervention employed during a clinical trial.
 */
class ClinicalTrialIntervention {

	/** The type of intervention. */
	ClinicalTrialInterventionType type;
	
	/** The name of the intervention. */
	String name;
	
	/** A description of the intervention. */
	String description;
	
	
	@Override
	public String toString() {
		return "ClinicalTrialIntervention [type=" + type + ", name=" + name
				+ ", description=" + description + "]";
	}
	
	

}
