package org.biogroovy.models;

/**
 * This interface describes a simple model with an ID.  
 * 
 *
 */
public interface IModel {
	
	/** Gets the identifier associated with the model. */
	String getModelId();

}
