package org.biogroovy.models

import groovy.transform.ToString
import groovy.transform.TupleConstructor
import thewebsemantic.Namespace
import thewebsemantic.RdfProperty

/**
 * This class represents a single journal article.
 */
@Namespace("http://edamontology.org/")
@ToString(includeNames=true)
class Article implements IModel{


	/**
	 * The title of the article.
	 */
	String title;

	/**
	 * The abstract of the article.
	 */
	@RdfProperty("data_2849")
	String abs;

	/**
	 * The PubMed ID for the article.
	 */
	@RdfProperty("data_1187")
	String pubmedId;
	
	
	@RdfProperty("data_1188")
	String doi;
	
	/**
	 * The PubMedCentral ID. (Optional)
	 */
	String pmcId;

	/**
	 * The date that the article was completed.
	 */
	Date dateCompleted;

	/**
	 * The date that the article was created.
	 */
	Date dateCreated;

	/**
	 * The date that the article was last revised.
	 */
	Date dateRevised;

	/**
	 *  The date that the article was published.
	 */
	Date datePublished;

	/**
	 * The journal in which the article appeared.
	 */
	Journal journal = new Journal();

	/**
	 * The pages in which the article appeared.
	 */
	String pagination;
	
	/** The URL for the article at the journal's web site. */
	String url;

	/**
	 * A list of authors for the article.
	 */
	List<Author> authors = new ArrayList<>();

	/**
	 * The Medical Search Headings used to describe the article.
	 */
	List<MeshHeading> meshHeadings = new ArrayList<>();

	/**
	 * A set of user-definable keywords associated with this article.
	 */
	Set<String> keywords = new HashSet<>();
	
	/**
	 * A list of media associated with the article.  These can be still images
	 * or audio/video files.
	 */
	List<MediaMetadata> mediaList = new ArrayList<>();

    /** A flag that indicates that the article is a review article. */
    boolean isReview = false;

	/**
	 * Concatenates the authors into a single string using a semicolon as a delimiter.
	 * @return a concatenated list of authors
	 */
	public String getAuthorsAsString(){
		StringBuilder sb = new StringBuilder();

		authors.eachWithIndex { author, index ->
			if (index != 0){
				sb.append(";")
			}
			sb.append("${author.lastname}, ${author.firstname}");
		}

		return sb.toString();
	}

	/**
	 * Concatenates the MeSH headings into a single string using a semicolon as a delimiter
	 * @return a string containing the MeSH headings.
	 */
	public String meshHeadingsAsString(){
		StringBuilder sb = new StringBuilder();

		meshHeadings.eachWithIndex { meshHeading, index ->
			if (index != 0){
				sb.append(";");
			}
			sb.append(meshHeading.toString());
		}

		return sb.toString();
	}

	/**
	 * This method creates a semicolon delimited list of keywords.
	 * @return a string containing a semicolon-delimited list of keywords.
	 */
	public String keywordsAsString(){
		StringBuilder sb = new StringBuilder();

		keywords.eachWithIndex { keyword, index ->
			if (index != 0){
				sb.append(";");
			}
			sb.append(keyword);
		}

		return sb.toString();
	}
	
	public void setKeywordsFromString(String keys){
		keywords.addAll(Arrays.asList(keys.split(";")));
	}

	@Override
	public String toString(){
		"${pubmedId}:${title}\n"
	}

	@Override
	public String getModelId() {
		return pubmedId;
	}
}

