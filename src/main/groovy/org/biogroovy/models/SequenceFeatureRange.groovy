package org.biogroovy.models;

/**
 * This class represents a sequence feature range, where the feature has a distinct start 
 * and stop point within the sequence.
 */
class SequenceFeatureRange extends AbsSequenceFeature{

    /** The starting point of the feature within the sequence. */
    int start;
    
    /** The ending point of the feature within the sequence. */
    int end;
}
