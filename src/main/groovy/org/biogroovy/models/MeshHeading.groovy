package org.biogroovy.models;

import thewebsemantic.Id
import thewebsemantic.Namespace
import thewebsemantic.RdfProperty

/**
 * This class represents a Medical Search Heading (MeSH) term from a PubMed reference.
 *
 */
@Namespace("http://edamontology.org")
class MeshHeading {

	/**
	 * The mesh descriptor name
	 */
	@RdfProperty("data_1177")
	@Id String descriptorName;

	/**
	 * A list of qualifier names for the mesh descriptor.
	 */
	List<String> qualifierNames = new ArrayList<>();


	/**
	 * Constructor.
	 * @param descriptorName the descriptor name for the MeSH heading.
	 * @param qualifierNames an optional list of qualifier names.
	 */
	public MeshHeading(String descriptorName, String... qualifierNames){
		this.descriptorName = descriptorName;

		for(String name : qualifierNames){
			this.qualifierNames.add(name);
		}
	}

	/**
	 * Constructor.
	 */
	public MeshHeading(){
	}



	@Override
	public String toString(){
		String str = "";
		if (this.descriptorName != null){
			str =  this.descriptorName + " ";
			Iterator<String> qualIt = qualifierNames.iterator();
			if (qualIt.hasNext()){
				str += "[ "
				while(qualIt.hasNext()){
					String nextItem = qualIt.next();
					if (nextItem != null){
						str += nextItem + " ";
					}
				}
				str += "]"
			}
			str +=";"
		}
	}
}
