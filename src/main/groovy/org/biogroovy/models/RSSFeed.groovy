package org.biogroovy.models

import java.text.SimpleDateFormat

/**
 * This model represents a bookmark for an RSS feed.  
 *
 */
class RSSFeed implements IModel{
	
	/** The name of the RSS feed. */
	String name;
	
	/** The URL of the RSS feed. */
	String url;
	
	/** The description of the RSS feed. */
	String description;
	
	/** The icon url of the RSS feed. */
	String iconUrl;
	
	
	Set<String> tagSet = new HashSet<>();
	Date lastRead = new Date();
	SimpleDateFormat format = new SimpleDateFormat("YYYY-MM-dd");
	
	List<Article> articleList = new ArrayList<>();
	

	/**
	 * Constructor.
	 */
	public RSSFeed() {
		
	}
	
	/**
	 * Sets the date using a properly formatted date string
	 * @param date a date in the format "YYYY-MM-dd"
	 */
	public void setDateAsString(String date){
		lastRead = format.parse(date);
	}
	
	/**
	 * Gets the date as a string.
	 * @return a date in the format "YYYY-MM-dd"
	 */
	public String getDateAsString(){
		return format.format(lastRead)
	}


	@Override
	public String getModelId() {
		return url;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((format == null) ? 0 : format.hashCode());
		result = prime * result + ((iconUrl == null) ? 0 : iconUrl.hashCode());
		result = prime * result
				+ ((lastRead == null) ? 0 : lastRead.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((tagSet == null) ? 0 : tagSet.hashCode());
		result = prime * result + ((url == null) ? 0 : url.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {

		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RSSFeed other = (RSSFeed) obj;
		if (format == null) {
			if (other.format != null)
				return false;
		} else if (!format.equals(other.format))
			return false;
		if (iconUrl == null) {
			if (other.iconUrl != null)
				return false;
		} else if (!iconUrl.equals(other.iconUrl))
			return false;
		if (lastRead == null) {
			if (other.lastRead != null)
				return false;
		} else if (!lastRead.equals(other.lastRead))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (tagSet == null) {
			if (other.tagSet != null)
				return false;
		} else if (!tagSet.equals(other.tagSet))
			return false;
		if (url == null) {
			if (other.url != null)
				return false;
		} else if (!url.equals(other.url))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "RSSFeed [name=" + name + ", url=" + url + ", iconUrl="
				+ iconUrl + ", tagSet=" + tagSet + ", lastRead=" + lastRead
				+ ", format=" + format + "]";
	}
	
	

}
