package org.biogroovy.models

import groovy.transform.ToString

@ToString(includeNames=true)
class MediaMetadata {

	String url;
	String mediaType;
	Integer height;
	Integer width;
	String title;
	String description;
}
