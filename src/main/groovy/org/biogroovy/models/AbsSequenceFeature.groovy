package org.biogroovy.models;

/**
 * This is the base class for Sequence Features.
 */
class AbsSequenceFeature{
	
    /** The accession of the feature. */
	String accession;
    
    /** The type of sequence feature. */
	FeatureType type;
    
    /** The description of the feature. */
	String description;
    
    /** The evidence (articles) associated with the feature.  */
	Set<Article> evidence = new HashSet<Article>();
}