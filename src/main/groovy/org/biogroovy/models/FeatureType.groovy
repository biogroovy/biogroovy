package org.biogroovy.models;

/**
 * This enumeration contains the {@link Protein} feature types.
 * 
 *
 */
public enum FeatureType{
    ACTIVE_SITE("active site", "POINT"),
    BINDING_SITE("binding site", "POINT"),
    CALCIUM_BINDING_REGION("calcium-binding region","REGION"),
    CHAIN("chain","REGION"),
    COILED_COIL_REGION("coiled-coil region","REGION"),
    COMPOSITIONALLY_BIASED_REGION("compositionally biased region","REGION"),
    CROSS_LINK("cross-link","POINT"),
    DISULFIDE_BOND("disulfide bond","POINT"),
    DNA_BINDING_REGION("DNA-binding region",'REGION'),
    DOMAIN("domain","REGION"),
    GLYCOSYLATION_SITE("glycosylation site","POINT"),
    HELIX("helix","REGION"),
    INITIATOR_METHIONINE("initiator methionine","POINT"),
    LIPID_MOIETY_BINDING_REGION("lipid moiety-binding region","REGION"),
    METAL_ION_BINDING_SITE("metal ion-binding site","POINT"),
    MODIFIED_RESIDUE("modified residue", "POINT"),
    MUTAGENESIS_SITE("mutagenesis site","POINT"),
    NON_CONSECUTIVE_RESIDUES("non-consecutive residues","REGIOn"),
    NON_TERMINAL_RESIDUE("non-terminal residue","POINT"),
    NUCLEOTIDE_PHOSPHATE_BINDING_REGION("nucleotide phosphate-binding region","REGION"),
    PEPTIDE("peptide","POINT"),
    PROPEPTIDE("propeptide","POINT"),
    REGION_OF_INTEREST("region of interest","REGION"),
    REPEAT("repeat","REGION"),
    NON_STANDARD_AMINO_ACID("non-standard amino acid","POINT"),
    SEQUENCE_CONFLICT("sequence conflict","REGION"),
    SEQUENCE_VARIANT("sequence variant","POINT"),
    SHORT_SEQUENCE_MOTIF("short sequence motif","REGION"),
    SIGNAL_PEPTIDE("signal peptide","POINT"),
    SITE("site","POINT"),
    SPLICE_VARIANT("splice variant","POINT"),
    STRAND("strand","REGION"),
    TOPOLOGICAL_DOMAIN("topological domain","REGION"),
    TRANSIT_PEPTIDE("transit peptide","POINT"),
    TRANSMEMBRANE_REGION("transmembrane region","REGION"),
    TURN("turn","REGION"),
    UNSURE_RESIDUE("unsure residue","POINT"),
    ZINC_FINGER_REGION("zinc finger region","REGION"),
    INTRAMEMBRANE_REGION('intramembrane region',"REGION");

	/** The name of the feature. */
    String name;
	
	/** The type of feature. */
    String type;


    /**
     * Constructor.
     * @param name the name of the feature type
     */
    FeatureType(String name, String type){
        this.name = name;
        this.type = type;
    }

	/**
	 * This method gets a FeatureType from a string-value.
	 * @param name the name of the feature.
	 * @return the appropriate feature type
	 */
    public static FeatureType getTypeFromString(String name){
        FeatureType type = null;
        for(FeatureType currType: FeatureType.values()){
            if (currType.name.equals(name)){
                type = currType;
                break;
            }
        }
        return type;
    }
}