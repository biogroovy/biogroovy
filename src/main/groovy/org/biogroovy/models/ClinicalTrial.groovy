package org.biogroovy.models

/**
 * This class contains clinical trial metadata.
 */
class ClinicalTrial {
	
	/** The short title of the trial. */
	String briefTitle;
	
	/** The official title of the trial. */
	String officialTitle;
	
	/** The clinical trial id from the clinicaltrials.gov site. */
	String clinicalTrialId;
	
	/** The URI of the clinical trial. */
	String clinicalTrialUri;
	
	/** A brief summary of the trial. */
	String briefSummary;
	
	/** A detailed description of the trial. */
	String detailedDescription;
	
	/** The phase of the trial. */
	String phase;
	
	/** The start date of the trial. */
	String startDate;
	
	/** The primary completion date of the trial. */
	String primaryCompletionDate;
	
	/** The overall status of the trial. */
	String overallStatus;
	
	/** The <a href="https://clinicaltrials.gov/ct2/help/how-find/advanced/field-defs#StudyType">type</a> of study. */
	String studyType;
	
	/** The design of the study. */
	String studyDesign;
	
	/** A list of MeSH terms associated with the trial. */
	List<MeshHeading> meshTerms = new ArrayList<>();
	
	/** A list of tags associated with the trial. */
	List<String> tags = new ArrayList<>();
	
	/** The sponsors of the trial. */
	List<ClinicalTrialSponsor> sponsors = new ArrayList<>();
	
	/** The collaborators on the trial. */
	List<ClinicalTrialSponsor> collaborators = new ArrayList<>();
	
	/** The primary outcomes of the trial. */
	List<ClinicalTrialOutcome> primaryOutcomes = new ArrayList<>();
	
	/** Interventions used in the trial. */
	List<ClinicalTrialIntervention> interventions = new ArrayList<>();
	
	/** Patient eligibility criteria. */
	List<ClinicalTrialEligibility> eligibility = new ArrayList<>();

	@Override
	public String toString() {
		return "ClinicalTrial [clinicalTrialId=" + clinicalTrialId
				+ ", clinicalTrialsUri=" + clinicalTrialUri + "]";
	}


}
