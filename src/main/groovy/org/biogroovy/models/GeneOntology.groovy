package org.biogroovy.models;

import thewebsemantic.Id
import thewebsemantic.RdfProperty

/**
 * This class represents a Gene Ontology record.
 */
public class GeneOntology {
    


    /**
     * The type of gene ontology annotation type: Cellular Process
     */
	GeneOntologyType type;

    /**
     * The gene ontology name.
     */
    @RdfProperty("data_0966")
	String name;

    /**
     * The Gene Ontology ID
     */
	@Id String goId;

    /** The evidence code for this Gene Ontology reference. */
	GeneOntologyEvidence evidence;
	
	GeneOntologyQualifier qualifier;

    /** A list of PubMed IDs that act as supporting evidence for this gene ontology reference. */
    List<String> pmidList = new ArrayList<>();

	public String toString(){
		return "GeneOntology[id:${goId}, type:${type}, name:${name}, evidenceCode:${evidence}, evidence:${pmidList}]"
	}

}