package org.biogroovy.models

import thewebsemantic.Id
import thewebsemantic.Namespace
import thewebsemantic.RdfProperty

/**
 * This class contains reference information for the protein structure.
 */
@Namespace("http://edamontology.org/")
class ProteinStructureEntry {
    
    /** The type of protein structure record (either a PDB Summary or a PDB record). */
    enum StructureType{
        PDBSum,
        PDB
    }
    
    /** The experimental method used to determine the structure. */
    enum ExperimentalMethod{
        X_RAY,
        NMR
    }
    
    /** The type of protein structure */
    StructureType structureType;
    
    /** The PDB ID. */
    @RdfProperty("data_1127")
    @Id
    String accession;
    
    /** The method used to determine the structure. */
    ExperimentalMethod expMethod;
    
    /** The chains. */
    @RdfProperty("data_1467")
    String chains;
    
    /** The resolution of the structure image. */
    Float resolution;
    
    /**
     * This method sets the experimental method used to determine the structure.
     * @param method the experimental method used to determine the structure
     */
    public void setExpMethod(String method){
        if(method.equals("X-ray")){
            expMethod = ExperimentalMethod.X_RAY;
        }else{
            expMethod = ExperimentalMethod.NMR;
        }
    }
    

}
