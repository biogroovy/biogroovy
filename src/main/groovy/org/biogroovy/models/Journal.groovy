package org.biogroovy.models;

/**
 * This class represents a Journal object.
 */
class Journal implements IModel{

	/**
	 * The title of the journal.
	 */
	String title;

	/**
	 * The volume of the journal.
	 */
	String volume

	/**
	 * The issue of the journal.
	 */
	String issue;

	/**
	 * The publication date for the journal.
	 */
	Date publicationDate;

	/**
	 * The ISSN identifier for the journal
	 */
	String issn;

	/** The starting page of the reference. */
	int startPage;

	/** The ending page of the reference. */
	int endPage;
	
	/** The URL for the RSS feed for the journal. */
	String rssUrl;
	
	/** The URL for the thumbnail image for the journal. */
	String imageUrl;
	
	/** The URL for the journal's web site. */
	String siteUrl;
	
	/** The journal rights. */
	JournalRights rights;

	public String toIdString(){
		return "${title}:${volume}:${issue}:${issn}";
	}


	public String toString(){
		return "Journal:${this.properties.toMapString()}";
	}

	/**
	 * Gets the URL for the journal.
	 * @param userId requires the user's journal tocs userId.
	 * @return the url for the journal.
	 */
	public String getURL(String userId){
		String url = "http://www.journaltocs.ac.uk/api/journals/${issn}?user=${userId}";
		def root = new XmlParser().parse(url);
		return root.item.link[0].text()
	}
	
	public void updateRights(String rightsStr) {
		rights = JournalRights.lookup(rightsStr);
	}


	@Override
	public String getModelId() {
		return issn;
	}
}
