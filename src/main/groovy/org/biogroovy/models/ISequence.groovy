package org.biogroovy.models

/**
 * This interface defines the methods required for Sequence-based classes.
 */
public interface ISequence extends IModel{

    /**
     * This method gets the sequence.
     * @return the sequence
     */
    String getSequence();


    /**
     * This method gets the accession for the sequence.
     * @return the accession for the sequence.
     */
    String getAccession();

    /**
     * Gets the symbol for the gene|transcript|protein.
     * @return the symbol for the gene|transcript|protein.
     */
    String getSymbol();

    /**
     * Gets the name for the gene|transcript|protein.
     * @return the name for the gene|transcript|protein.
     */
    String getName();

    /**
     * Gets the description for the gene|transcript|protein.
     * @return the description for the gene|transcript|protein.
     */
    String getDescription();
    
    /**
     * Gets the species for the gene|transcript|protein.
     * @return the species for the gene|transcript|protein.
     */
    String getSpecies();
    
}

