package org.biogroovy.models;

import thewebsemantic.Id
import thewebsemantic.Namespace
import thewebsemantic.RdfProperty

import com.google.common.collect.ArrayListMultimap
import com.google.common.collect.Multimap

/**
 * This class represents an Online Mendelian Inheritance In Man database record.
 */
@Namespace("http://edamontology.org/")
class Omim {
	

    /**
     * The record ID.
     */
	@RdfProperty("data_1153")
	@Id
	String mimId;

    /**
     * The title of the OMIM record.
     */
	String title;

    /**
     * The gene symbol
     */
	String symbol

    /**
     * The gene locus
     */
	String locus

    /**
     * A list of aliases for the disease that the OMIM record describes.
     */
	List<String> aliases = new ArrayList<>();

    /**
     * The text elements that provide a general description of the disease.
     */
	List<OmimText> textElements = new ArrayList<>();

    /**
     * The text elements that the describe the cytogenetic mapping of the disease.
     */
	List<OmimText> cytoMapping = new ArrayList<>();

    /**
     * The text elements that describe the pathogenesis of the disease.
     */
	List<OmimText> pathogenesis = new ArrayList<>();

    /**
     * The text elements that describe the molecular genetics of the disease.
     */
	List<OmimText> molecularGenetics = new ArrayList<>();

    /**
     * The text elements that describe the animal model of the disease.
     */
	List<OmimText> animalModel = new ArrayList<>();

    /**
     * The text elements that describe the clinical features of the disease.
     */
	List<OmimText> clinicalFeatures = new ArrayList<>();

    /**
     * The PubMed references used in the OMIM record.
     */
	List<Article> references = new ArrayList<>();

    /**
     * The Medline record IDs referred to in the record.
     */
	List<String> medlineLinks = new ArrayList<>();

    /**
     * The Protein record IDs referred to in the record.
     */
	List<String> proteinLinks = new ArrayList<>();

    /**
     * The Nucleotide record IDs referred to in the record.
     */
	List<String> nucleotideLinks = new ArrayList<>();

	/**
     * A multimap of the clinical synopses.
     */
	Multimap<String, String> clinicalSynopsis = ArrayListMultimap.create();
	
	/**
     * This method adds an OMIM text object to the appropriate collection.
     * @param text  The OMIM text object.
     */
	public void addOmimText(OmimText text){
		if (text.type == "MAPPING"){
			this.cytoMapping.add(text);
		}else if (text.type == "PATHOGENESIS"){
			this.pathogenesis.add(text)
		}else if (text.type == 'TEXT'){
			this.textElements.add(text)
		}else if (text.type == 'MOLECULAR GENETICS'){
			this.molecularGenetics.add(text)
		}else if (text.type == 'ANIMAL MODEL'){
			this.animalModel.add(text)
		}else if (text.type == 'CLINICAL FEATURES'){
			this.clinicalFeatures.add(text)
		}else {
			throw new RuntimeException("Unsupported text type: ${text.type}")
		}
	}

    /**
     * This method adds a list of terms to the clinical synopsis map.
     * @param key  The key for the clinical synopsis terms.
     * @param terms  A list of terms.
     */
	public void addClinicalSynopsisTerms(String key, List<String> terms){
		this.clinicalSynopsis.putAll (key, terms)
	}

}
