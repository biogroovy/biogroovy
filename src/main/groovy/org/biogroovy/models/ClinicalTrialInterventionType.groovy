package org.biogroovy.models;

/**
 * This enumeration describes the types of clinical trial interventions. 
 */
public enum ClinicalTrialInterventionType {

	OTHER, PROCEDURE;

	/**
	 * This method lookups the intervention type.
	 * 
	 * @param value
	 *            A string containing the type name
	 * @return an intervention type, or null if not found.
	 */
	public static ClinicalTrialInterventionType lookup(String value) {

		ClinicalTrialInterventionType type = null;
		for (ClinicalTrialInterventionType types : values()) {
			if (types.name().equalsIgnoreCase(value)) {
				type = types;
				break;
			}
		}
		return type;
	}

}
