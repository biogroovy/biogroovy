package org.biogroovy.models;

/**
 * This enumeration describes a qualifier term for a gene ontology term.
 * 
 *
 */
public enum GeneOntologyQualifier {

	NOT,
	colocalizes_with,
	contributes_to
}
