/**
 * This package contains classes used to decorate genes, transcripts and proteins
 * with data from multiple databases.
 */
package org.biogroovy.models.decorators;
