package org.biogroovy.models.decorators

/**
 * This exception is thrown whenever the decorator has a problem getting the appropriate data
 * for a ISequence object.
 *
 */
class DecorationException extends Exception {
}
