package org.biogroovy.models.decorators

import java.util.concurrent.Callable

import org.biogroovy.io.eutils.EntrezGeneReader;
import org.biogroovy.models.AbsSequence
import org.biogroovy.models.Gene

/**
 * 
 *
 */
class EntrezGeneDecorator extends AbsSeqDecorator<Gene> implements ISequenceDecorator<Gene> {

    @Override
    void decorate(Gene... sequences) throws DecorationException {
        for(Gene gene : sequences){
            exec.submit(new DecoratorTask(gene));
        }
    }
    
    /**
     * This task is responsible for decorating the protein object
     * with additional data.
     */
    class DecoratorTask implements Callable<Void>{
        
        private Gene gene;

        public DecoratorTask(final Gene gene){
            this.gene = gene;
        }

        @Override
        public Void call() throws Exception {
            
           EntrezGeneReader reader = new EntrezGeneReader();
           reader.read(gene); 
           return null;
        }
        
    }



}
