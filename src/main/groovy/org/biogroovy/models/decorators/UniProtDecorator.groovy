package org.biogroovy.models.decorators;

import java.util.concurrent.Callable
import java.util.concurrent.ThreadPoolExecutor

import org.biogroovy.models.AbsSequence
import org.biogroovy.models.Protein

/**
 * 
 *
 */
class UniProtDecorator extends AbsSeqDecorator<Protein> {
	

	@Override
	void decorate(Protein... sequences) throws DecorationException{
		
		for(Protein protein : sequences){
			if (protein.swissprotAccession != null){
				exec.submit(new DecoratorTask(protein));
			}
		}
		
	}
	

	
	/**
	 * This task is responsible for decorating the protein object
	 * with additional data.
	 */
	class DecoratorTask implements Callable<Void>{
		
		private Protein protein;

		public DecoratorTask(final Protein protein){
			this.protein = protein;
		}

		@Override
		public Void call() throws Exception {
			
			String uniprotRec = "http://www.uniprot.org/${protein.swissprotAccession}".toURL().getText()
			def root = new XmlSlurper().parseText(uniprotRec);
			def symbol = root.gene.name.find{it.@type='primary'};
			protein.sequence = root.sequence;
			
			
			return null;
		}
		
	}

	

}
