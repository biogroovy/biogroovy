package org.biogroovy.models.decorators

import org.biogroovy.models.AbsSequence

/**
 * Implementations of this interface decorate/annotate sequences with additional data gathered from other sources.
 * For example, if you have a Gene and you want to add data to it from another source, like the bio2rdf site
 * the sequence decorator will fetch that data, and decorate the Gene record with it.
 */
interface ISequenceDecorator<S extends AbsSequence> {

	/**
	 * This method adds data from another operation to the input sequences.
	 * @param sequences the sequences to be annotated.
	 */
	void decorate(S... sequences ) throws DecorationException;
}
