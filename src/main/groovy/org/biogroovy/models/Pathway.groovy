
package org.biogroovy.models

import thewebsemantic.Namespace
import thewebsemantic.RdfProperty

/**
 * This class represents a pathway.
 */
@Namespace("http://edamontology.org/")
class Pathway {

    /**
     * The name of the pathway.
     */
    @RdfProperty("data_2342")
    String name;
	
	/** The description of the pathway */
	String description;
	
	/** A list of genes in the pathway */
	List<Gene> geneList = new ArrayList<>();
	
	/** The database containing the pathway. */
	String datasource;
	
	/** The pathway identifier used within the context of the datasource. */
    @RdfProperty("data_2365")
	String accession;
    
    @Override
    public String toString() {
        return "Pathway[name: ${name}, datasource: ${datasource}, accession: ${accession}]";
    }
	
}

