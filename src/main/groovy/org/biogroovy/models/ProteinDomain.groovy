package org.biogroovy.models

/**
 * This class defines a protein domain.
 */
class ProteinDomain {

	/** The name of the protein domain database. */
	String database;
	
	/** The id of the protein domain in the database. */
	String domainId;
	
	/** A short description of the domain. */
	String shortDesc;
	
	/** The long description of the domain. */
	String desc;
	
	
	@Override
	public String toString() {
		return "ProteinDomain [database=" + database + ", id=" + domainId
				+ ", shortDesc=" + shortDesc + ", desc=" + desc + "]";
	}

	
	
}
