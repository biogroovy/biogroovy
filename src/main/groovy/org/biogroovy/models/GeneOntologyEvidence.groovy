package org.biogroovy.models;

/**
 * This enumeration contains the GO evidence codes and their descriptions.
 */
public enum GeneOntologyEvidence {
	
	// experimental evidence codes
	
	EXP("Inferred from Experiment"),
	IDA("Inferred from Direct Assay"),
	IPI("Inferred from Physical Interaction"),
	IMP("Inferred from Mutant Phenotype"),
	IGI("Inferred from Genetic Interaction"),
	IEP("Inferred from Expression Pattern"),
	
	
	// computational evidence codes
	
	ISS("Inferred from Sequence or structural Similarity"),
	ISO("Inferred from Sequence Orthology"),
	ISA("Inferred from Sequence Alignment"),
	ISM("Inferred from Sequence Model"),
	IGC("Inferred from Genomic Context"),
	IBA("Inferred from Biological aspect of Ancestor"),
	IBD("Inferred from Biological aspect of Descendant"),
	IKR("Inferred from Key Residues"),
	IRD("Inferred from Rapid Divergence"),
	RCA("Inferred from Reviewed Computational Analysis"),
	
	
	// author evidence codes
	TAS("Traceable Author Statement"),
	NAS("Non-traceable Author Statement"),
	
	// curatorial evidence codes
	
	IC("Inferred by Curator"),
	ND("No biological Data available"),
	
	// automatically assigned evidence codes
	IEA("Inferred from Electronic Annotation"),
	
	// obsolete evidence codes
	NR("Not Recorded")
	
	
	
	
	
	/** A description of the evidence code. */
	String desc;

	
	/**
	 * Constructor	
	 * @param desc description of the evidence code
	 */
	public GeneOntologyEvidence(String desc){
		this.desc = desc;
	}
	
	

}
