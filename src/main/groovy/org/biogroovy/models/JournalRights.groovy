package org.biogroovy.models;

/**
 * This enumeration indicates the type of distribution rights that journals have.
 */
public enum JournalRights {
	
	/** Indicates that the journal is Open Access. */
	OPEN_ACCESS, 
	
	/** Indicates that the journal is only available via subscription. */
	SUBSCRIPTION, 
	
	/** Indicates that some articles are freely available. */
	PARTIALLY_FREE, 
	
	/** Indicates that the articles are freely available. */
	FREE, 
	
	/** Indicates that the journal contains both freely available and subscription articles. */
	HYBRID,
	
	
	/** Indicates that the Journal Rights are unknown. */
	UNKNOWN;
	
	/**
	 * This method is used to convert plain-text references into JournalRights
	 * references.
	 * @param rights The string version of a JournalRights value.
	 * @return a JournalRights value.
	 */
	public static JournalRights lookup(String rights) {
		if(rights == '' || rights == null){
			return JournalRights.UNKNOWN;
		}
		
		String newRights = rights.replace(" ", "_").toUpperCase();
		JournalRights rights2 = null;
		try {
			rights2 = JournalRights.valueOf(newRights);
		}catch (IllegalArgumentException ex) {
			println "No JournalRights value for: '${rights}'"
			rights2 = JournalRights.UNKNOWN;
		}
		return rights2;
	}

}
