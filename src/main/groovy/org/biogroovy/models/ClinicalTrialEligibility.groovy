package org.biogroovy.models;

/**
 * This class contains clinical trial eligibility criteria.
 */
public class ClinicalTrialEligibility {
	
	enum Gender{
		MALE,
		FEMALE,
		BOTH
	}

	/** The study population. */
	String studyPop;
	
	/** The sampling method. */
	String samplingMethod;
	
	/** The criteria. */
	String criteria;
	
	/** The gender. */
	Gender gender;
	
	/** The minimum qualifying age of the patient. */
	String minAge;
	
	/** The maximum qualifying age of the patient. */
	String maxAge;
	
	
	String healthyVolunteers;
	
	/**
	 * This method looks up the gender;
	 * @param genderStr
	 * @return
	 */
	static Gender lookupGender(String genderStr){
		
		Gender gender = null;
		for(Gender curr : Gender.values()){
			if (curr.name().equalsIgnoreCase(genderStr)){
				gender = curr;
				break;
			}
		}
			
		return gender;
	}

	@Override
	public String toString() {
		return "ClinicalTrialEligibility [studyPop=" + studyPop
				+ ", samplingMethod=" + samplingMethod + ", gender=" + gender
				+ ", minAge=" + minAge + ", maxAge=" + maxAge
				+ ", healthyVolunteers=" + healthyVolunteers + "]";
	}
	
	
	
}
