package org.biogroovy.models

/**
 * This class describes the outcome of the clinical trial.
 */
class ClinicalTrialOutcome {
	
	/** The outcome measurement. */
	String measure;
	
	/** The time frame of the outcome. */
	String timeFrame;
	
	/** Indicates if there was a safety issue. */
	String safetyIssue;
	
	/** A description of the outcome. */
	String description;
	
	
	@Override
	public String toString() {
		return "ClinicalTrialOutcome [measure=" + measure + ", timeFrame="
				+ timeFrame + ", safetyIssue=" + safetyIssue + "]";
	}

	

}
