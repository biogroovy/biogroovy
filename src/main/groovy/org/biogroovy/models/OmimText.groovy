package org.biogroovy.models;

/**
 * This class represents the text blocks that appear in an OMIM record.
 *
 */
class OmimText {

	/**
	 * Indicates the type of text block (i.e. 'Text', 'Molecular Genetics', 
	 * 'Pathogenesis', etc)
	 */
	String type;

	/**
	 * The text of the text block.
	 */
	String text;

	/**
	 * The PubMed UIDs of the articles that support this text block.
	 */
	String uids
}
