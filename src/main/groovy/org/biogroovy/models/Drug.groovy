package org.biogroovy.models

/**
 * This class represents either a Drug or a Compound.  
 */
class Drug {
	
	/** The name of the drug. */
	String name
	
	/** A comma-delimited list of aliases for the drug. */
	String synonyms
	
	/** The drugbank ID for the drug.  (May be absent if it is a compound).*/
	String drugbankId
	
	/** The PubChem ID for the compound. */
	String pubchemId
	
	/** The Chembl ID for the compound. */
	String chemblId
	
	/** The molecular weight for the compound. */
	Float molWeight = 0.0f;
	
	/** The molecular formula for the compound. */
	String molFormula
	
	/** The InChi key for the compound. */
	String inChiKey
	
	/** The smiles string for the compound. */
	String smiles
	
	String sourceUri
	
	boolean passesRuleOfThree = false;
	
	/** A flag that indicates if the compound is a known drug. */
	boolean isKnownDrug = false;
	
	/** A flag that indicates if the compound is med chem friendly. */
	boolean isMedChemFriendly = false;
	
	Float acdLogp = 0.0f;
	Float acdLogd = 0.0f;
	
	/** The XLogP value of the compound. */
	Float xLogp = 0.0f;

	/** The number of rotatable bonds. */
	Integer rotatableBonds = 0;
	
	/** The ALogP value of the compound. */
	Float alogp = 0.0F;
	Integer numRo5Violations = 0;
	
	
	@Override
	public String toString() {
		return "Drug [name=" + name 
				+ ", synonyms=" + synonyms 
				+ ", drugbankId="+ drugbankId 
				+ ", pubchemId=" + pubchemId 
				+ ", chemblId="+ chemblId 
				+ ", molWeight=" + molWeight 
				+ ", molFormula="+ molFormula 
				+ ", inChiKey=" + inChiKey 
				+ ", smiles=" + smiles
				+ ", sourceUri=" + sourceUri 
				+ ", passesRuleOfThree="+ passesRuleOfThree 
				+ ", isKnownDrug=" + isKnownDrug
				+ ", isMedChemFriendly=" + isMedChemFriendly 
				+ ", acdLogp="+ acdLogp 
				+ ", acdLogd=" + acdLogd 
				+ ", rotatableBonds="+ rotatableBonds 
				+ ", alogp=" + alogp 
				+ ", numRo5Violations="+ numRo5Violations 
				+ "]";
	}


	
}
