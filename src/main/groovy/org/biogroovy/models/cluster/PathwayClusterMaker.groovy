package org.biogroovy.models.cluster;

import java.util.Collection;

import org.biogroovy.models.Gene;
import org.biogroovy.models.Pathway;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;

/**
 * This cluster maker clusters genes according to the pathways to which they belong.
 *
 */
public class PathwayClusterMaker implements IClusterMaker<Pathway, Gene> {

	@Override
	public Multimap<Pathway, Gene> createCluster(Collection<Gene> seqCollection) {
		Multimap<Pathway, Gene> map = ArrayListMultimap.create();
		
		for(Gene gene : seqCollection){
			for(Pathway pathway : gene.getPathways()){
				map.put(pathway, gene);
			}
		}
		
		return map;
	}

}
