/**
 * This package contains classes used to create clusters based on gene attributes, 
 * and journal attributes.
 */
package org.biogroovy.models.cluster;
