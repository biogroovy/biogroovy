package org.biogroovy.models.cluster

import org.biogroovy.models.ISequence

import com.google.common.collect.Multimap

/**
 * Implementations of this interface create a cluster using the objects in the collection
 * and return a map whose keys are the centroids, and whose values are the objects that cluster to that centroid.
 *
 * @param <C> The centroid type
 * @param <V> The value type that belong to the centroid.
 */
interface IClusterMaker<C, V> {

	/**
	 * This method creates a cluster and stores the result in a Multimap. 
	 * 
	 * @param seqCollection the collection model objects to be clustered
	 * @return a multimap whose keys are the centroids (which may be Strings, IDs, accessions or classes)
	 * and the values are the model objects which belong to that centroid.
	 */
	Multimap<C,V> createCluster(Collection<V> seqCollection);
}
