package org.biogroovy.models.cluster;

import java.util.Collection;

import org.biogroovy.models.Article;
import org.biogroovy.models.MeshHeading;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;

/**
 *
 */
public class MeshClusterMaker implements IClusterMaker<MeshHeading, Article> {

	@Override
	public Multimap<MeshHeading, Article> createCluster(
			Collection<Article> seqCollection) {
		Multimap<MeshHeading, Article> map = ArrayListMultimap.create();
		
		for (Article article : seqCollection){
			for (MeshHeading heading : article.getMeshHeadings()){
				map.put(heading, article);
			}
		}
		
		return map;
	}

}
