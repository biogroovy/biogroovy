package org.biogroovy.models.cluster;

import java.util.Collection;

import org.biogroovy.models.Gene;
import org.biogroovy.models.GeneOntology;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;

/**
 * This cluster maker clusters genes by the Gene Ontology function to which they belong.
 */
public class GoFunctionClusterMaker implements IClusterMaker<GeneOntology, Gene> {

	@Override
	public Multimap<GeneOntology, Gene> createCluster(
			Collection<Gene> seqCollection) {
		
		Multimap<GeneOntology, Gene> map = ArrayListMultimap.create();
		
		for(Gene gene : seqCollection){
			for(GeneOntology go : gene.getGoFunctionList()){
				map.put(go, gene);
			}
		}
		
		return map;
	}
	
	

}
