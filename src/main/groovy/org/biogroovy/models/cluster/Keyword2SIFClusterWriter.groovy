/**
 * 
 */
package org.biogroovy.models.cluster;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import org.biogroovy.models.Article;

import com.google.common.collect.Multimap;

/**
 * This cluster writer outputs a cluster of articles whose centroids are
 * keywords associated with those articles.
 */
public class Keyword2SIFClusterWriter implements
		IClusterWriter<String, Article> {

	@Override
	public void write(Multimap<String, Article> cluster, OutputStream out)
			throws IOException {
		

		for (String keyword : cluster.keySet()) {
			for (Article article : cluster.get(keyword)) {
				out << keyword + "\tdescribes\t" + article.getTitle();
			}
		}

		

	}

}
