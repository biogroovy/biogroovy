package org.biogroovy.models.cluster;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;

import org.biogroovy.models.Gene;
import org.biogroovy.models.GeneOntology;

import com.google.common.collect.Multimap;

/**
 *  This cluster writer, outputs a cluster containing genes clustered by gene ontology entries.
 */
public class Go2SIFClusterWriter implements IClusterWriter<GeneOntology, Gene>{

	@Override
	public void write(Multimap<GeneOntology, Gene> cluster, OutputStream out) throws IOException {
		
		
		
		for(GeneOntology go : cluster.keySet()){
			for(Gene gene : cluster.get(go)){	    
				out << go.getName() + "\tisDescribedBy\t" + gene.getGeneSymbol();
			}
		}
		
		
		
		
	}

}
