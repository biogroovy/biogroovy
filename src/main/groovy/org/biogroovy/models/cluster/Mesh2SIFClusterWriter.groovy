package org.biogroovy.models.cluster;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import org.biogroovy.models.Article;
import org.biogroovy.models.MeshHeading;

import com.google.common.collect.Multimap;

/**
 * This cluster writer outputs clusters to SIF files suitable for rendering in Cytoscape.
 * 
 */
public class Mesh2SIFClusterWriter implements IClusterWriter<MeshHeading, Article> {

	@Override
	public void write(Multimap<MeshHeading, Article> cluster, OutputStream out)
			throws IOException {
		
		for(MeshHeading mesh : cluster.keySet()){
			for(Article article : cluster.get(mesh)){
				out << mesh.toString() + "< \tdescribes\t" + article.getTitle();
			}
		}
		
		
	}

}
