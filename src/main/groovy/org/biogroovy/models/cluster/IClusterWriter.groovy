package org.biogroovy.models.cluster;

import java.io.IOException;
import java.io.OutputStream;

import com.google.common.collect.Multimap;

/**
 * Implementations of this interface are designed to output clusters in
 * different file formats.
 */
public interface IClusterWriter<C, V> {

	/**
	 * This method writes the contents of a cluster out to a file.
	 * 
	 * @param cluster
	 *            the cluster to be written.
	 * @param filepath
	 *            the path to the output file.
	 * @throws IOException
	 *             if there is a problem writing the file out.
	 */
	void write(Multimap<C, V> cluster, OutputStream output) throws IOException;

}
