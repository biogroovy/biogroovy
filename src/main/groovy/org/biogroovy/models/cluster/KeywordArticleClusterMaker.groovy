package org.biogroovy.models.cluster;

import java.util.Collection;

import org.biogroovy.models.Article;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;

/**
 * This class uses keywords associated with articles to cluster the articles.
 */
public class KeywordArticleClusterMaker implements
		IClusterMaker<String, Article> {

	@Override
	public Multimap<String, Article> createCluster(Collection<Article> articles) {

		Multimap<String, Article> map = ArrayListMultimap.create();

		for (Article article : articles) {
			for (String keyword : article.getKeywords()) {
				map.put(keyword, article);
			}
		}

		return map;

	}

}
