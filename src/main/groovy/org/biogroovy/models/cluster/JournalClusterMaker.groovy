package org.biogroovy.models.cluster;

import java.util.Collection;

import org.biogroovy.models.Article;
import org.biogroovy.models.Journal;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;

/**
 * This cluster maker clusters articles by the journals that published them.
 */
public class JournalClusterMaker implements IClusterMaker<Journal, Article> {

	@Override
	public Multimap<Journal, Article> createCluster(
			Collection<Article> seqCollection) {
		
		Multimap<Journal, Article> map = ArrayListMultimap.create();
		
		for(Article article :seqCollection){
			map.put(article.getJournal(), article);
		}
		
		return map;
	}

}
