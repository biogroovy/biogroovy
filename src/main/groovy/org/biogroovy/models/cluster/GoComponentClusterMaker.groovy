package org.biogroovy.models.cluster;

import java.util.Collection;

import org.biogroovy.models.Gene;
import org.biogroovy.models.GeneOntology;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;

/**
 * This cluster maker clusters Genes by the Gene Ontology cellular component to
 * which they belong.
 */
public class GoComponentClusterMaker implements
		IClusterMaker<GeneOntology, Gene> {

	@Override
	public Multimap<GeneOntology, Gene> createCluster(
			Collection<Gene> seqCollection) {

		Multimap<GeneOntology, Gene> map = ArrayListMultimap.create();

		for (Gene gene : seqCollection) {
			for (GeneOntology go : gene.getGoComponentList()) {
				map.put(go, gene);
			}
		}

		return map;
	}

}
