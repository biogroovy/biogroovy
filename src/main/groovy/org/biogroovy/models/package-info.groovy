/**
 * This package contains models (POGOs - Plain Old Groovy Objects) used to store data.
 */
package org.biogroovy.models;