package org.biogroovy.models

import thewebsemantic.Id
import thewebsemantic.Namespace
import thewebsemantic.RdfProperty

import com.google.common.collect.ArrayListMultimap
import com.google.common.collect.Multimap



/**
 * This class represents a protein.
 */
@Namespace("http://edamontology.org/")
class Protein extends AbsSequence{


    /**
     * The SwissProt accession for the protein.
     */
	@RdfProperty("data_3021")
	@Id
    String uniprotAccession
    

    /** The uniprot symbol for the protein as opposed to the HGNC symbol. */
    String uniprotSymbol;

    /** A map of evidence keys to PubMed articles. The evidence keys are used as references through out the UniProt record.*/
    Map<String, Article> evidenceKeyMap = new HashMap<>();

    /** A map containing the feature types, and features for a given protein. */
    Multimap<FeatureType, AbsSequenceFeature> featureMap = ArrayListMultimap.create();
    
    /** A map whose keys are the PDB IDs and whose values are the associated Protein Structure Entry. */
    Map<String, ProteinStructureEntry> pdbMap = new HashMap<>();
    
    /** A map whose keys are the PDB Sum IDs and whose values are the associated Protein Structure Entry. */
    Map<String, ProteinStructureEntry> pdbSumMap = new HashMap<>();

	/** The protein domains. */
	Set<ProteinDomain> domains = new HashSet<>();
	
	Set<String> keywords = new LinkedHashSet<>();
	
	
    /**
     * Adds a feature to the protein model object.
     * @param feature The sequence feature to be added to the protein.
     */
    public void addFeature(AbsSequenceFeature feature){
    	featureMap.put(feature.type, feature);
    }



}

