package org.biogroovy.models

/**
 * This interface defines the methods required for Sequence-based classes.
 */
public interface Sequence {

    /**
     * This method gets the sequence.
     */
    String getSequence();

    /**
     * This method sets the sequence.
     * @param seq  The sequence.
     */
    void setSequence(String seq);


    /**
     * This method gets the accession for the sequence.
     */
    String getAccession();

    /**
     * This method sets the accession for the sequence.
     * @param accession  The accession for the sequence.
     */
    void setAccession(String accession);
	
}

