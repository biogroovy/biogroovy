package org.biogroovy.models


import thewebsemantic.Namespace
import thewebsemantic.RdfProperty

import com.google.common.collect.ArrayListMultimap
import com.google.common.collect.Multimap

/**
 * This is the base class for a sequence.
 *
 */
@Namespace("http://edamontology.org/")
class AbsSequence implements ISequence {

    /** The sequence. */
    @RdfProperty("data_2044")
    String sequence;

    /** The description or summary of the sequence. */
    String description;

    /** The accession. */
    @thewebsemantic.Id
    String accession;
	
	/**
	 * The  RefSeq accession, or a comma-delimited list of associated
	 * ref seq accessions.
	 */
	String refSeqAcc;
    
    /**
     * The EntrezGene ID for the gene.
     */
    @RdfProperty("data_1027")
    int entrezGeneId;

	/** The unigene ID. */
	@RdfProperty("data_1029")
	String unigeneId
    
    /**
     * The species for the protein.
     */
    @RdfProperty("data_1045")
    String species;
    
    /** The short name of a gene; a single word that does not contain white space characters.  It is typically derived from the gene name.*/
    @RdfProperty("data_1026")
    String symbol;
    
    /** Symbol of a gene approved by the HUGO Gene Nomenclature Committee. */
    @RdfProperty("data_1791")
    String name;

    /**
     * A list of pathways associated with this particular sequence.
     */
    Multimap<String, Pathway> pathways = ArrayListMultimap.create();

    /** A list of phenotypes associated with this particular sequence. */
    List<Phenotype> phenotypes = new ArrayList<>();

    /**
     * A map containing the database names as keys, and database IDs as values.
     */
    Map<String, String> references = new HashMap<>();

    /**
     * A list of Gene Ontology entries for the gene.
     */
    List<GeneOntology> goFunctionList = new ArrayList<>();

    /**
     * A list of the cellular processes in which the gene is involved.
     */
    List<GeneOntology> goProcessList = new ArrayList<>();

    /**
     * A list of the cellular components in which the gene is involved.
     */
    List<GeneOntology> goComponentList = new ArrayList<>();

    /**
     * A list of synonyms for the gene.
     */
    List<String> synonyms = new ArrayList<>();

    /**
     * A list of articles (PubMed articles).
     */
    List<Article> articles = new ArrayList<>();
    
    @RdfProperty("data_1153")
    String omimId = null;

	Set<String> ensemblIds = new HashSet<>();
    
    public List<Article> getArticles(){
        return articles;
    }
    
    /**
     * Sets the list of articles associated with the sequence.
     * @param articleList the list of articles associated with the sequence.
     */
    public void setArticles(List<Article> articleList){
        this.articles = articleList;
    }
    
    @Override
    public String getSequence(){
        return this.sequence;
    }

    /**
     * Sets the sequence.
     * @param seq the sequence.
     */
    public void setSequence(String seq){
        this.sequence = seq;
    }

    @Override
    public String getAccession(){
        return this.accession;
    }

    /**
     * Sets the accession for the sequence.
     * @param acc the accession for the sequence.
     */
    public void setAccession(String acc){
        this.accession = acc;
    }

    @Override
    public String getSymbol() {
        return this.symbol;
    }
    
    /**
     * Sets the symbol of the sequence.
     * @param symbol the symbol of the sequence.
     */
    public void setSymbol(String symbol){
        this.symbol = symbol;
    }

    @Override
    public String getName() {
        return this.name;
    }
    
    /**
     * Sets the name of the sequence.
     * @param name the name of the sequence.
     */
    public void setName(String name){
        this.name = name;
    }
    
    @Override
    public String getDescription(){
        return this.description;
    }
    
    public void setDescription(String desc){
        this.description = desc;
    }
    
    @Override
    public String getSpecies(){
        return this.species;
    }
    
    public void setSpecies(String species){
        this.species = species;
    }

	@Override
	public String getModelId() {
		return accession;
	}
    
    
    
}
