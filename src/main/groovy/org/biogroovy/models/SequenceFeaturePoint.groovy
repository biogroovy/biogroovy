package org.biogroovy.models;

/**
 * This class represents a single point feature, such as a SNP.
 */
class SequenceFeaturePoint extends AbsSequenceFeature{
	
    /** The index of the feature within the sequence.*/
	int location;
}