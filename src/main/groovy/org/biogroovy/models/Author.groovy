
package org.biogroovy.models

import thewebsemantic.Id 

/**
 * This class represents the author for an article found in a PubMed record.
 */
class Author {

    /**
     * The first name of the author.
     */
    String firstname;

    /**
     * The last name of the author
     */
    String lastname;
	
	/**
	 * The initials of the author
	 */
	String initials;

    /** The orcid ID of the author */
    String orcid;

    /** The twitter ID of the author */
    String twitterId;

    /** The email address of the author. */
    String email;
 

	@Id
    public String toString(){
        return "${lastname}, ${firstname}"
    }
	
}

