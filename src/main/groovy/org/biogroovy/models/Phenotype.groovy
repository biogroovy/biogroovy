package org.biogroovy.models;

/**
 * This class represents a biological phenotype.
 */
class Phenotype {

	/** The name of the Phenotype. */
	String name;

	/** The description of the Phenotype. */
	String description;

	/** The med gen ID. */
	String medGenId;

	/** The OMIM ID. */
	String omimId;

	/** A list of articles that support the phenotype. */
	List<Article> refs = new ArrayList<>();

	@Override
	public String toString() {
		return "Phenotype [name=" + name + ", description=" + description
				+ ", medGenId=" + medGenId + ", omimId=" + omimId + ", refs="
				+ refs + "]";
	}
	
	

}