package org.biogroovy.models

import thewebsemantic.Id 
import thewebsemantic.Namespace 
import thewebsemantic.RdfProperty 

/**
 * This class represents a Gene. 
 */
@Namespace("http://edamontology.org/")
class Gene extends AbsSequence{

    /**
     * A list of transcripts created from the gene.
     */
    List<Transcript> transcripts = new ArrayList<Transcript>();

    /**
     * A list of proteins
     */
    List<Protein> proteins = new ArrayList<Protein>();


	
	public String toString(){
		return "Gene:${properties}"
	}
}

