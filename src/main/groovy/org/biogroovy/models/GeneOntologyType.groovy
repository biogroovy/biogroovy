package org.biogroovy.models;

/**
 * This enum contains the gene ontology term types.
 */
public enum GeneOntologyType {
    
    
   COMPONENT("C","Component"), 
   FUNCTION("F", "Function"), 
   PROCESS("P", "Process");
   
   /** The prefix for gene ontology term type. */
   String prefix;
   
   /** The text for the gene ontology term type. */
   String text;
   
   GeneOntologyType(String prefix, String text){
        this.prefix = prefix;
        this.text = text;
   }
   
   /**
    * This method identifies the GeneOntologyType based on the prefix.
    * @param val a string that starts with one of the identified prefixes.
    * @return  a GeneOntologyType or null if not identifiable.
    */
   public static GeneOntologyType identifyType(String val){
       String pref = val.substring(0, 1);
       GeneOntologyType currType = null;
       for(GeneOntologyType type : values()){
           if (type.prefix.equals(pref)){
               currType = type;
           }
       }
       
       return currType;
   }
   
   /**
    * This method gets the pretty version of the gene ontology type text.
    */
   public String toString(){
       return text;
   }
   
   
}
