#!/usr/bin/env groovy
package org.biogroovy.samples;
import org.biogroovy.eutils.*;
import org.biogroovy.io.eutils.EntrezGeneReader;
import org.biogroovy.models.*;

import java.beans.*;

import javax.xml.parsers.DocumentBuilderFactory
import javax.xml.xpath.*;
import javax.xml.namespace.QName;

def inputfile = "/Users/markfortner/Documents/pancreatic_cancer/core_signalling.csv"

// search for gene id
File file = new File(inputfile);
FileOutputStream xmlFile = new FileOutputStream(new File("/Users/markfortner/Documents/pancreatic_cancer/core_signalling.xml"))

xmlFile << "<results>\n";
XMLEncoder xenc = new XMLEncoder(xmlFile);
EntrezGeneReader reader = new EntrezGeneReader();
Gene gene = null;
String[] data = null;

file.eachLine {line -> 
	if (!line.startsWith("#")){
	data = line.split("\t");
	println data[0];
	if (data.size() == 2){
		gene = reader.read(data[1]);
		xenc.writeObject(gene);
		xmlFile.flush();
		gene = null;
	}
	}




}
xmlFile << "</results>\n"

//out.flush();
//out.close();

// select first hit


