#!/usr/bin/env groovy
package org.biogroovy.samples;
import org.biogroovy.eutils.*;
import org.biogroovy.models.*;
import java.beans.*;

import javax.xml.parsers.DocumentBuilderFactory
import javax.xml.xpath.*;
import javax.xml.namespace.QName;


def inputfile = "/Users/markfortner/Documents/pancreatic_cancer/core_signalling.list.txt"
def xpathStr = "/eSearchResult/IdList/Id[1]"

// search for gene id
File file = new File(inputfile);
File outFile = new File("/Users/markfortner/Documents/pancreatic_cancer/core_signalling.csv");

def out = outFile.newOutputStream();

file.eachLine {geneName -> 
def root = EUtilsURLFactory.searchXML( EUtilsURLFactory.DB_GENE, "${geneName}+AND+Homo+Sapiens[Organism]")
XPathFactory factory = XPathFactory.newInstance();
def xpath = factory.newXPath();
def Id = xpath.evaluate(xpathStr, root, XPathConstants.STRING)

if (Id != null && Id != ""){
	
	out << "${geneName}\t${Id}\n"
}



}
out.flush();
out.close();


