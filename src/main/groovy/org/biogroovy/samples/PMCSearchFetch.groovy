#!/usr/bin/env groovy
package org.biogroovy.samples

/**
 * This script performs a search for Pancreatic Cancer Metastasis papers which are
 * available as free fulltext.  It then downloads each of these papers in XML format.
 */
def eUrl = "http://eutils.ncbi.nlm.nih.gov/entrez/eutils/esearch.fcgi?db=pmc&term=metastasis+AND+pancreatic+cancer+AND+free+fulltext[filter]";

def fetchUrl = "http://eutils.ncbi.nlm.nih.gov/entrez/eutils/efetch.fcgi?db=pmc"

def doc = new XmlSlurper().parse(new URL(eUrl).openStream());
doc.IdList.Id.each(){
    println "pmcid: ${it} "
    String url = fetchUrl + "&id=${it}";
    String filename = "${it}.xml"
    def file = new FileOutputStream(filename)
    def out = new BufferedOutputStream(file)
    out << new URL(url).openStream()
    out.close()
};

