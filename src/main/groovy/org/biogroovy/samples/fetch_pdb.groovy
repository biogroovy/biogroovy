package org.biogroovy.samples;

import org.biojava.bio.structure.*
import org.biojava.bio.structure.io.*


// also works for gzip compressed files
 String filename =  "/home/mfortner/pdbfile.ent" ;

 PDBFileReader pdbreader = new PDBFileReader();

 // the following parameters are optional:

 //the parser can read the secondary structure
 // assignment from the PDB file header and add it to the amino acids
 pdbreader.setParseSecStruc(true);

 // align the SEQRES and ATOM records, default = true
 // slows the parsing speed slightly down, so if speed matters turn it off.
 pdbreader.setAlignSeqRes(true);

 // parse the C-alpha atoms only, default = false
 pdbreader.setParseCAOnly(false);

 // download missing PDB files automatically from EBI ftp server, default = false
 pdbreader.setAutoFetch(true);

 try{
     Structure struc = pdbreader.getStructure(filename);

     System.out.println(struc);

     GroupIterator gi = new GroupIterator(struc);

     while (gi.hasNext()){

           Group g = (Group) gi.next();

           if ( g instanceof AminoAcid ){
               AminoAcid aa = (AminoAcid)g;
               Map sec = aa.getSecStruc();
               Chain  c = g.getParent();
               System.out.println(c.getName() + " " + g + " " + sec);
           }
     }

 } catch (Exception e) {
     e.printStackTrace();
 }
