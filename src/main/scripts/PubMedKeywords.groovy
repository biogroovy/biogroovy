#!/usr/bin/env groovy
package scripts

/**
 * This script injects pubmed metadata into a pdf.
 */

@Grapes([
@Grab(group='org.apache.ivy', module='ivy', version='2.2.0'),
@Grab(group='org.apache.pdfbox', module='pdfbox', version='1.7.1'),
@Grab(group='org.biogroovy', module='biogroovy',version='1.1')
])
import org.apache.pdfbox.pdmodel.common.*;
import org.apache.pdfbox.pdmodel.*;
import org.biogroovy.eutils.PubMedArticleReader;
import org.biogroovy.models.*;
import groovy.util.CliBuilder;

String file1 = "/Users/markfortner/Downloads/1471-2105-10-73.pdf"
String pmid = 19245720;

def cli = new CliBuilder(usage: 'PubMedKeywords.groovy -f <inputFile> ');
cli.with{
	h(longOpt: 'help','PubMedKeywords.groovy -f <inputFile>')
	f(longOpt: 'inputFile', args:1, argName:'file','The PDF to be processed')
}

def options = cli.parse(args)

if (!options){
	return;
}

if (options.h){
	cli.usage();
	return;
}

File file = new File(options.f.value);
if (!file.exists()){
	println "The file: '${options.f}' does not exist";
	return;
}

PDDocument document = PDDocument.load(options.f);
PDDocumentCatalog cat = document.documentCatalog;
PDDocumentInformation info = document.getDocumentInformation();
println "author: " + info.author;
println "title: " + info.title;
println "keywords:" + info.keywords;

