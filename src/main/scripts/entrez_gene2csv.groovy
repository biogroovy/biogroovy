#!/usr/bin/env groovy
package scripts;
/**
 * This script parses a set of entrez gene XML files and outputs a csv file.
 */

import org.biogroovy.io.*;
import org.biogroovy.models.Gene;
import org.biogroovy.eutils.*;

String dirStr = "/Users/markfortner/Documents/pancreatic_cancer/data";
File dir = new File(dirStr);

EntrezGeneReader reader = new EntrezGeneReader();
Gene2CSVWriter writer = new Gene2CSVWriter();
OutputStream os = new FileOutputStream(new File(dirStr +"/data.csv"));
Gene gene = null;
dir.eachFileMatch( ~".*.xml",{ 
	println "processing: ${it}"
	gene = reader.readFile("${it}");
	writer.output(gene, os);
});


