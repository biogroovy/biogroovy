package org.biogroovy.eutils;

import org.biogroovy.io.eutils.OmimReader;
import org.biogroovy.models.Omim 
import org.junit.Ignore 
import org.junit.Test 

import static org.junit.Assert.*;

class OmimReaderTest {
	
	OmimReader reader = new OmimReader();
	
	@Ignore
	public void testReadString(){
		Omim omim = reader.read("600110");
		validateOmim(omim);
	}
	
	@Test
	public void testReadInputStream(){
		Omim omim = reader.read(new FileInputStream("src/test/resources/omim.xml"));
		validateOmim(omim);
	}
	
	private validateOmim(Omim omim){
		assertNotNull("The omim object was null",omim)
		assertNotNull("The mimId was null",omim.mimId);
		assertNotNull("The title was null",omim.title);
		assertNotNull("The symbol was null", omim.symbol);
		assertNotNull("The locus was null", omim.symbol);
		assertFalse("The aliases were empty", omim.aliases.isEmpty());
		assertNotNull("The aliases were null", omim.aliases.get(0))
		assertFalse("The textElements were empty", omim.textElements.isEmpty())
		assertFalse("The mapping elements were empty", omim.cytoMapping.isEmpty())
		assertFalse("The pathogenesis elements were empty", omim.pathogenesis.isEmpty())
		assertFalse("The molecular genetics elements were empty", omim.molecularGenetics.isEmpty())
		assertFalse("The animal model elements were empty", omim.animalModel.isEmpty())
		assertFalse("The clinical features elements were empty", omim.clinicalFeatures.isEmpty())
		assertFalse("The references were empty", omim.references.isEmpty())
		assertFalse("The medlineLinks were empty", omim.medlineLinks.isEmpty())
		assertFalse("The proteinLinks were empty",omim.proteinLinks.isEmpty())
		assertFalse("The nucleotideLinks were empty",omim.nucleotideLinks.isEmpty())
		assertFalse("The clinical synopsis was empty",omim.clinicalSynopsis.isEmpty())
	}

}
