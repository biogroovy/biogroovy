
package org.biogroovy.eutils;

import org.biogroovy.io.AbsReader;
import org.biogroovy.io.eutils.EntrezGeneReader;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.*;

import javax.xml.parsers.DocumentBuilderFactory;


/**
 *
 * @author markfortner
 */
public class AbstractReaderTest {

    static String CAR_RECORDS = '''
   	 <records>
    <car name='HSV Maloo' make='Holden' year='2006'>
      <country>Australia</country>
      <record type='speed'>Production Pickup Truck with speed of 271kph</record>
    </car>
    <car name='P50' make='Peel' year='1962'>
      <country>Isle of Man</country>
      <record type='size'>Smallest Street-Legal Car at 99cm wide and 59 kg in weight</record>
    </car>
    <car name='Royale' make='Bugatti' year='1931'>
      <country>France</country>
      <record type='price'>Most Valuable Car at $15 million</record>
    </car>
  </records>
		''';
   

    static def records;

    public AbstractReaderTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {

    	//def builder     = DocumentBuilderFactory.newInstance().newDocumentBuilder()
    	//def inputStream = new ByteArrayInputStream(CAR_RECORDS.bytes)
    	//def records     = builder.parse(inputStream).documentElement
    	def records = new XmlParser().parseText(CAR_RECORDS);

    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    @Test
    public void parseNumber(){
        AbsReader reader = new EntrezGeneReader();
        def builder     = DocumentBuilderFactory.newInstance().newDocumentBuilder()
    	def inputStream = new ByteArrayInputStream(CAR_RECORDS.bytes)
    	def records     = builder.parse(inputStream).documentElement
        Integer year = reader.parseNumber(records, "//car[@make='Holden']/@year");
        assertNotNull("The year is null", year);
        assertEquals("The year is empty", year, 2006);
        println year;
    }

    @Test
    public void parseString(){
    	AbsReader reader = new EntrezGeneReader();
        def builder     = DocumentBuilderFactory.newInstance().newDocumentBuilder()
    	def inputStream = new ByteArrayInputStream(CAR_RECORDS.bytes)
    	def records     = builder.parse(inputStream).documentElement;
        
        String name = reader.parseString(records, "//car[@year='2006']/@name");
        assertNotNull("The name is null", name);
        assertFalse("The name is empty", name.equals(""));
        println name;
    }

    @Test
    public void parseList(){
    	AbsReader reader = new EntrezGeneReader();
        def builder     = DocumentBuilderFactory.newInstance().newDocumentBuilder()
    	def inputStream = new ByteArrayInputStream(CAR_RECORDS.bytes)
    	def records     = builder.parse(inputStream).documentElement;
        
        List<String> list = reader.parseList(records, "//car/@name");
        assertNotNull("The list is null", list);
        assertFalse("The list is empty", list.isEmpty());
        assertTrue("The list is the wrong size: ${list.size()}", list.size() == 3);
        Set<String> values = ['HSV Maloo','P50', 'Royale'];
        list.containsAll values;
        println list;
    }

}