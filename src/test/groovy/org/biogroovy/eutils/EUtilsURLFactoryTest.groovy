
package org.biogroovy.eutils

import org.junit.*;

/**
 * @author mfortner
 *
 */
public class EUtilsURLFactoryTest{
	
	@Test
	public void testGetFasta(){
		EUtilsURLFactory.getFasta("nucleotide","119395733", null);
		File file = new File("119395733.fasta");
		Assert.assertTrue(file.exists());
		Assert.assertTrue(file.length() > 0);
	}
	
	@Test
	public void testGetXML(){
		EUtilsURLFactory.getXML("gene", "675", null);
		File file = new File("675.xml");
		Assert.assertTrue(file.exists());
		Assert.assertTrue(file.length() > 0);
	}
	
}
