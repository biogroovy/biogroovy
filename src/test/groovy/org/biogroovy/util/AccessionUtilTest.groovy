package org.biogroovy.util;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 *
 * @author markfortner
 */
class AccessionUtilTest {

    static Map<String, String> ACCESSION_TEST_MAP = new LinkedHashMap<String, String>();


    @Test
    public void testAccessionTypes(){
		ACCESSION_TEST_MAP.put(AccessionType.ALT_COMPLEX_GENOMIC, "AC_123456");
		ACCESSION_TEST_MAP.put(AccessionType.ALT_PROTEIN_RECORD, "AP_123456");
		ACCESSION_TEST_MAP.put(AccessionType.COMPLETE_GENOMIC, "NC_123456");
		ACCESSION_TEST_MAP.put(AccessionType.GENOMIC_REC, "NS_123456");
		ACCESSION_TEST_MAP.put(AccessionType.INCOMPLETE_GENOMIC, "NG_123456");
		ACCESSION_TEST_MAP.put(AccessionType.INT_GENOMIC_ASSEMBLY, "NT_123456");
		ACCESSION_TEST_MAP.put(AccessionType.INT_GENOMIC_ASSEMBLY2, "NW_123456");
		ACCESSION_TEST_MAP.put(AccessionType.INT_GENOMIC_ASSEMBLY2, "NW_123456789");
		ACCESSION_TEST_MAP.put(AccessionType.NON_CODE_TRANS,"NR_123456");
		ACCESSION_TEST_MAP.put(AccessionType.PROTEIN_PROD2, "NP_123456");
		ACCESSION_TEST_MAP.put(AccessionType.PROTEIN_PROD2, "NP_123456789");
		ACCESSION_TEST_MAP.put(AccessionType.PROTEIN_PROD3, "XP_123456");
		ACCESSION_TEST_MAP.put(AccessionType.PROTEIN_PROD3, "XP_123456789");
		ACCESSION_TEST_MAP.put(AccessionType.PROTEIN_PROD4, "YP_123456")
		ACCESSION_TEST_MAP.put(AccessionType.PROTEIN_PROD4, "YP_123456789");
		ACCESSION_TEST_MAP.put(AccessionType.PROTEIN_PROD5, "ZP_12345678");
		
        ACCESSION_TEST_MAP.each(){key, item ->
            assertEquals("Unable to identify accession of type: " + key,key, AccessionUtil.getAccessionType(item));
			
        }
    }

	
}

