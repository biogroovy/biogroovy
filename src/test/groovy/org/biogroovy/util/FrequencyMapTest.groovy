package org.biogroovy.util;

import static org.junit.Assert.*

import java.util.Map.Entry

import org.junit.Before
import org.junit.Test

/**
 * This class verifies the functionality of the FrequencyMap class.
 */
class FrequencyMapTest {
	
	private FrequencyMap<String> freqMap = null;
	
	@Before
	public void init(){	
		freqMap = new FrequencyMap<String>();
	}


	/**
	 * Verifies that we can add entries to the frequency map and 
	 * that the frequencies are updated appropriately.
	 */
	@Test
	public void testAdd() {
		
		// verify that we have not yet added an entry
		assertEquals(0, freqMap.getCount("Test1"));
		
		// verify that we've
		freqMap.add("Test1");
		assertEquals(1,freqMap.getCount("Test1"));
		
		freqMap.add("Test1");
		assertEquals(2, freqMap.getCount("Test1"));
		
		freqMap.add("Test2");
		assertEquals(1, freqMap.getCount("Test2"));
		
		assertEquals(2, freqMap.size());
		
		println freqMap.getEntriesAsPercentages();
	}

	/**
	 * Verify that we can remove entries from the map
	 * and that the frequencies are updated appropriately.
	 */
	@Test
	public void testRemove() {
		
		String key = "Test1";
		freqMap.add(key);
		freqMap.add(key);
		assertEquals(2, freqMap.getCount(key));
		
		freqMap.remove(key);
		assertEquals(1, freqMap.getCount(key));
		
		freqMap.remove(key);
		assertEquals(0, freqMap.getCount(key));
		
		assertTrue(freqMap.isEmpty());

	}

	/**
	 * Verify that we can remove all entries for a single key.
	 */
	@Test
	public void testRemoveAll() {
		String key = "Test1";
		freqMap.add(key);
		freqMap.add(key);
		assertEquals(2, freqMap.getCount(key));
		
		freqMap.removeAll(key);
		assertEquals(0, freqMap.getCount(key));
		assertTrue(freqMap.isEmpty());
	}

	/**
	 * Verify that the entry set for the frequency map returns
	 * the entries in order of highest to lowest frequency.
	 */
	@Test
	public void testEntrySet() {
		String[] keys = ["Test1","Test2"];
		freqMap.addAll(keys);
		assertEquals(2, freqMap.size());
		
		freqMap.add(keys[1]);
		
		Set entrySet = freqMap.entrySet();
		List<Entry<String, Integer>> entryList = new ArrayList<Entry<String, Integer>>();
		entryList.addAll(entrySet);
		
		// verify that the frequency count is in highest to lowest order
		assertEquals(2, entryList.get(0).value);
		assertEquals(1, entryList.get(1).value);
	}


	/**
	 * Verify that we can clear the frequency map.
	 */
	@Test
	public void testClear() {
		freqMap.add("Test1");
		freqMap.add("Test2");
		assertEquals(2, freqMap.size());
		
		freqMap.clear();
		assertTrue(freqMap.isEmpty());
	}



}
