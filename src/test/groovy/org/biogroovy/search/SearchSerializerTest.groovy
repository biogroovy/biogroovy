package org.biogroovy.search;

import static org.junit.Assert.*
import groovy.util.logging.Slf4j

import org.biogroovy.search.eutils.PubMedSearchEngine;
import org.junit.Test

@Slf4j
class SearchSerializerTest {


	@Test
	public void test() {

		PubMedSearchEngine se = new PubMedSearchEngine();
		Search search = se.createSearch();
		search.setName("Test Search");
		search.setParameterValue("Term", "pancreatic cancer");

		SearchSerializer ser = new SearchSerializer();
		File tempDir = File.createTempDir()
		File tempFile = new File(tempDir, "search.temp");
		tempFile.setReadable(true);
		tempFile.setWritable(true);

		OutputStream os = tempFile.newOutputStream();
		ser.write(search, os);
		os.flush();
		os.close();


		File tempFile2 = new File(tempDir, "search.temp");

		String text = tempFile2.getText();
		log.info text;

		assertTrue(tempFile2.exists());
		assertTrue(tempFile2.size() > 0)

		BufferedInputStream inputStream = null;
		try{

			inputStream = tempFile2.newInputStream()
			Search search2 = ser.read(inputStream);

			assertEquals("name", search.name, search2.name);
			assertEquals("lastExecuted", search.getLastExecutedAsString(), search2.getLastExecutedAsString());
			assertEquals("searchEngineName", search.searchEngineName, search2.searchEngineName);
			assertEquals("numSearchParams", search.searchParams.parameters.size(), search2.searchParams.parameters.size());
			
		}catch (Exception ex){
			log.error(ex.getMessage(), ex);
		}

		tempDir.deleteOnExit();
	}
}
