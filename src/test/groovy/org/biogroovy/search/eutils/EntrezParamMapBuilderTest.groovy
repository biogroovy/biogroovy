package org.biogroovy.search.eutils;

import static org.junit.Assert.*
import groovy.util.logging.Slf4j

import org.biogroovy.eutils.EUtilsURLFactory;
import org.biogroovy.search.SearchParam
import org.biogroovy.search.SearchParamSet
import org.biogroovy.search.eutils.EntrezParamMapBuilder;
import org.junit.Before
import org.junit.Test


@Slf4j
class EntrezParamMapBuilderTest {
	
	SearchParam termParam = new SearchParam(name:"term",dataName:'Term', isTerm:true);
	SearchParam symbol = new SearchParam(name:'Symbol',isSubTerm:true, value:'KRAS');
	SearchParam org = new SearchParam(name:'Organism',isSubTerm:true, value:'Homo sapiens');
	SearchParam db = new SearchParam(name:'db',isEditable: false, isRequired:true, value:'pubmed');
	
	
	SearchParamSet paramSet = null;
	EntrezParamMapBuilder builder = null; 
	Map<String, String> paramMap = null;
	
	@Before
	public void setup(){
		paramSet = new SearchParamSet();
		builder = new EntrezParamMapBuilder();
		paramMap = new TreeMap<>();
	}

	/**
	 * Verify a minimal test case with a search 'term' and a number
	 * of 'subTerm' parameters.
	 */
	@Test
	public void testConstructTermEntry() {
		
		paramSet.addAll(termParam, symbol, org, db);
		
		builder.constructTermEntry(paramSet, paramMap);
		
		assertFalse("The parameter map was empty",paramMap.isEmpty());
		assertNotNull("The parameter map doesn't have a 'term' entry", paramMap.get("Term"));
		assertEquals(1,paramMap.size());
		
		println "'" + paramMap.get("term") + "'";
				
		termParam.value="something";
		builder.constructTermEntry(paramSet, paramMap);
		String termTxt = paramMap.get("Term");
		assertTrue("Invalid construction: " + termTxt, termTxt.startsWith("something AND"));
		println termTxt
		
	}
	
	@Test
	public void testUrlConstruction(){
		paramSet.addAll(termParam, symbol, org, db);
		Map<String, String> map = builder.createPopulatedParamMap(paramSet);
		println map;
		String url = EUtilsURLFactory.getURL(EUtilsURLFactory.EFETCH, map);
		println url;
	}
	
	
	

}
