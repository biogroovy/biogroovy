package org.biogroovy.search.eutils;

import static org.junit.Assert.*

import org.biogroovy.models.Article
import org.biogroovy.search.Search
import org.biogroovy.search.SearchParamSet
import org.biogroovy.search.SearchResult
import org.biogroovy.search.eutils.PubMedSearchEngine;
import org.junit.Test

class PubMedSearchEngineTest {
	
	PubMedSearchEngine searchEngine = new PubMedSearchEngine();

	@Test
	public void testGetName() {
		assertNotNull("The search engine name was null",searchEngine.getName());
	}

	@Test
	public void testGetURLTemplate() throws Exception{
		String template = searchEngine.getURLTemplate();
		println template;
		assertNotNull("The search engine url was null",template);
	}

	@Test
	public void testDoSearch() {
		List<SearchResult> results = searchEngine.doSearch([term:'pancreatic cancer']);
		assertNotNull("The results were null", results);
		assertFalse("The results were empty", results.isEmpty());
		assertEquals("Result count not correct",20, results.size())
		println results
	}
	
	/**
	 * Verify that we can set a search parameter and get a result.
	 */
	@Test
	public void testDoSearch2(){
		
		Search bSearch = searchEngine.createSearch();
		SearchParamSet paramSet = bSearch.getSearchParams();
		assertTrue("Term not found",paramSet.searchParams.containsKey("Term"));
		
		paramSet.setParameterValue("Term", "pancreatic cancer");
		
		searchEngine.doSearch(bSearch);
		List<Article> bArticleList = bSearch.results;
		assertNotNull("Article list was null",bArticleList);
		assertFalse("Article list was empty", bArticleList.isEmpty());
	}
	
	
	/**
	 * Verify that we can set the search term directly, and perform a search.
	 */
	@Test
	public void testDoSearch3(){
		
		Search bSearch = searchEngine.createSearch();
		SearchParamSet paramSet = bSearch.getSearchParams();
		assertTrue("Term not found",paramSet.searchParams.containsKey("Term"));
		
		paramSet.setTerm("pancreatic cancer");
		
		searchEngine.doSearch(bSearch);
		List<Article> bArticleList = bSearch.results;
		assertNotNull("Article list was null",bArticleList);
		assertFalse("Article list was empty", bArticleList.isEmpty());
	}


}
