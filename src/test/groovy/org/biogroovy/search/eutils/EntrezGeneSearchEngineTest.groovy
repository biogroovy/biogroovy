package org.biogroovy.search.eutils;

import static org.junit.Assert.*
import groovy.util.logging.Slf4j

import org.biogroovy.search.SearchResult
import org.biogroovy.search.eutils.EntrezGeneSearchEngine;
import org.junit.Test


@Slf4j
class EntrezGeneSearchEngineTest {
	
	EntrezGeneSearchEngine se = new EntrezGeneSearchEngine();

	@Test
	public void testGetName() {
		assertNotNull "The search engine name was null", se.getName();
	}

	@Test
	public void testGetURLTemplate() {
		String template = se.getURLTemplate();
		assertNotNull "The template was null", template;
		
		log.info "template: " + template;
	}

	/**
	 * Test doing the deprecated version of doSearch
	 */
	@Test
	public void testDoSearch() {
		List<SearchResult> results = se.doSearch([term:'KRAS[Symbol] AND Homo Sapiens[Organism]']);
		assertNotNull("The results were null", results);
		assertFalse("The results were empty", results.isEmpty());
		log.info "Results: " + results;
		results.each{ println it};
	}
	

}
