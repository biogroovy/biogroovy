package org.biogroovy.conf;

import static org.junit.Assert.*;

import org.junit.Test;

class BioGroovyConfigTest {

	@Test
	public void testGetConfig() {
		ConfigObject conf = BioGroovyConfig.getDefaultConfig();
				
		assertNotNull("The config object was null",conf)
		assertEquals("Unable to retrieve eutils.tool", "biogroovy",conf.eutils.tool)
		assertEquals("Unable to retrieve eutils.email", 'goofy@disney.com', conf.eutils.email)
		assertEquals("Unable to retrieve journaltocs.userid", 'goofy@disney.com', conf.journaltocs.userid)
		assertNotNull("Unable to retrieve entrezajax.userid", conf.entrezajax.userid)
		assertNotNull("Unable to retrieve mygeneinfo.email", conf.mygeneinfo.email)
	}

}
