package org.biogroovy.io;

import static org.junit.Assert.*

import org.biogroovy.models.Article
import org.junit.Test

class HTMLMetadataSlurperTest {

	@Test
	public void testReadInputStream() {
		HTMLMetadataSlurper slurper = new HTMLMetadataSlurper();
		Article article = slurper.read(getClass().getResourceAsStream("/article.html"));
		
		validateArticle(article);
		println article;
		println "keywords: " + article.keywords;
		println "abstract: " + article.abs;
		
	}
	
	private void validateArticle(Article article){
		assertNotNull("The article was not null", article);
		assertNotNull("The title was not null", article.title);
		assertNotNull("The abstract was not null", article.abs);
		assertFalse("The abstract was empty", article.abs=="");
		assertNotNull("The keywords were not null", article.keywords);
		assertFalse("The keywords were not empty", article.keywords.isEmpty());

	}

}
