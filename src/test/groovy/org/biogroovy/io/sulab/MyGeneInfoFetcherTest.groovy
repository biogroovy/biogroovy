package org.biogroovy.io.sulab;

import static org.junit.Assert.*
import groovy.json.JsonSlurper

import org.biogroovy.io.sulab.MyGeneInfoFetcher;
import org.biogroovy.models.Gene
import org.junit.Before
import org.junit.Test


class MyGeneInfoFetcherTest {
	
	MyGeneInfoFetcher fetcher = new MyGeneInfoFetcher(); 
	MyGeneInfoFetcher fetcher2 = new MyGeneInfoFetcher(); 
	


	@Test
	public void testFetch() {
		Gene gene = fetcher.fetch("675");
		validateGene(gene);	
	}

	@Test
	public void testGetUrl() {
		URL url = fetcher.getUrl("1234", null);
		assertNotNull("The url was null", url)
		assertTrue(url.toString().startsWith("http://mygene.info/v2/query?q=entrezgene:1234"));
	}

	@Test
	public void testGetDatabaseName() {
		String name = fetcher.getDatabaseName();
		assertNotNull("The database name was null", name);
	}

	/**
	 * Verify that we can fetch multiple genes.
	 */
	@Test
	public void testFetchAll() {
		List<Gene> geneList = fetcher.fetchAll("675,676");
		assertNotNull("The gene list was null",geneList)
		assertFalse("The gene list was empty", geneList.isEmpty())
		
	}

	/**
	 * This method parses the contents of JSON file, and validates the resulting gene.
	 */
	@Test
	public void testParseGeneMap() {
		Gene gene = new Gene();
		
		JsonSlurper slurper = new JsonSlurper();
		URL url = getClass().getResource("/675.json");
		
		assertNotNull("The input stream was null", url)
		
		def result = slurper.parse(url);
		
		Map<String, String> map = (Map<String, String>) result;
		assertNotNull("The slurper result was null", map)
		
		fetcher.parse(gene, map.hits[0])
		
		validateGene(gene);
	}
	
	private void validateGene(Gene gene){
		assertNotNull("The gene was null", gene)
		assertNotNull("The entrez gene id was null", gene.entrezGeneId);
		assertEquals("The symbol was wrong", "BRCA2",gene.symbol)
		assertTrue("The name was wrong", gene?.name?.startsWith("breast cancer 2"))
		assertNotNull("The description was null", gene.description)
		
		assertEquals("omimId was not correct","600185", gene.omimId);
		assertEquals("pharmgkb was not correct","PA25412", gene.references.'pharmgkb')
		assertEquals("unigene was not correct","Hs.34012", gene.unigeneId)
		
		assertNotNull("The GO component list was null", gene.goComponentList);
		assertFalse("The GO component list was empty", gene.goComponentList.isEmpty())
		println gene.goComponentList;
		
		assertNotNull("The GO function list was null", gene.goFunctionList);
		assertFalse("The GO function list was empty", gene.goFunctionList.isEmpty())
		println gene.goFunctionList
		
		assertNotNull("The GO process list was null", gene.goProcessList);
		assertFalse("The GO process list was null", gene.goProcessList.isEmpty())
		println gene.goProcessList
		
		assertNotNull("The pathways object was null", gene.pathways)
		assertFalse("The pathways object was empty", gene.pathways.isEmpty())
		println "pathways: " + gene.pathways;
		
		assertNotNull("The synonyms were null", gene.synonyms)
		assertFalse("The synonym list was empty", gene.synonyms.isEmpty())
		println "synonyms: " + gene.synonyms;
				
		assertNotNull("The genomic refseq was null",  gene.refSeqAcc)
		println "genomic refseq: ${gene.refSeqAcc}"
		
		assertNotNull("The article list was null", gene.articles)
		
	}

}
