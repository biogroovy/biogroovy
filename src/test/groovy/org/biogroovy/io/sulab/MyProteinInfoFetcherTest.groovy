package org.biogroovy.io.sulab;

import static org.junit.Assert.*
import groovy.json.JsonSlurper

import org.biogroovy.io.sulab.MyProteinInfoFetcher;
import org.biogroovy.models.Gene
import org.biogroovy.models.Protein
import org.junit.Before;
import org.junit.Test


class MyProteinInfoFetcherTest {
	
	MyProteinInfoFetcher fetcher = null
	MyProteinInfoFetcher fetcher2 = null
	
	@Before
	void before() {
		fetcher = new MyProteinInfoFetcher();
		fetcher2 = new MyProteinInfoFetcher();
	}
	

	@Test
	public void testFetch() {
		println "fetcher: ${fetcher}"
		Protein prot = fetcher.fetch("675");
		assertNotNull("The protein was null", prot)
		validateProtein(prot)
	}

	@Test
	public void testGetUrl() {
		
		URL url = fetcher.getUrl("1234", null)
		assertNotNull("The URL was null", url)
	}

	@Test
	public void testGetDatabaseName() {
		String name = fetcher.getDatabaseName();
		assertNotNull("The database name was null", name);
	}

	@Test
	public void testFetchAll() {
		List<Protein> protList = fetcher.fetchAll("675,676");
		assertNotNull(protList)
		assertFalse(protList.isEmpty())
		assertEquals(2, protList.size())
	}

	@Test
	public void testRead() {
		Protein protein = new Protein();
		
		InputStream is = getClass().getResourceAsStream("/675.json");
		protein = fetcher.read(is)
				
		validateProtein(protein);
	}
	
	private void validateProtein(Protein protein){
		assertNotNull("The gene was null", protein)
		assertNotNull("The entrez gene id was null", protein.entrezGeneId);
		assertEquals("The symbol was wrong", "BRCA2",protein.symbol)
		
		assertEquals("omimId was not correct","600185", protein.omimId);
		assertEquals("pharmgkb was not correct","PA25412", protein.references.'pharmgkb')
		assertEquals("unigene was not correct","Hs.34012", protein.unigeneId)
		
		assertNotNull("The GO component list was null", protein.goComponentList);
		assertFalse("The GO component list was empty", protein.goComponentList.isEmpty())
		println protein.goComponentList;
		
		assertNotNull("The GO function list was null", protein.goFunctionList);
		assertFalse("The GO function list was empty", protein.goFunctionList.isEmpty())
		println protein.goFunctionList
		
		assertNotNull("The GO process list was null", protein.goProcessList);
		assertFalse("The GO process list was null", protein.goProcessList.isEmpty())
		println protein.goProcessList
		
		assertNotNull("The pathways object was null", protein.pathways)
		assertFalse("The pathways object was empty", protein.pathways.isEmpty())
		println "pathways: " + protein.pathways;
		
		assertNotNull("The synonyms were null", protein.synonyms)
		assertFalse("The synonym list was empty", protein.synonyms.isEmpty())
		println "synonyms: " + protein.synonyms;
		
		assertNotNull("The genomic refseq was null",  protein.refSeqAcc)
		
		assertNotNull("The domains were null", protein.domains);
		assertFalse("The domains were empty", protein.domains.isEmpty())
		println "domains: " + protein.domains;
		
		assertNotNull("The ensembl ids were null", protein.ensemblIds)
		assertFalse("The ensembl ids were empty", protein.ensemblIds.isEmpty())
		println "ensemblIds: " + protein.ensemblIds
		
	}

}
