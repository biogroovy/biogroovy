package org.biogroovy.io.clinicaltrials;

import static org.junit.Assert.*

import org.biogroovy.io.clinicaltrials.ClinicalTrialReader;
import org.biogroovy.models.ClinicalTrial
import org.junit.Test

class ClinicalTrialReaderTest {

	@Test
	public void testReadInputStream() {
		
		ClinicalTrialReader reader = new ClinicalTrialReader();
		
		InputStream instream = getClass().getResourceAsStream("/clinicaltrials.result.xml");
		ClinicalTrial trial = reader.read(instream);
		
		assertNotNull("The trial object was null", trial);
		assertNotNull("The briefTitle was null", trial.briefTitle)
		assertNotNull("The briefSummary was null", trial.briefSummary);
		assertNotNull("The clinicalTrialId was null", trial.clinicalTrialId);
		assertNotNull("The clinicalTrialURI was null", trial.clinicalTrialUri);
		assertNotNull("The detailedDescription was null", trial.detailedDescription);
		assertNotNull("The officialTitle was null", trial.officialTitle);
		
		
		assertFalse("The mesh terms were empty", trial.meshTerms.isEmpty());
		println "\nmeshTerms: ";
		trial.meshTerms.each{println it}
		
		assertFalse("The tags were empty", trial.tags.isEmpty());
		println "tags: " + trial.tags;
		
		assertFalse("The collaborator list was empty", trial.collaborators.isEmpty());
		println "\ncollaborators: ";
		trial.collaborators.each{println it}
		
		
		assertFalse("The sponsor list was empty", trial.sponsors.isEmpty());
		println "\nsponsors: " + trial.sponsors;
		trial.sponsors.each{println it}
		
		assertFalse("The intervention list was empty", trial.interventions.isEmpty());
		println "\ninterventions: ";
		trial.interventions.each{println it}
		
		assertNotNull("The overall status was empty",trial.overallStatus);
		assertNotNull("The phase was empty", trial.phase)
		assertNotNull("The primary completion date was empty",trial.primaryCompletionDate);
		assertNotNull("The study type was null", trial.studyType);
		assertNotNull("The study design was null", trial.studyDesign);
		
		assertFalse("The primary outcomes was empty", trial.primaryOutcomes.isEmpty());
		println "\nprimary outcomes: ";
		trial.primaryOutcomes.each{println it}
		
		assertFalse("The eligibility list was empty", trial.eligibility.isEmpty());
		println "\neligibility";
		trial.eligibility.each {println it}
	}

}
