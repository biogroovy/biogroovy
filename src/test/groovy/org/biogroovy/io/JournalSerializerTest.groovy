package org.biogroovy.io;

import static org.junit.Assert.*

import org.biogroovy.io.local.JournalReader;
import org.biogroovy.io.local.JournalWriter;
import org.biogroovy.models.Journal
import org.biogroovy.models.RSSFeed
import org.junit.Test

class JournalSerializerTest {

	@Test
	public void test() {
		Serializer ser = new Serializer(name:'Journal', reader:new JournalReader(), writer: new JournalWriter());

		
		Journal journal = new Journal(title:'Test Journal',
			issn:'1234', 
			startPage:1, 
			endPage:2, 
			issue:'1', 
			volume:'1', 
			publicationDate: new Date(),
			imageUrl:'http://whatever.com',
			siteUrl:'http://whatever.com',
			rssUrl:'http://whatever.com'
			);		
		
		File file = File.createTempFile("test",".journal.xml");
		file.deleteOnExit();
		
		
		FileOutputStream out = new FileOutputStream(file);
		ser.writer.write(journal, out);
		
		out.flush();
		out.close();
		
		println file;
		println file.text
		
		assertTrue("The file doesn't exist",file.exists());
		assertTrue("The file's size is too small",file.length() > 0);
		
		BufferedInputStream inStream = file.newInputStream();
		
		Journal journal2 = ser.reader.read(inStream);
		assertNotNull("The journal was null", journal2);
		
		assertEquals(journal.title, journal2.title);
		assertEquals(journal.startPage, journal2.startPage);
		assertEquals(journal.endPage, journal2.endPage);
		assertEquals(journal.issn, journal2.issn);
		assertEquals(journal.issue, journal2.issue);
		assertEquals(journal.volume, journal2.volume);
		assertEquals(journal.imageUrl, journal2.imageUrl);
		assertEquals(journal.rssUrl, journal2.rssUrl);
		assertEquals(journal.siteUrl, journal2.siteUrl);
		
		inStream.close();
	}

}
