package org.biogroovy.io;

import static org.junit.Assert.*

import org.biogroovy.io.eutils.PubMedSlurper
import org.biogroovy.models.Article
import org.junit.Test


class FetchManagerTest {

	@Test
	public void testDownloadFiles() {

	    println "Downloading articles"
	    FetchManager fetchMgr = new FetchManager();
	    IFetcher<Article> slurper = new PubMedSlurper();
	    
	    File root = new File(System.getProperty("user.home"), ".biogroovy");
	    println root.getAbsolutePath()
	    try{
	       fetchMgr.downloadFiles(slurper,"pubmed.xml", root,  "10022822","10023327"); //'10023947','10051160'
	    }catch (Exception ex){
	        ex.printStackTrace();
	    }
	}

}
