package org.biogroovy.io;

import static org.junit.Assert.*
import groovy.util.logging.Slf4j

import org.biogroovy.io.uniprot.UniProtReader
import org.biogroovy.models.Article
import org.biogroovy.models.GeneOntology
import org.biogroovy.models.GeneOntologyType
import org.biogroovy.models.Journal
import org.biogroovy.models.Protein
import org.junit.Ignore
import org.junit.Test

/**
 * This class verifies that we can read a uniprot record.
 */
@Slf4j
class UniProtReaderTest {

    @Test
    public void test() {
        InputStream inputStream = getClass().getResourceAsStream("/UniProtKB.xml");
		
        assertNotNull("The input stream was null",inputStream);
        
        UniProtReader reader = new UniProtReader();
        Protein prot = reader.read(inputStream);
        assertNotNull("The protein was null", prot);
        assertEquals("Aurora kinase A", prot.name);
        assertEquals("AURKA", prot.symbol);
        assertEquals("AURKA_HUMAN", prot.uniprotSymbol);
        assertEquals("Homo sapiens",prot.species);
        assertNotNull("The accession was null", prot.uniprotAccession);
        
        assertNotNull("The sequence was null",prot.sequence);
        
        assertFalse("The sequence features were empty", prot.featureMap.isEmpty());
        
        // validate the synonyms
        assertNotNull(prot.synonyms);
        assertFalse("The synonym list was empty",prot.synonyms.isEmpty());
        
        validateGoTerms(GeneOntologyType.COMPONENT, prot.goComponentList);
        validateGoTerms(GeneOntologyType.FUNCTION, prot.goFunctionList);
        validateGoTerms(GeneOntologyType.PROCESS, prot.goProcessList);
        
        assertNotNull("The article list was null", prot.articles);
        assertFalse("The article list was empty", prot.articles.empty);
        
        prot.articles.each{ article ->
            validateArticle(article)
        }
        
        assertFalse("The pathway list was empty", prot.pathways.empty);
        prot.pathways.values().each{path ->
            assertNotNull("Pathway name was null: " + path, path.name);
            assertNotNull("Pathway accession was null: " + path, path.accession);
            assertNotNull("Pathway datasource was null: " + path, path.datasource);
			println path;
        }
        
        assertNotNull("The omimId was null", prot.omimId);
        assertNotNull("The geneId was null", prot.entrezGeneId);

        assertFalse("The evidence map was empty", prot.evidenceKeyMap.isEmpty());
        
        assertFalse("The pdb map was empty", prot.pdbMap.isEmpty())
        assertFalse("The pdb sum map was empty", prot.pdbSumMap.isEmpty());
		
		assertFalse("The keyword set was empty", prot.keywords.isEmpty())
		println prot.keywords;
              
    }
	
	@Test
	public void testSyndecan2(){
		InputStream inputStream = getClass().getResourceAsStream("/P34741.xml");
		UniProtReader reader = new UniProtReader();
		Protein prot = reader.read(inputStream);
		
		assertFalse("The pathway list was empty", prot.pathways.empty);
		prot.pathways.values().each{path ->
			assertNotNull("Pathway name was null: " + path, path.name);
			assertNotNull("Pathway accession was null: " + path, path.accession);
			assertNotNull("Pathway datasource was null: " + path, path.datasource);
			println path;
		}
	}
	
	@Ignore
	public void testSyndecan2a(){
		UniProtReader reader = new UniProtReader();
		Protein prot = reader.fetch("P34741");
		
		assertFalse("The pathway list was empty", prot.pathways.empty);
		prot.pathways.values().each{path ->
			assertNotNull("Pathway name was null: " + path, path.name);
			assertNotNull("Pathway accession was null: " + path, path.accession);
			assertNotNull("Pathway datasource was null: " + path, path.datasource);
			println path;
		}
	}
    
    /**
     * This method validates that the gene ontology term list was properly parsed.
     * @param type the type of term list.
     * @param goTerms the list of terms.
     */
    private void validateGoTerms(GeneOntologyType type, List<GeneOntology> goTerms){
        assertFalse("The list for " + type + " were null",goTerms.isEmpty())
        for(GeneOntology term : goTerms){
            assertNotNull("The term id was null: " + term, term.goId);
            assertNotNull("The term name was null: " + term, term.name);
            assertNotNull("The term type was null: " + term, term.type);
        }
    }
    
    private void validateArticle(Article article){
        log.debug ("Article ")
        assertNotNull("The pubmed id was null", article.pubmedId);
        assertNotNull("The title was null", article.title);
        
        validateJournal(article.journal);
        assertFalse("The keywords were empty" , article.keywords.isEmpty())
    }
    
    private void validateJournal(Journal journal){
        assertNotNull("The journal was null" , journal);
        assertNotNull("The journal title was null", journal.title);
        assertNotNull("The journal startPage was null", journal.startPage);
    }

}
