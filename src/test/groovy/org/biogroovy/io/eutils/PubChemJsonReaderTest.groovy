package org.biogroovy.io.eutils;

import static org.junit.Assert.*

import org.biogroovy.models.Drug
import org.junit.Test

class PubChemJsonReaderTest {
	
	PubChemJsonReader reader = new PubChemJsonReader();

	@Test
	public void testFetch() {
		Drug drug = reader.fetch("60750")
		validateDrug(drug)
	}
	
	public void testRead() {
		InputStream is = getClass().getResourceAsStream("60750.pubchem.json")
		Drug drug = reader.read(is)
		validateDrug(drug)
	}

	@Test
	public void testGetUrl() {
		URL url = reader.getUrl("60750", null);
		assertNotNull(url);
		println url;
	}
	

	private void validateDrug(Drug drug) {
		assertNotNull("The drug was null", drug)
		assertNotNull("The drug name was null", drug.name)
		assertEquals("The molecular formula was incorrect",'C9H11F2N3O4', drug.molFormula)
		assertTrue("The molecular weight was incorrect", drug.molWeight == 263.198146f)
		assertEquals("The smiles string was incorrect", "C1=CN(C(=O)N=C1N)C2C(C(C(O2)CO)O)(F)F", drug.smiles)
		assertEquals("The InChi key was incorrect","SDUQYLNIPVEERB-QPPQHZFASA-N", drug.inChiKey)
		assertTrue("The XLogP value was incorrect", -1.5f == drug.xLogp)
		assertEquals("The number of rotational bonds was incorrect",2, drug.rotatableBonds)
		
	}

}
