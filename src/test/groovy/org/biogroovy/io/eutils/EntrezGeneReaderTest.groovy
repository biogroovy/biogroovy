package org.biogroovy.io.eutils;
import static org.junit.Assert.*

import org.biogroovy.io.eutils.EntrezGeneReader;
import org.biogroovy.models.Gene
import org.biogroovy.models.GeneOntology
import org.junit.BeforeClass
import org.junit.Ignore
import org.junit.Test

import thewebsemantic.Bean2RDF

import com.hp.hpl.jena.ontology.OntModel
import com.hp.hpl.jena.rdf.model.ModelFactory

/**
 * This test verifies that the EntrezGeneReader can parse an EntrezGene record.
 *
 */
public class EntrezGeneReaderTest {

	static String DESC = '''The protein encoded by this gene is known to interact with cellular and viral survival-promoting proteins, such as BCL2 and the Epstein-Barr virus in order to enhance programed cell death. Because its activity is suppressed in the presence of survival-promoting proteins, this protein is suggested as a likely target for antiapoptotic proteins. This protein shares a critical BH3 domain with other death-promoting proteins, BAX and BAK. [provided by RefSeq]''';

	/**
	 * Test method for {@link org.biogroovy.eutils.EntrezGeneReader#fetch(java.lang.String)}.
	 */
	@Test
	public void testRead() throws Exception{
		EntrezGeneReader reader = new EntrezGeneReader();
		Gene gene = reader.fetch("675")
		
		validateGene(gene);
	}

	private void validateGene(Gene gene){
		assertNotNull("The gene was null", gene);
		assertEquals("The gene id was incorrect",675,gene.entrezGeneId);
		assertEquals("The symbol was incorrect: ${gene.symbol}","BRCA2",gene.symbol);
		assertEquals("The gene name was incorrect","breast cancer 2, early onset", gene.name);
		assertNotNull("The description was incorrect",gene.description)
		assertEquals("The species was incorrect","Homo sapiens", gene.species);

        println "Synonyms: ${gene.synonyms}"
        assertNotNull("The synonyms were null", gene.synonyms);
        assertFalse("The synonyms were empty", gene.synonyms.isEmpty());

		// validate the references
        //println "db references: ${gene.references}"
		Map<String, String> dbMap = [HGNC:"1101", Ensembl:"ENST00000380152", HPRD:"02554", MIM:"600185"];
		
		assertNotNull("The references were null", gene.references)
		assertFalse("The references were empty", gene.references.isEmpty())	
		
		dbMap.each { key, value ->	
			assertEquals("The ${key} reference was not parsed properly", value, gene.references.get(key));
		}

		assertNotNull("The GeneRIF list was null", gene.articles);
		assertFalse("The article list was empty", gene.articles.isEmpty());
		//println "GeneRIFs ${gene.articles}";

		assertNotNull("The GO Function List was null", gene.goFunctionList);
		assertNotNull("The GO Process list was null", gene.goProcessList);
		assertNotNull("The GO Component list was null", gene.goComponentList);

        gene.goFunctionList.each{GeneOntology go ->
            println go;
        }
		
		assertFalse("The GO Function list was empty", gene.goFunctionList.isEmpty());
		assertFalse("The Go Process list was empty", gene.goProcessList.isEmpty());
		assertFalse("The GO Component list was empty", gene.goComponentList.isEmpty());


		assertNotNull("The phenotype list was null", gene.phenotypes);
		println "phenotypes: "+ gene.phenotypes;
		assertFalse("The phenotype list was empty", gene.phenotypes.isEmpty());
		
		println "goFunctions: " + gene.goFunctionList
		println "goProcesses: " + gene.goProcessList
		println "goComponents: " +  gene.goComponentList
	}


	/**
	 * Test method for {@link org.biogroovy.eutils.EntrezGeneReader#readFile(java.lang.String)}.
	 */
	@Test
	public void testReadFile() throws Exception {
		EntrezGeneReader reader = new EntrezGeneReader();
		Gene gene = reader.readFile("src/test/resources/675.xml");
		validateGene(gene)
	}
	
	@Ignore
	public void testJenaBeanSerialize(){
		EntrezGeneReader reader = new EntrezGeneReader();
		Gene gene = reader.readFile("src/test/resources/675.xml");
		
		OntModel ontModel = ModelFactory.createOntologyModel();
		Bean2RDF writer = new Bean2RDF(ontModel);
		writer.save(gene);
		ontModel.write( new FileOutputStream(new File("675.rdf")));
	
	}

}