
package org.biogroovy.io.eutils;

import static org.junit.Assert.*;

import java.text.DateFormat;
import java.text.SimpleDateFormat 

import org.junit.Ignore 
import org.junit.Test;
import org.biogroovy.io.eutils.PubMedArticleReader;
import org.biogroovy.models.*;

class PubMedArticleReaderTest {
	PubMedArticleReader reader = new PubMedArticleReader();

	@Test
	public void testReadFile() {
		File file = new File('src/test/resources/pubmed.xml')
		Article article = reader.read(new FileInputStream(file))
		validateArticle(article)
		println article;
	}
	
	@Ignore
	public void testRead(){
		Article article = reader.fetch("19245720");
		for(MeshHeading meshHeading : article.meshHeadings){
			if (meshHeading.descriptorName != null){
				print meshHeading.toString();
			}
		}
	}
	
	@Test
	public void testReadString() {
		Article article = reader.fetch("11748933");
		validateArticle(article)
	}
	
	private void validateArticle(Article article){
		assertEquals("11748933",article.pubmedId);
		DateFormat format = new SimpleDateFormat("yyyy/MM/dd")
		Date createdDate = format.parse("2001/12/26")
		assertEquals(createdDate, article.dateCreated);
		assertEquals("Cryobiology", article.journal.title);
		assertEquals("42", article.journal.volume);
		assertEquals("4", article.journal.issue);
		assertEquals("Is cryopreservation a homogeneous process? Ultrastructure and motility of untreated, prefreezing, and postthawed spermatozoa of Diplodus puntazzo (Cetti).", article.title)
		assertNotNull("Abstract was null",article.abs);
		
		assertNotNull("DOI was null", article.doi);
		
		assertFalse("author list was empty",article.authors.isEmpty());
		
		Author firstAuth = article.authors.get(0);
		assertNotNull(firstAuth);
		assertEquals("Taddei", firstAuth.lastname);
		assertEquals("A R", firstAuth.firstname);
		assertEquals("AR", firstAuth.initials);
		
		assertFalse("Mesh Headings were empty",article.meshHeadings.isEmpty());
		MeshHeading mesh = article.meshHeadings.get(0);
		assertNotNull(mesh);
		assertEquals("Animals",mesh.descriptorName)
		
		mesh = article.meshHeadings.get(1)
		println mesh;
		
		assertNotNull(mesh)
		assertEquals("Cell Membrane", mesh.descriptorName);
		assertEquals("ultrastructure", mesh.qualifierNames.get(0))
		
		// validate journal
		println article.journal
		assertNotNull("The journal was null", article.journal);
		assertEquals("Cryobiology",article.journal.title);
		assertEquals("0011-2240",article.journal.issn);
		assertEquals("42",article.journal.volume);
		assertEquals("4",article.journal.issue);
	}



}
