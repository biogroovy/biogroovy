package org.biogroovy.io.eutils;

import static org.junit.Assert.*;

import org.junit.Test;
import org.biogroovy.io.eutils.GBSeqTranscriptReader;
import org.biogroovy.models.*;

public class GBSeqTranscriptReaderTest {
	
	GBSeqTranscriptReader reader = new GBSeqTranscriptReader();
	

	@Test
	public void testReadString() {
		Transcript trans = reader.read("5");
		validateTranscript(trans);
	}

	private void validateTranscript(Transcript trans){
		assertNotNull(trans.sequence)
		assertEquals(trans.species, 'Bos taurus');
		assertEquals(trans.name, 'B.bovis beta-2-gpI mRNA for beta-2-glycoprotein I');
		assertEquals(trans.accession, 'X60065')
	}

}
