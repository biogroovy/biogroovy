package org.biogroovy.io.eutils;

import static org.junit.Assert.*

import java.text.DateFormat
import java.text.SimpleDateFormat

import org.biogroovy.io.eutils.PubMedSlurper;
import org.biogroovy.models.Article
import org.biogroovy.models.Author
import org.biogroovy.models.MeshHeading
import org.junit.Test

class PubMedSlurperTest {

	@Test
	public void testReadInputStream() {
		PubMedSlurper slurper = new PubMedSlurper();
		Article article = slurper.read(getClass().getResourceAsStream("/pubmed.xml"));
		validateArticle(article);
	}
	
	private void validateArticle(Article article){
		assertEquals("11748933",article.pubmedId);
		DateFormat format = new SimpleDateFormat("yyyy/MM/dd")
		Date createdDate = format.parse("2000/12/31")
		assertEquals(createdDate, article.dateCreated);
		
		assertNotNull("The journal was null", article.journal)
		assertEquals("Cryobiology", article.journal.title);
		assertEquals("42", article.journal.volume);
		assertEquals("4", article.journal.issue);
		assertEquals("Is cryopreservation a homogeneous process? Ultrastructure and motility of untreated, prefreezing, and postthawed spermatozoa of Diplodus puntazzo (Cetti).", article.title)
		assertNotNull("Abstract was null",article.abs);
        assertTrue("Abstract was empty", ""!=article.abs);
        println "abs: ${article.abs}"
		
		assertNotNull("DOI was null", article.doi);
		
		assertFalse("author list was empty",article.authors.isEmpty());
		
		Author firstAuth = article.authors.get(0);
        println "author: " + firstAuth
		assertNotNull(firstAuth);
        assertNotNull(firstAuth.lastname)
		assertEquals("Taddei", firstAuth.lastname);
		assertEquals("A R", firstAuth.firstname);
		assertEquals("AR", firstAuth.initials);
		
		assertFalse("Mesh Headings were empty",article.meshHeadings.isEmpty());
		MeshHeading mesh = article.meshHeadings.get(0);
		assertNotNull(mesh);
		assertEquals("Animals",mesh.descriptorName)
		
		mesh = article.meshHeadings.get(1)
		println mesh;
		
		assertNotNull(mesh)
		assertEquals("Cell Membrane", mesh.descriptorName);
		assertFalse(mesh.qualifierNames.isEmpty());
		assertEquals("Qualifier names not parsed properly: " + mesh,"ultrastructure", mesh.qualifierNames.get(0))
		
		// validate journal
		println article.journal
		assertNotNull("The journal was null", article.journal);
		assertEquals("Cryobiology",article.journal.title);
		assertEquals("0011-2240",article.journal.issn);
		assertEquals("42",article.journal.volume);
		assertEquals("4",article.journal.issue);
	}

}
