package org.biogroovy.io;

import static org.junit.Assert.*

import org.biogroovy.io.local.PDFArticleSerializer;
import org.biogroovy.models.Article
import org.junit.Ignore
import org.junit.Test

/**
 *
 */
class PDFArticleSerializerTest {
	
	PDFArticleSerializer serializer = new PDFArticleSerializer();
	
	private static final String inFileName = "/1471-2105-10-73.pm.pdf";
	private static final String outFileName = "1471-2105-10-73.out.pdf";

	@Test
	public void testReadMetadata() {
		InputStream inFile = getClass().getResourceAsStream(inFileName);
		assertNotNull("The input stream was null", inFile);
		
		Article article = serializer.readMetadata(inFile);
		assertNotNull("The article was null", article);
		assertNotNull("The title was null", article.title);
		assertNotNull("The abstract was null", article.abs);
		assertFalse("There were no keywords", article.meshHeadings.isEmpty())
		assertFalse("There were no authors", article.authors.isEmpty());
		assertNotNull("Creation date was null", article.dateCreated);
		assertNotNull("Revised date was null", article.dateRevised);
		assertNotNull("The PMID was null", article.pubmedId);
	}
	
	@Ignore
	public void testWriteMetadata(){
		InputStream inFile = getClass().getResourceAsStream(inFileName);
		
		File outFile = new File(outFileName)
		FileOutputStream outStream = new FileOutputStream();
		
		Article article = null;
		serializer.writeMetadata(inFile, outStream, article);
		
		
		// read the file that we just updated
		Article readArticle = serializer.readMetadata(new FileInputStream(outFile));
		
		// verify that the data is the same.
		
		// cleanup afterwards
		outFile.delete();
	}

}
