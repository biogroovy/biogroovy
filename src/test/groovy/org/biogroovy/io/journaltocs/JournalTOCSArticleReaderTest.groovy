package org.biogroovy.io.journaltocs;

import static org.junit.Assert.*

import org.biogroovy.io.journaltocs.JournalTOCSArticleReader;
import org.biogroovy.models.Article
import org.biogroovy.models.Author
import org.junit.Test

class JournalTOCSArticleReaderTest {

	@Test
	public void testReadListInputStream() {
		InputStream instream = getClass().getResourceAsStream("/1471-2407.articles.xml");
		assertNotNull("Can't find test resource",instream);
		
		JournalTOCSArticleReader reader = new JournalTOCSArticleReader();
		List<Article> articleList = reader.readList(instream);
		assertNotNull("The article list was null", articleList);
		assertFalse("The article list was empty", articleList.isEmpty());
		
		articleList.each{ Article article -> 
			println "${article.title} -- ${article.doi}"
			article.authors.each {Author author ->
				println "  ${author.lastname}, ${author.firstname}"
			}
			println "${article.abs}\n";
			
		}

	
	}

}
