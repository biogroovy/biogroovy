package org.biogroovy.io.journaltocs;

import static org.junit.Assert.*

import org.biogroovy.models.Journal
import org.biogroovy.models.JournalRights
import org.junit.Ignore
import org.junit.Test

class JournalTOCSJournalReaderTest {

	@Test
	public void test() {
		InputStream instream = getClass().getResourceAsStream("/1471-2407.journal.xml");
		assertNotNull("Can't find test resource",instream);
		
		JournalTOCSJournalReader reader = new JournalTOCSJournalReader();
		Journal journal = reader.read(instream);
		
		println journal;
		
		validate(journal);
		
	}
	
	@Test
	public void testFetch(){
		JournalTOCSJournalReader reader = new JournalTOCSJournalReader();
		Journal journal = reader.fetch('1471-2407')
		println journal
		validate(journal)
	}
	
	@Test
	public void testFetch2(){
		JournalTOCSJournalReader reader = new JournalTOCSJournalReader();
		Journal journal = reader.fetch('0028-0836')
		println journal
	}
	
	private void validate(Journal journal) {
		assertNotNull("The journal was null", journal)
		assertEquals("The title was wrong", 'BMC Cancer', journal.title)
		assertEquals("The siteUrl was wrong", 'http://www.biomedcentral.com/bmccancer/', journal.siteUrl)
		assertEquals("The issn was wrong", '1471-2407', journal.issn)
		assertEquals("The rights were wrong", JournalRights.OPEN_ACCESS)
		
	}

}
