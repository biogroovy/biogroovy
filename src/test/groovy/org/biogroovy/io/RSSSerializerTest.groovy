package org.biogroovy.io;

import static org.junit.Assert.*

import org.biogroovy.io.local.RSSReader;
import org.biogroovy.io.local.RSSWriter;
import org.biogroovy.models.RSSFeed
import org.junit.Test

class RSSSerializerTest {

	@Test
	public void test() {
		Serializer ser = new Serializer(name:'RSS', reader:new RSSReader(), writer: new RSSWriter());
		
		RSSFeed feed = new RSSFeed(name:'Test Feed', url:'http://www.whatever.com', iconUrl:"http://www.whatever.com");
		feed.tagSet = ['Cool','Really Cool','Whatever'];
		feed.description = "This is a description";
		
		File file = File.createTempFile("test",".rss.xml");
		file.deleteOnExit();
		
		
		FileOutputStream out = new FileOutputStream(file);
		ser.writer.write(feed, out);
		
		out.flush();
		out.close();
		
		println file;
		println file.text
		
		assertTrue("The file doesn't exist",file.exists());
		assertTrue("The file's size is too small",file.length() > 0);
		
		BufferedInputStream inStream = file.newInputStream();
		
		RSSFeed feed2 = ser.reader.read(inStream);
		assertNotNull("The feed was null", feed2);
		
		assertEquals(feed.name, feed2.name);
		assertEquals(feed.url, feed2.url);
		assertEquals(feed.iconUrl, feed2.iconUrl);
		assertEquals(feed.tagSet.size(), feed2.tagSet.size())
		assertEquals(feed.description, feed2.description);
		
		inStream.close();
	}

}
