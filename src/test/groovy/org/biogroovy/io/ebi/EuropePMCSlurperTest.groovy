package org.biogroovy.io.ebi

import groovy.json.JsonSlurper
import groovy.util.slurpersupport.NodeChild;
import org.biogroovy.models.Article;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * This test validates that the EuropePMCSlurper is working properly.
 */
public class EuropePMCSlurperTest {

    @Test
    public void testRead() throws Exception {
        EuropePMCSlurper slurper = new EuropePMCSlurper();
        InputStream is = this.getClass().getResourceAsStream("/europepmc_list.json");
        Article article = slurper.read(is);
        validateArticle(article);
    }


    @Test
    public void testReadList() throws Exception {

        EuropePMCSlurper slurper = new EuropePMCSlurper();
        InputStream is = this.getClass().getResourceAsStream("/europepmc_list.json");
        List<Article> articleList = slurper.readList(is);
        assertFalse("The article list was empty", articleList.isEmpty())
    }

    private void validateArticle(Article article){
        assertNotNull("The article was null", article);
        assertNotNull("The title was null", article.title);
        assertFalse("The author list is empty", article.authors.isEmpty());
        assertNotNull("The journal title was null", article.journal.title)
        assertNotNull("The abstract was null", article.abs)
        println article;
    }
}