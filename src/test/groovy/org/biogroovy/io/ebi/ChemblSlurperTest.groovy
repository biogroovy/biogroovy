package org.biogroovy.io.ebi;

import static org.junit.Assert.*

import org.biogroovy.models.Drug
import org.junit.Test

class ChemblSlurperTest {

	/**
	 * Verify that we can fetch a drug from the chembl service.
	 */
	@Test
	public void testFetch() {
		ChemblSlurper reader = new ChemblSlurper();
		Drug drug = reader.fetch("CHEMBL888")
		validate(drug)
	}
	
	/**
	 * Verify that we can read a chembl xml file and parse a drug properly.
	 */
	@Test
	public void testReadStream() {
		ChemblSlurper reader = new ChemblSlurper();
		InputStream is = getClass().getResourceAsStream("/CHEMBL888.xml");
		assertNotNull("The input stream was null", is)
		Drug drug = reader.read(is);
		validate(drug)
	}
	
	/**
	 * This method validates the Drug object after it has been parsed from the file or URL.
	 * @param drug the drug to be validated.
	 */
	private void validate(Drug drug) {
		assertNotNull("The drug was null", drug)
		assertEquals("The drug name was not parsed properly", "GEMCITABINE", drug.name)
		assertEquals("The smiles string was not parsed properly", "NC1=NC(=O)N(C=C1)[C@@H]2O[C@H](CO)[C@@H](O)C2(F)F", drug.smiles)
		assertEquals("The chembl ID was not parsed properly",drug.chemblId, 'CHEMBL888')
		assertEquals("PassesRuleOfThree incorrect",Boolean.FALSE, drug.passesRuleOfThree)
		assertTrue("Mol Weight incorrect: " + drug.molWeight,Float.parseFloat('263.20') == drug.molWeight)
		assertEquals("Mol Formula incorrect", 'C9H11F2N3O4', drug.molFormula)
		assertTrue("ACD LogD incorrect", Float.parseFloat('-2.22') == drug.acdLogd)
		assertTrue("ACD LogP incorrect", Float.parseFloat('-2.22') == drug.acdLogp)
		assertTrue("alogp incorrect: " + drug.alogp, Float.parseFloat('-1.39') == drug.alogp)
		assertEquals("isKnownDrug incorrect", Boolean.TRUE, drug.isKnownDrug)
		assertEquals("isMedChemFriendly incorrect",Boolean.FALSE, drug.isMedChemFriendly)
		assertEquals("Synonyms incorrect","Gemzar,LY-188011,SID124893555,Gemcitabine,SID50113286", drug.synonyms)
		assertEquals("Rotatable bonds incorrect",2, drug.rotatableBonds)

	}

}
