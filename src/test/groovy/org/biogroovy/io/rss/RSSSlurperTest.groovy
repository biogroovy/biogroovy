package org.biogroovy.io.rss;

import static org.junit.Assert.*
import groovy.json.JsonSlurper
import groovy.json.StreamingJsonBuilder
import groovy.util.logging.Slf4j

import org.biogroovy.models.Article
import org.biogroovy.models.RSSFeed
import org.junit.Test

@Slf4j
class RSSSlurperTest {

	Map<String, String> urlMap = [
		"nytimes":"http://www.nytimes.com/services/xml/rss/nyt/HomePage.xml",
		"nytimesbusiness":"http://feeds.nytimes.com/nyt/rss/Business",
		"guardian-world":"http://www.theguardian.com/world/rss",
		"guardian-us":"http://www.theguardian.com/us/rss",
		"guardian-tech":"http://www.theguardian.com/us/technology/rss",
		"cnet":"http://www.cnet.com/rss/news/",
		"fiercebiotech":"http://www.fiercebiotech.com/feed",
		"aacr":"http://cancerres.aacrjournals.org/rss/current.xml",
		"journaltocs":"http://www.journaltocs.ac.uk/api/mark.fortner@aspenbiosciences.com?output=articles"
	];

	@Test
	public void testAacr() {
		testWrite(urlMap.aacr, "aacr.json")
	}
	
	@Test
	public void testNYTimes() {
		testWrite(urlMap.nytimes, "nytimes.json")
		testWrite(urlMap.nytimesbusiness, "nytimesbusiness.json")
	}
	
	@Test
	public void testGuardian() {
		testWrite(urlMap."guardian-world","guardian-world.json")
	}
	
	@Test
	public void testGuardianMultiWriteList() {
		 testMultiWriteList("guardian.multilist.json", urlMap."guardian-world", urlMap."guardian-us", urlMap."guardian-tech")
	}
	
	@Test
	public void testGuardianMultiWrite() {
		 testMultiWrite("guardian.multi.json", urlMap."guardian-world", urlMap."guardian-us", urlMap."guardian-tech")
	}



	@Test
	public void testReadSource() {
		testRead("nytimes",urlMap.nytimes)
	}

	@Test
	public void testJournalTocs() {
		testRead("journaltocs",urlMap.journaltocs)
	}

	@Test
	public void testMultipleSources(){
		fetchFiles("/newsfeeds.txt", "news.json")
	}

	@Test
	public void cancerJournals() {
		fetchFiles("/cancer.newsfeeds.txt","cancer.json")
	}
	
	@Test
	public void writeCancerJournals() {
		testWrite(urlMap.journaltocs, "cancer2.json")
	}

	@Test
	public void biotechJournals() {
		fetchFiles("/biotech.newsfeeds.txt","biotech.json")
	}

	private void testRead(String name, String url) {
		IRSSSlurper<RSSFeed> slurper = new RSSSlurper();

		RSSFeed source = slurper.readSource(getInputSource(url));

		assertNotNull("The feed name was null [${name}]",source.name)
		assertNotNull("The description was null [${name}]",source.description)
		
		assertNotNull(source.articleList)
		assertFalse(source.articleList.isEmpty())

		source.articleList.each{Article article ->
			assertNotNull(article.mediaList);
			assertFalse(article.mediaList.isEmpty())
			println article;
		}
		
		
	}

	private void testWrite(String urlStr, String out) {
		IRSSWriter<RSSFeed, Writer> rssWriter = new RSSJSONFileWriter<>();
		IRSSSlurper<RSSFeed> slurper = new RSSSlurper();

		File outputFile = new File(System.getProperty("user.home"), out);
		FileWriter writer = new FileWriter(outputFile)
		
		URL url = new URL(urlStr);
		RSSFeed feed = slurper.readSource(url.openStream());
		
		rssWriter.write(feed, writer)
		
		assertTrue( outputFile.exists());
		assertTrue(outputFile.size() > 100);
		
		JsonSlurper jsonSlurper = new JsonSlurper();
		def node = jsonSlurper.parse(outputFile);
		println node;
	}
	
	private void testMultiWrite(String out, String... urls){
		List<RSSFeed> feeds = new ArrayList<>();
		
		urls.each{String urlStr ->
			IRSSSlurper<RSSFeed> slurper = new RSSSlurper();
			URL url = new URL(urlStr)
			RSSFeed feed = slurper.readSource(url.openStream())
			feeds.add(feed)
		}
		
		File outfile = new File(System.getProperty("user.home"), out);
		FileWriter writer = new FileWriter(outfile)
		
		IRSSWriter<RSSFeed, Writer> rssWriter = new RSSJSONFileWriter();
		rssWriter.writeFeeds(writer, feeds[0],feeds[1],feeds[2]);
		
		assertTrue(outfile.exists())
		assertTrue(outfile.size() > 200)
		
	}
	
	private void testMultiWriteList(String out, String... urls){
		List<RSSFeed> feeds = new ArrayList<>();
		
		urls.each{String urlStr ->
			IRSSSlurper<RSSFeed> slurper = new RSSSlurper();
			URL url = new URL(urlStr)
			RSSFeed feed = slurper.readSource(url.openStream())
			feeds.add(feed)
		}
		
		File outfile = new File(System.getProperty("user.home"), out);
		FileWriter writer = new FileWriter(outfile)
		
		IRSSWriter<RSSFeed, Writer> rssWriter = new RSSJSONFileWriter();
		rssWriter.writeFeeds(writer, feeds);
		
		assertTrue(outfile.exists())
		assertTrue(outfile.size() > 200)
		
	}

	private void fetchFiles(String inputfile, String outputfile) {
		IRSSSlurper<RSSFeed> slurper = new RSSSlurper();
		IRSSWriter<RSSFeed, Writer> rssWriter = new RSSJSONFileWriter<>();

		File outputFile = new File(System.getProperty("user.home"), outputfile);
		FileWriter writer = new FileWriter(outputFile)
		def builder = new StreamingJsonBuilder(writer)

		InputStream inputStream = getClass().getResourceAsStream(inputfile);

		builder.feeds {

			inputStream.eachLine{ String line ->

				if (line.length() > 0){

					try {
						URL url = new URL(line);
						RSSFeed feed = slurper.readSource(url.openStream());

						List<Article> articleList = feed.articleList;

						feed (

								name: feed.name,
								url: feed.url,
								description: feed.description,
								imageUrl: feed.iconUrl,
								articles: articleList.collect { Article article ->
									[
										title: article.title,
										"abstract": article.abs,
										createDate: article.dateCreated,
										keywords: article.getKeywords()
									]
								}
						)
					} catch(Exception ex) {
						log.debug(ex.getMessage(), ex);
					}
				}
			}
		}
		writer.flush();
		writer.close();

		assertTrue( outputFile.exists());
		assertTrue(outputFile.size() > 0);
	}

	private InputStream getInputSource(String urlStr){
		URL url = new URL(urlStr);
		return url.openStream();
	}


}
