import static org.junit.Assert.*

import org.biogroovy.models.Journal
import org.junit.Test


class JournalTest {

	@Test
	public void test() {
		Journal journal = new Journal(issn:'1362-4962');
		String url = journal.getURL("mark.fortner@aspenbiosciences.com");
		assertEquals("http://nar.oxfordjournals.org/", url);
	}

}
