<?xml version="1.0"?>
<xsl:stylesheet 
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
	xmlns:ncbi="http://www.biogroovy.org/gene" 
	>
	
	<xsl:template match="/Entrezgene-Set">
		<ncbi:geneset>
			<xsl:apply-templates />
		</ncbi:geneset>
	</xsl:template>
    
	<xsl:template match="/Entrezgene">
		<ncbi:gene>
			<xsl:attribute name="id">
			<xsl:value-of select="Entrezgene_track-info/Gene-track/Gene-track_geneid"/>
			</xsl:attribute>
			
			<xsl:attribute name="maploc">
				<xsl:value-of select="//Gene-ref_maploc"/>
			</xsl:attribute>
			
			<xsl:attribute name="created-date">
			<xsl:apply-templates />
			</xsl:attribute>
			
			<xsl:attribute name="updated-date">
			<xsl:apply-templates />
			</xsl:attribute>
			
			<xsl:attribute name="symbol">
			<xsl:value-of select="//Entrezgene_gene/Gene-ref/Gene-ref_locus"/>
			</xsl:attribute>
			
			<xsl:attribute name="name">
			<xsl:value-of select="//Entrezgene_gene/Gene-ref/Gene-ref_desc"/>
			</xsl:attribute>
			
			
			
			<ncbi:summary>
			<xsl:value-of select="//Entrezgene_summary"/>
			</ncbi:summary>
			
			
			<xsl:apply-templates />
		</ncbi:gene>
	</xsl:template>

	<xsl:template match="Date">
		
	</xsl:template>	
	
	<xsl:template match="Dbtag">
	 <ncbi:reference>
	 	<xsl:attribute name="id">
	 	<xsl:value-of select="./Dbtag_tag/Object-id/Object-id_id"/>
	 	</xsl:attribute>
	 	
	 	<xsl:attribute name="database">
	 	<xsl:value-of select="./Dbtag_db"/>
	 	</xsl:attribute>
	 </ncbi:reference>
	</xsl:template>
	
	<xsl:template match="//BioSource">
		<ncbi:biosource>
			<xsl:attribute name="type">
				<xsl:value-of select="./BioSource_genome/@value"/>
			</xsl:attribute>
			
			<xsl:attribute name="taxon">
				<xsl:value-of select="//Object-id_id"/>
			</xsl:attribute>
			
			<xsl:attribute name="name">
				<xsl:value-of select="//Org-ref_taxname"/>
			</xsl:attribute>
			<xsl:attribute name="lineage">
				<xsl:value-of select="//OrgName_lineage"/>
			</xsl:attribute>
		</ncbi:biosource>
	</xsl:template>
	
	
	<xsl:template match="//Gene-commentary">
		<ncbi:commentary>
			<xsl:attribute name="type">
				<xsl:value-of select="Gene-commentary_type/@value"/>
			</xsl:attribute>
			
			<xsl:attribute name="created-date">
			
				<xsl:apply-templates />
			</xsl:attribute>
			
			<xsl:attribute name="updated-date">
				<xsl:apply-templates />
			</xsl:attribute>
			
			<ncbi:comment>
				<xsl:value-of select="//Gene-commentary_text"/>
			</ncbi:comment>
			
			
			<xsl:apply-templates/>
		</ncbi:commentary>
	
	</xsl:template>
	
	<xsl:template match="//Pub">
		<ncbi:reference type="pubmed">
			<xsl:attribute name="id">
				<xsl:value-of select="Pub_pmid/PubMedId"/>
			</xsl:attribute>
		</ncbi:reference>
	</xsl:template>
	
	

</xsl:stylesheet>

