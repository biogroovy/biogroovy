# README #

BioGroovy? is an open-source project dedicated to providing a Groovy framework for processing biological data. Like it's sister projects BioPerl and BioPython, BioGroovy can be used either in interpreted or compiled mode, or one can mix both compiled and interpreted code. The Groovy scripting language can be used with any Java-based library (such as the BioJava API) or with native code (such as the EMBOSS library) through the Java Native Access API, or through the Java Native Interface. Groovy syntax can be either Java-like or Groovy-fied depending on your level of comfort. Groovy is also the basis for Web Development frameworks like Grails which makes it possible to generate fully functional web applications within a few minutes. Thus Groovy is well-suited for a wide-variety of bioinformatics-related tasks.


[Setup](https://bitbucket.org/biogroovy/biogroovy/wiki/Setup)

# Contributing to BioGroovy #
## Setting Up Your Environment ##
In order to contribute to BioGroovy you'll need Java, Groovy, Gradle and Git.

1. Install the latest version of Java 8.
1. Install [sdkman](http://www.sdkman.io).
1. Use gvm to install Groovy, and Gradle.
1. Create an issue in BioGroovy? for your contribution.
1. Use git to clone the repository
1. Create a branch in your local git repository for your contribution
1. Use GGTS/Intellij IDEA or your favorite IDE to edit the code.
1. Add unit tests for you code that verify that it does what it's supposed to.
1. Submit a pull request for your contribution.

## Compiling The Code ##
To compile the code, you can use the **gradle clean install** command from the command line. To see what gradle tasks are available in this project, type **gradle tasks**.

## Trying Out The Code ##
The unit tests provide an easy entrez into the BioGroovy code base. Each package contains a **package-info.groovy** file that describes the contents of the package and entry points into code. These entry points are typically the names of key interfaces, and some example implementing classes. Once you have an idea of which piece of code you'd like to try out, open up the corresponding unit test class in your favorite IDE and right-click the name of the test that you want to run. A popup menu will appear allowing you to run the unit test method. If you're using Eclipse, the menu is typically found in **Run As > JUnit Test**. The unit test methods usually provide you enough insite into how to invoke a particular method, and what to expect back in response.

Let's take a look at the **MyGeneInfoFetcherTest** class and its **testFetch** method. In this example, we are retrieving the human BRCA2 gene (whose EntrezGene ID is 675).

The first time you run this class you may find an error message telling you that you need to personalize your **biogroovy.conf** file. This file typically contains the userids, and email addresses required by RESTful web services. When you invoke a service, biogroovy uses the information in your configuration file to identify you to the web service owner. This allows them to provide you with feedback about how your web service client is running. To get past this error message, simply provide your email address in the appropriate places in the configuration file, and resave it.

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact