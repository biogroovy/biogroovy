package org.biogroovy.eutils;

import static org.junit.Assert.*

import org.biogroovy.search.SearchResult
import org.junit.Ignore
import org.junit.Test

class PubMedSearchEngineTest {
	
	static PubMedSearchEngine searchEngine = new PubMedSearchEngine();

	@Test
	public void testGetName() {
		assertNotNull("The search engine name was null",searchEngine.getName());
	}

	@Test
	public void testGetURLTemplate() {
		String template = searchEngine.getURLTemplate([searchTerm:'pancreatic cancer']);
		println template;
		assertNotNull("The search engine url was null",template);
	}

	@Ignore
	public void testDoSearch() {
		List<SearchResult> results = searchEngine.doSearch([searchTerm:'pancreatic cancer']);
		assertNotNull("The results were null", results);
		assertFalse("The results were empty", results.isEmpty());
		assertEquals("Result count not correct",20, results.size())
		println results
	}

}
