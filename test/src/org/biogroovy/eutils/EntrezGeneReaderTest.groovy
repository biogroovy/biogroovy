package org.biogroovy.eutils;
import static org.junit.Assert.*

import org.biogroovy.io.EntrezGeneReader
import org.biogroovy.models.Gene
import org.junit.BeforeClass
import org.junit.Ignore
import org.junit.Test

import thewebsemantic.Bean2RDF

import com.hp.hpl.jena.ontology.OntModel
import com.hp.hpl.jena.rdf.model.ModelFactory

/**
 * @author markfortner
 *
 */
public class EntrezGeneReaderTest {

	static String DESC = '''The protein encoded by this gene is known to interact with cellular and viral survival-promoting proteins, such as BCL2 and the Epstein-Barr virus in order to enhance programed cell death. Because its activity is suppressed in the presence of survival-promoting proteins, this protein is suggested as a likely target for antiapoptotic proteins. This protein shares a critical BH3 domain with other death-promoting proteins, BAX and BAK. [provided by RefSeq]''';
	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	/**
	 * Test method for {@link org.biogroovy.eutils.EntrezGeneReader#read(java.lang.String)}.
	 */
	@Test
	public void testRead() {
		EntrezGeneReader reader = new EntrezGeneReader();
		Gene gene = reader.fetch("638")
		
		validateGene(gene);
	}

	private void validateGene(Gene gene){
		assertNotNull("The gene was null", gene);
		assertEquals("The gene id was incorrect",638,gene.entrezGeneId);
		assertEquals("The symbol was incorrect: ${gene.getGeneSymbol()}","BIK",gene.geneSymbol);
		assertEquals("The gene name was incorrect","BCL2-interacting killer (apoptosis-inducing)", gene.geneName);
		assertNotNull("The description was incorrect",gene.description)
		assertEquals("The species was incorrect","Homo sapiens", gene.species);

		// validate the references
        println "Validating References"
		Map<String, String> dbMap = [HGNC:"1051", Ensembl:"ENSG00000100290", HPRD:"04547",MIM:"603392"];
		
		assertNotNull("The references were null", gene.references)
		assertFalse("The references were empty", gene.references.isEmpty())		
		assertEquals("The references were not parsed properly", dbMap, gene.references);
        dbMap.each{String key, String value ->
            assertNotNull("The ${key} value was null",gene.references.get(key))
            println "${key}: ${gene.references.get(key)}"
        }

        assertEquals("HGNC not parsed properly", dbMap.HGNC, gene.references.'HGNC');
        assertEquals("Ensembl ID not parsed properly", dbMap.Ensembl, gene.references.'Ensembl');
        assertEquals("HPRD not parsed properly", dbMap.HPRD, gene.references.'HPRD');
        assertEquals("MIM not parsed properly", dbMap.MIM, gene.references.'MIM');



        assertNotNull("The GeneRIF list was null", gene.articles);
		assertFalse("The article list was empty", gene.articles.isEmpty());
        assertEquals("HGNC not parsed properly", dbMap.HPRD, gene.references.'HPRD');
		println gene.articles;

		assertNotNull("The GO Function List was null", gene.goFunctionList);
		assertNotNull("The GO Process list was null", gene.goProcessList);
		assertNotNull("The GO Component list was null", gene.goComponentList);
		
		assertFalse("The GO Function list was empty", gene.goFunctionList.isEmpty());
		assertFalse("The Go Process list was empty", gene.goProcessList.isEmpty());
		assertFalse("The GO Component list was empty", gene.goComponentList.isEmpty());


		println gene.goFunctionList
		println gene.goProcessList
		println gene.goComponentList
	}


	/**
	 * Test method for {@link org.biogroovy.eutils.EntrezGeneReader#readFile(java.lang.String)}.
	 */
	@Test
	public void testReadFile() {
		EntrezGeneReader reader = new EntrezGeneReader();
		Gene gene = reader.readFile("./test/resources/638.xml");
		validateGene(gene)
	}
	
	@Ignore
	public void testJenaBeanSerialize(){
		EntrezGeneReader reader = new EntrezGeneReader();
		Gene gene = reader.readFile("./test/resources/638.xml");
		
		OntModel ontModel = ModelFactory.createOntologyModel();
		Bean2RDF writer = new Bean2RDF(ontModel);
		writer.save(gene);
		ontModel.write( new FileOutputStream(new File("./test/resources/638.rdf")));
	
	}

}