package org.biogroovy.eutils;

import static org.junit.Assert.*;

import org.junit.Test;

class StreamFactoryTest {

	@Test
	public void testProcessInputStreamStringStringFile() {
		File inputFile = new File("./test/resources/","638.xml")
		File outputFile = new File("./test/resources/","638.html")
		assertTrue inputFile.exists();
		StreamFactory.processInputStream(inputFile, EntrezGeneTransforms.BRIEF, new FileOutputStream(outputFile));
		assertTrue outputFile.exists();
		assertTrue outputFile.length() > 0
	}

}
