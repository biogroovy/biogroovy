package org.biogroovy.eutils;

import static org.junit.Assert.*

import org.biogroovy.models.Protein
import org.junit.Ignore
import org.junit.Test

public class GBSeqProteinReaderTest {
	GBSeqProteinReader reader = new GBSeqProteinReader();

	@Test
	public void testReadString() {
		Protein prot = reader.read("8");
		validateProtein(prot);
	}
	
	@Ignore
	public void testRead(){
		InputStream is = GBSeqProteinReaderTest.class.getResourceAsStream("8.xml");
		Protein prot = reader.read(is);
	}

	
	private void validateProtein(Protein prot){
		assertNotNull(prot.sequence);
		assertEquals('Bos taurus',prot.species);
		assertEquals('CAA35997', prot.accession);
		assertEquals('unnamed protein product [Bos taurus]', prot.name)
	}

}
