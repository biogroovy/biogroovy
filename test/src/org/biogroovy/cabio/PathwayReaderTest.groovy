package org.biogroovy.cabio;

import static org.junit.Assert.*

import org.biogroovy.models.Gene
import org.biogroovy.models.Pathway
import org.junit.Test

class PathwayReaderTest {

	@Test
	public void testReadFile() {
		PathwayReader reader = new PathwayReader();
		Pathway pathway = reader.read(this.getClass().getResourceAsStream("/cabio/pathway.xml"));
		assertNotNull("The pathway was null", pathway);
		assertNotNull("The pathway name was null", pathway.name);
	}
	
	@Test
	public void testReadGenes(){
		PathwayReader reader = new PathwayReader();
		List<Gene> geneList = reader.readList(this.getClass().getResource("/cabio/genes_in_pathway.xml"));
		geneList.each{gene ->
			println "gene: ${gene}"
		}
		assertNotNull("The gene list is null", geneList)
		assertFalse("The gene list was empty", geneList.isEmpty());
	}

}
