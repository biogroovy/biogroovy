package org.biogroovy.samples;

class DeliciousPubMedTest extends GroovyTestCase {
	void testParsePMIDFromURL() {
		String url = "http://www.ncbi.nlm.nih.gov/sites/entrez?Db=PubMed&Cmd=ShowDetailView&TermToSearch=10825129&ordinalpos=154&itool=EntrezSystem2.PEntrez.Pubmed.Pubmed_ResultsPanel.Pubmed_RVDocSum";
		String id = DeliciousPubMed.parsePMIDFromURL(url);
		assertNotNull("The id was null", id);
		assertEquals(id, "10825129");
	}
}